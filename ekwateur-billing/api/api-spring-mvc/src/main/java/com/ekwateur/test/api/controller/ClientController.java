package com.ekwateur.test.api.controller;

import com.ekwateur.test.core.exceptions.invalidinput.InvalidInputException;
import com.ekwateur.test.core.exceptions.notfound.ClientNotFoundException;
import com.ekwateur.test.core.pojos.models.client.Client;
import com.ekwateur.test.core.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/clients")
public class ClientController {
    @Autowired
    private ClientService clientService;

    @GetMapping()
    public List<Client> getAllClients(){
        return clientService.getAllClients();
    }

    @GetMapping("/{clientId}")
    public Client getClientById(@PathVariable String clientId) throws ClientNotFoundException, InvalidInputException {
        return clientService.getClientById(clientId);
    }

    @PostMapping
    public Client saveClient(@RequestBody Client client) throws InvalidInputException {
        return clientService.saveClient(client);
    }
}
