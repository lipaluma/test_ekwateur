package com.ekwateur.test.api.controller;

import com.ekwateur.test.core.exceptions.notfound.ClientNotFoundException;
import com.ekwateur.test.core.pojos.models.energy.EnergyRate;
import com.ekwateur.test.core.pojos.models.energy.EnergyRateCombination;
import com.ekwateur.test.core.pojos.models.energy.EnergyType;
import com.ekwateur.test.core.service.EnergyRateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("/rates")
public class EnergyRateController {

    @Autowired
    private EnergyRateService energyRateService;

    @GetMapping
    public Map<EnergyRateCombination, EnergyRate> getAllRates() {
        return energyRateService.getAllRates();
    }

    @GetMapping("/client/{clientId}/energy/{energy}")
    public EnergyRate getClientRateOnEnergy(@PathVariable String clientId, @PathVariable(name="energy") EnergyType energyType) throws ClientNotFoundException {
        return energyRateService.getClientEnergyRate(clientId, energyType);
    }
}
