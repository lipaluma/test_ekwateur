package com.ekwateur.test.api.controller;

import com.ekwateur.test.core.exceptions.BillAlreadyExistsException;
import com.ekwateur.test.core.exceptions.invalidinput.InvalidInputException;
import com.ekwateur.test.core.exceptions.notfound.BillNotFoundException;
import com.ekwateur.test.core.exceptions.notfound.ClientNotFoundException;
import com.ekwateur.test.core.pojos.models.bill.EnergyBill;
import com.ekwateur.test.core.service.EnergyBillingService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/energy/bills")
public class EnergyBillController {
    public static final Logger log = LoggerFactory.getLogger(EnergyBillController.class);

    @Autowired
    private EnergyBillingService energyBillingService;

    @GetMapping("/{id}")
    public EnergyBill getEnergyBillById(@PathVariable String id) throws BillNotFoundException {
        return energyBillingService.getBillById(id);
    }

    @GetMapping("/client/{clientId}/year/{year}/month/{month}")
    public EnergyBill getEnergyBillOfClientOnMonth(@PathVariable String clientId, @PathVariable int year, @PathVariable int month) throws BillNotFoundException {
        return energyBillingService.getBillOfClientOnMonth(clientId, year, month);
    }
    @PostMapping("/client/{clientId}/year/{year}/month/{month}")
    public EnergyBill generateEnergyBillOfClientOnMonth(@PathVariable String clientId, @PathVariable int year, @PathVariable int month) throws ClientNotFoundException, BillAlreadyExistsException, InvalidInputException {
        return energyBillingService.generateBill(clientId, year, month);
    }

}
