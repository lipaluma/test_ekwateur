package com.ekwateur.test.api.controller;

import com.ekwateur.test.core.service.EnergyRateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/admin")
public class Admin {

    @Autowired
    private EnergyRateService energyRateService;

    @GetMapping()
    public String accueilAdmin() {
        return "Greetings from Spring Boot!";
    }

    @PatchMapping("/reload-rates")
    public void reloadRates() {
        energyRateService.reloadRates();
    }
}
