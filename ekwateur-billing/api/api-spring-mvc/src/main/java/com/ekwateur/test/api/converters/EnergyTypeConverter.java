package com.ekwateur.test.api.converters;

import com.ekwateur.test.core.pojos.models.energy.EnergyType;
import org.springframework.core.convert.converter.Converter;

public class EnergyTypeConverter implements Converter<String, EnergyType> {
    @Override
    public EnergyType convert(String source) {
        return EnergyType.valueOf(source.toUpperCase());
    }
}