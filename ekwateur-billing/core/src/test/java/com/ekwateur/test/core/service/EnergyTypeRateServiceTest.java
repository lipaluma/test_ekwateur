package com.ekwateur.test.core.service;

import com.ekwateur.test.core.dao.ClientDao;
import com.ekwateur.test.core.dao.EnergyRateDao;
import com.ekwateur.test.core.exceptions.invalidinput.InputCannotBeNullException;
import com.ekwateur.test.core.exceptions.invalidinput.InvalidClientIdException;
import com.ekwateur.test.core.exceptions.notfound.ClientNotFoundException;
import com.ekwateur.test.core.pojos.models.client.ClientType;
import com.ekwateur.test.core.pojos.models.energy.EnergyRate;
import com.ekwateur.test.core.pojos.models.energy.EnergyType;
import com.ekwateur.test.core.test.utils.GivenData;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static com.ekwateur.test.core.pojos.models.energy.EnergyRateCombination.of;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

class EnergyTypeRateServiceTest {
    private static GivenData data = new GivenData().withRandomRates().withClients();

    private static ClientDao clientDao = Mockito.mock(ClientDao.class);
    private static EnergyRateDao energyRateDao = Mockito.mock(EnergyRateDao.class);

    private static ClientService clientService = new ClientService(clientDao);
    private static EnergyRateService energyRateService;

    @BeforeAll
    public static void init() {
        when(energyRateDao.loadAllRates()).thenReturn(data.RATES);
        energyRateService = new EnergyRateService(energyRateDao, clientService);
    }

    @BeforeEach
    public void setup(){
        Mockito.reset(clientDao);
    }

    @Test
    void should_return_some_energy_rates() {
        EnergyRate rate = energyRateService.getEnergyRate(ClientType.INDIVIDUAL, EnergyType.GAZ);
        assertEquals(data.RATES.get(of(ClientType.INDIVIDUAL, EnergyType.GAZ)), rate);
        rate = energyRateService.getEnergyRate(ClientType.PRO_REVENUE_GT_1M, EnergyType.ELECTRICITY);
        assertEquals(data.RATES.get(of(ClientType.PRO_REVENUE_GT_1M, EnergyType.ELECTRICITY)), rate);
    }

    @Test
    void should_return_gaz_energy_rate_of_client() throws ClientNotFoundException {
        when(clientDao.getById(data.PRO_CLIENT_LAMBDA.getId())).thenReturn(data.PRO_CLIENT_LAMBDA);
        EnergyRate rate = energyRateService.getClientEnergyRate(data.PRO_CLIENT_LAMBDA.getId(), EnergyType.GAZ);
        assertEquals(data.RATES.get(of(ClientType.PRO_REVENUE_LT_1M, EnergyType.GAZ)), rate);
    }

    @Test
    void should_return_client_not_found() throws ClientNotFoundException {
        String clientId = "EKW12345678";
        when(clientDao.getById(clientId)).thenThrow(new ClientNotFoundException(clientId));
        ClientNotFoundException ex = assertThrows(ClientNotFoundException.class, () -> energyRateService.getClientEnergyRate(clientId, EnergyType.GAZ));
        assertEquals(clientId, ex.getClientId());
    }
    @Test
    void should_return_bad_parameter_error() {
        assertThrows(InputCannotBeNullException.class, () -> energyRateService.getClientEnergyRate((String)null, EnergyType.GAZ));
        assertThrows(InvalidClientIdException.class, () -> energyRateService.getClientEnergyRate("3213548", EnergyType.GAZ));
    }
}