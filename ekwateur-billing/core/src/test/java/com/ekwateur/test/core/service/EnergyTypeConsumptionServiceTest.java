package com.ekwateur.test.core.service;

import com.ekwateur.test.core.dao.ClientDao;
import com.ekwateur.test.core.dao.EnergyConsumptionDao;
import com.ekwateur.test.core.exceptions.notfound.ClientNotFoundException;
import com.ekwateur.test.core.pojos.wrappers.ClientConsumption;
import com.ekwateur.test.core.test.utils.GivenData;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

class EnergyTypeConsumptionServiceTest {
    private static GivenData data = new GivenData().withClients().withClientConsumptions();

    private static ClientDao clientDao = Mockito.mock(ClientDao.class);
    private static EnergyConsumptionDao energyConsumptionDao = Mockito.mock(EnergyConsumptionDao.class);

    private static ClientService clientService = new ClientService(clientDao);
    private static EnergyConsumptionService energyConsumptionService = new EnergyConsumptionService(energyConsumptionDao, clientService);

    @BeforeAll
    public static void init() {
        when(energyConsumptionDao.getConsumption(eq(data.CLIENT1.getId()), any(LocalDateTime.class), any(LocalDateTime.class))).thenReturn(data.CLIENT1_ENERGY_CONSUMPTION);
        when(energyConsumptionDao.getConsumption(eq(data.CLIENT2.getId()), any(LocalDateTime.class), any(LocalDateTime.class))).thenReturn(data.CLIENT2_ENERGY_CONSUMPTION);
    }

    @BeforeEach
    public void setup() {
        Mockito.reset(clientDao);
    }

    @Test
    public void should_return_consumption() throws ClientNotFoundException {
        when(clientDao.getById(data.CLIENT1.getId())).thenReturn(data.CLIENT1);
        when(clientDao.getById(data.CLIENT2.getId())).thenReturn(data.CLIENT2);
        ClientConsumption client1_consumption = energyConsumptionService.getConsumptionOnLastMonth(data.CLIENT1.getId());
        assertEquals(new ClientConsumption(data.CLIENT1, data.CLIENT1_ENERGY_CONSUMPTION), client1_consumption);
        ClientConsumption client2_consumption = energyConsumptionService.getConsumptionOnLastMonth(data.CLIENT2.getId());
        assertEquals(new ClientConsumption(data.CLIENT2, data.CLIENT2_ENERGY_CONSUMPTION), client2_consumption);
    }
    @Test
    public void should_not_found_client() throws ClientNotFoundException {
        when(clientDao.getById("EKW12345678")).thenThrow(new ClientNotFoundException("EKW12345678"));
        assertThrows(ClientNotFoundException.class, () -> energyConsumptionService.getConsumptionOnLastMonth("EKW12345678"));
    }

}