package com.ekwateur.test.core.service;

import com.ekwateur.test.core.dao.ClientDao;
import com.ekwateur.test.core.dao.EnergyBillDao;
import com.ekwateur.test.core.dao.EnergyConsumptionDao;
import com.ekwateur.test.core.dao.EnergyRateDao;
import com.ekwateur.test.core.exceptions.BillAlreadyExistsException;
import com.ekwateur.test.core.exceptions.invalidinput.InputCannotBeNullException;
import com.ekwateur.test.core.exceptions.invalidinput.InvalidInputException;
import com.ekwateur.test.core.exceptions.notfound.BillNotFoundException;
import com.ekwateur.test.core.exceptions.notfound.ClientNotFoundException;
import com.ekwateur.test.core.pojos.models.bill.EnergyBill;
import com.ekwateur.test.core.pojos.models.client.Client;
import com.ekwateur.test.core.pojos.models.consumption.EnergyConsumption;
import com.ekwateur.test.core.pojos.models.energy.EnergyType;
import com.ekwateur.test.core.pojos.wrappers.ClientConsumption;
import com.ekwateur.test.core.test.utils.GivenData;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.time.LocalDateTime;

import static com.ekwateur.test.core.pojos.models.energy.EnergyRateCombination.of;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

class EnergyTypeBillingServiceTest {
    private static GivenData data = new GivenData().withRandomRates().withClients().withClientConsumptions().withBills();

//    public static final IndividualClient CLIENT1 = new IndividualClient("EKW11111111", "Mr", "Soares", "Mario");
//    public static final ProClient CLIENT2 = new ProClient("EKW22222222", "Tesla", "213546868", 400000000l);
//    public static final EnergyBill BIL_EKW_11111111_202302 = EnergyBill.builder().id("BIL_EKW11111111_202302").client(CLIENT1).year(2023).month(2).gazAmount(130.0).electricityAmount(150.0).build();
//    public static final EnergyConsumption CLIENT1_ENERGY_CONSUMPTION = new EnergyConsumption(680, 550);
//    public static final EnergyConsumption CLIENT2_ENERGY_CONSUMPTION = new EnergyConsumption(123000, 132600);

    private static ClientDao clientDao = Mockito.mock(ClientDao.class);
    private static EnergyRateDao energyRateDao = Mockito.mock(EnergyRateDao.class);
    private static EnergyConsumptionDao energyConsumptionDao = Mockito.mock(EnergyConsumptionDao.class);
    private static EnergyBillDao energyBillDao = Mockito.mock(EnergyBillDao.class);

    private static ClientService clientService = new ClientService(clientDao);
    private static EnergyConsumptionService energyConsumptionService = new EnergyConsumptionService(energyConsumptionDao, clientService);
    private static EnergyRateService energyRateService;
    private static EnergyBillingService energyBillingService;

    @BeforeAll
    public static void init() throws BillAlreadyExistsException, BillNotFoundException, ClientNotFoundException {
        when(energyRateDao.loadAllRates()).thenReturn(data.RATES);
        energyRateService = new EnergyRateService(energyRateDao, clientService);
        energyBillingService = new EnergyBillingService(energyBillDao, clientService, energyRateService, energyConsumptionService);

        when(clientDao.getById(data.CLIENT1.getId())).thenReturn(data.CLIENT1);
        when(clientDao.getById(data.CLIENT2.getId())).thenReturn(data.CLIENT2);
        when(energyConsumptionDao.getConsumption(eq(data.CLIENT1.getId()), any(LocalDateTime.class), any(LocalDateTime.class))).thenReturn(data.CLIENT1_ENERGY_CONSUMPTION);
        when(energyConsumptionDao.getConsumption(eq(data.CLIENT2.getId()), any(LocalDateTime.class), any(LocalDateTime.class))).thenReturn(data.CLIENT2_ENERGY_CONSUMPTION);
        when(energyBillDao.getById(data.BIL_CLIENT1_202302.id())).thenReturn(data.BIL_CLIENT1_202302);
        doThrow(new BillAlreadyExistsException(data.BIL_CLIENT1_202302)).when(energyBillDao).save(data.BIL_CLIENT1_202302);
    }

    @Test
    public void should_return_bill_by_id() throws BillNotFoundException {
        EnergyBill energyBill = energyBillingService.getBillById(data.BIL_CLIENT1_202302.id());
        assertEquals(data.BIL_CLIENT1_202302, energyBill);
    }
    @Test
    public void should_return_bill_by_client_id_and_month() throws BillNotFoundException {
        EnergyBill energyBill = energyBillingService.getBillOfClientOnMonth(data.CLIENT1.getId(), data.BIL_CLIENT1_202302.year(), data.BIL_CLIENT1_202302.month());
        assertEquals(data.BIL_CLIENT1_202302, energyBill);
    }

    @Test
    public void should_not_found_client() throws BillNotFoundException {
        when(energyBillDao.getById("BIL_EKW51684884_201508")).thenThrow(new BillNotFoundException("BIL_EKW51684884_201508"));
        BillNotFoundException ex = assertThrows(BillNotFoundException.class, () -> energyBillingService.getBillById("BIL_EKW51684884_201508"));
        assertEquals("BIL_EKW51684884_201508", ex.getBillId());
    }
    @Test
    public void input_cant_be_null() {
        InputCannotBeNullException ex = assertThrows(InputCannotBeNullException.class, () -> energyBillingService.getBillById(null));
        assertEquals("'bill id'", ex.getField());
        assertEquals("getting bill by id", ex.getContext());
    }

    @Test
    public void should_generate_bill_id(){
        assertEquals("BIL_EKW11111111_202206", energyBillingService.generateBillId(data.CLIENT1.getId(), 2022, 6));
        assertEquals("BIL_EKW22222222_202304", energyBillingService.generateBillId(data.CLIENT2.getId(), 2023, 4));
    }

    @Test
    public void should_create_energy_bill_object(){
        EnergyBill bill = energyBillingService.createBill(new ClientConsumption(data.CLIENT1, data.CLIENT1_ENERGY_CONSUMPTION), 2022, 6);
        assertEquals("BIL_EKW11111111_202206", bill.id());
        assertEquals(data.CLIENT1, bill.client());
        assertEquals(2022, bill.year());
        assertEquals(6, bill.month());
        assertEquals(calculAmount(data.CLIENT1, data.CLIENT1_ENERGY_CONSUMPTION, EnergyType.GAZ), bill.gazAmount());
        assertEquals(calculAmount(data.CLIENT1, data.CLIENT1_ENERGY_CONSUMPTION, EnergyType.ELECTRICITY), bill.electricityAmount());
    }

    @Test
    public void should_generate_client1_bill() throws ClientNotFoundException, BillAlreadyExistsException, InvalidInputException {
        EnergyBill bill = energyBillingService.generateBill(data.CLIENT1.getId(), 2021, 7);
        verify(energyBillDao, times(1)).save(bill);
    }
    @Test
    public void error_on_generating_already_existing_bill() {
        assertThrows(BillAlreadyExistsException.class, () -> energyBillingService.generateBill(data.CLIENT1.getId(), data.BIL_CLIENT1_202302.year(), data.BIL_CLIENT1_202302.month()));
    }

    private int calculAmount(Client client, EnergyConsumption consumption, EnergyType energyType) {
        return (int)Math.round(data.RATES.get(of(client.getType(), energyType)).amount() * consumption.getConsumption(energyType)*100.0);
    }
}