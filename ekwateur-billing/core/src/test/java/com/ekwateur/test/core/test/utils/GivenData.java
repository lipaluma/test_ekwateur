package com.ekwateur.test.core.test.utils;

import com.ekwateur.test.core.pojos.models.bill.EnergyBill;
import com.ekwateur.test.core.pojos.models.client.Client;
import com.ekwateur.test.core.pojos.models.client.ClientType;
import com.ekwateur.test.core.pojos.models.client.IndividualClient;
import com.ekwateur.test.core.pojos.models.client.ProClient;
import com.ekwateur.test.core.pojos.models.consumption.EnergyConsumption;
import com.ekwateur.test.core.pojos.models.energy.EnergyRate;
import com.ekwateur.test.core.pojos.models.energy.EnergyRateCombination;
import com.ekwateur.test.core.pojos.models.energy.EnergyType;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.ekwateur.test.core.pojos.models.energy.EnergyRateCombination.of;

public class GivenData {

    public Map<EnergyRateCombination, EnergyRate> RATES = new HashMap<>();

    public IndividualClient INDIVIDUAL_CLIENT;
    public ProClient PRO_CLIENT_TESLA;
    public ProClient PRO_CLIENT_LAMBDA;

    public IndividualClient CLIENT1;
    public ProClient CLIENT2;
    public ProClient CLIENT3;

    public EnergyConsumption INDIVIDUAL_CLIENT_ENERGY_CONSUMPTION;
    public EnergyConsumption CLIENT_TESLA_ENERGY_CONSUMPTION;
    public EnergyConsumption CLIENT_LAMBDA_ENERGY_CONSUMPTION;
    public EnergyConsumption CLIENT1_ENERGY_CONSUMPTION;
    public EnergyConsumption CLIENT2_ENERGY_CONSUMPTION;
    public EnergyConsumption CLIENT3_ENERGY_CONSUMPTION;

    public EnergyBill BIL_CLIENT1_202302;

    public Stream<Client> getAllClients() {
        return Stream.of(INDIVIDUAL_CLIENT, PRO_CLIENT_TESLA, PRO_CLIENT_LAMBDA);
    }


    public GivenData withRandomRates() {
        Random random = new Random();
        this.RATES = Stream.of(ClientType.values())
            .flatMap(ct -> Arrays.stream(EnergyType.values()).map(e -> of(e, ct)))
            .collect(Collectors.toMap(Function.identity(), c -> new EnergyRate(random.nextDouble())));
        return this;
    }

    public GivenData withSpecifiedRates(Map<EnergyRateCombination, EnergyRate> rates) {
        this.RATES = rates;
        return this;
    }

    public GivenData withRateOn(EnergyType energyType, ClientType clientType, double amount) {
        this.RATES.put(EnergyRateCombination.of(energyType, clientType), new EnergyRate(amount));
        return this;
    }

    public GivenData withClients() {
        this.INDIVIDUAL_CLIENT = new IndividualClient("EKW11111111", "Mr", "Soares", "Mario");
        this.PRO_CLIENT_TESLA = new ProClient("EKW22222222", "Tesla", "213546868", 400000000l);
        this.PRO_CLIENT_LAMBDA = new ProClient("EKW33333333", "Lambda", "3184353843", 35000l);
        this.CLIENT1 = this.INDIVIDUAL_CLIENT;
        this.CLIENT2 = this.PRO_CLIENT_TESLA;
        this.CLIENT3 = this.PRO_CLIENT_LAMBDA;
        return this;
    }

    public GivenData withClientConsumptions() {
        this.INDIVIDUAL_CLIENT_ENERGY_CONSUMPTION = new EnergyConsumption(680, 550);
        this.CLIENT_TESLA_ENERGY_CONSUMPTION = new EnergyConsumption(123000, 132600);
        this.CLIENT_LAMBDA_ENERGY_CONSUMPTION = new EnergyConsumption(2190, 1870);
        this.CLIENT1_ENERGY_CONSUMPTION = this.INDIVIDUAL_CLIENT_ENERGY_CONSUMPTION;
        this.CLIENT2_ENERGY_CONSUMPTION = this.CLIENT_TESLA_ENERGY_CONSUMPTION;
        this.CLIENT3_ENERGY_CONSUMPTION = this.CLIENT_LAMBDA_ENERGY_CONSUMPTION;
        return this;
    }

    public GivenData withBills() {
        BIL_CLIENT1_202302 = EnergyBill.builder().id("BIL_EKW11111111_202302").client(CLIENT1).year(2023).month(2).gazAmount(130.0).electricityAmount(150.0).build();
        return this;
    }
}
