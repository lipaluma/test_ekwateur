package com.ekwateur.test.core.service;

import com.ekwateur.test.core.dao.ClientDao;
import com.ekwateur.test.core.exceptions.invalidinput.InputCannotBeNullException;
import com.ekwateur.test.core.exceptions.invalidinput.InvalidClientIdException;
import com.ekwateur.test.core.exceptions.invalidinput.InvalidInputException;
import com.ekwateur.test.core.exceptions.notfound.ClientNotFoundException;
import com.ekwateur.test.core.pojos.models.client.Client;
import com.ekwateur.test.core.pojos.models.client.IndividualClient;
import com.ekwateur.test.core.pojos.models.client.ProClient;
import com.ekwateur.test.core.test.utils.GivenData;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class ClientServiceTest {
    private static GivenData data = new GivenData().withClients();

    private static ClientDao clientDao = Mockito.mock(ClientDao.class);
    private static ClientService clientService = new ClientService(clientDao);

    @BeforeAll
    public static void init() throws ClientNotFoundException {
        when(clientDao.getById(data.CLIENT1.getId())).thenReturn(data.CLIENT1);
        when(clientDao.getById(data.CLIENT2.getId())).thenReturn(data.CLIENT2);
        when(clientDao.getById("EKW35181351")).thenThrow(new ClientNotFoundException("EKW35181351"));
        when(clientDao.getLastId()).thenReturn(data.CLIENT3.getId());
    }

    @Test
    public void should_get_individual_client() throws ClientNotFoundException, InvalidInputException {
        Client client = clientService.getClientById(data.CLIENT1.getId());
        assertInstanceOf(IndividualClient.class, client);
        IndividualClient individualClient = (IndividualClient) client;
        assertEquals(data.CLIENT1.getId(), individualClient.getId());
        assertEquals(data.CLIENT1.getCivility(), individualClient.getCivility());
        assertEquals(data.CLIENT1.getFirstname(), individualClient.getFirstname());
        assertEquals(data.CLIENT1.getName(), individualClient.getName());
    }

    @Test
    public void should_get_pro_client() throws ClientNotFoundException, InvalidInputException {
        Client client = clientService.getClientById(data.CLIENT2.getId());
        assertInstanceOf(ProClient.class, client);
        ProClient proClient = (ProClient) client;
        assertEquals(data.CLIENT2.getId(), proClient.getId());
        assertEquals(data.CLIENT2.getCompanyName(), proClient.getCompanyName());
        assertEquals(data.CLIENT2.getSiret(), proClient.getSiret());
        assertEquals(data.CLIENT2.getRevenue(), proClient.getRevenue());
    }

    @Test
    public void should_get_not_found_client() {
        ClientNotFoundException ex = assertThrows(ClientNotFoundException.class, () -> clientService.getClientById("EKW35181351"));
        assertEquals("EKW35181351", ex.getClientId());
    }

    @Test
    public void should_validate_client_id_format_and_nullity() throws InvalidClientIdException, InputCannotBeNullException {
        clientService.validateClientId(data.CLIENT1.getId());
        assertThrows(InvalidClientIdException.class, () -> clientService.validateClientId("11111111"));
        assertThrows(InvalidClientIdException.class, () -> clientService.validateClientId("EKW 11111111"));
        assertThrows(InputCannotBeNullException.class, () -> clientService.validateClientId(null));
    }

    @Test
    public void should_validate_client() throws InvalidInputException {
        clientService.validateClient(new IndividualClient(null, "Mr", "Soares", "Mario"));
        assertThrows(InvalidInputException.class, () -> clientService.validateClient(new IndividualClient(null, "Mr", " ", "Mario")));
        assertThrows(InvalidInputException.class, () -> clientService.validateClient(new IndividualClient(null, "Mr", "Soares", null)));

        clientService.validateClient(new ProClient(null, "Mario & co", "123 456 789", 45220l));
        assertThrows(InvalidInputException.class, () -> clientService.validateClient(new ProClient(null, "Mario & co", "123 456 789", null)));
        assertThrows(InvalidInputException.class, () -> clientService.validateClient(new ProClient(null, "Mario & co", "   ", 45220l)));
        assertThrows(InvalidInputException.class, () -> clientService.validateClient(new ProClient(null, "", "123 456 789", 45220l)));
    }

    @Test
    public void should_save_client() throws InvalidInputException {
        IndividualClient expected = new IndividualClient("EKW33333334", "Mr", "Soares", "Mario");
        assertEquals(expected, clientService.saveClient(new IndividualClient(null, "Mr", "Soares", "Mario")));
        verify(clientDao).save(expected);
    }
}