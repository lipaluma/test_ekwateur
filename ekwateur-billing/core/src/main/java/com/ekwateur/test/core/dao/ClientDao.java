package com.ekwateur.test.core.dao;

import com.ekwateur.test.core.exceptions.notfound.ClientNotFoundException;
import com.ekwateur.test.core.pojos.models.client.Client;

import java.util.List;
import java.util.Optional;

public interface ClientDao {
    Client getById(String clientId) throws ClientNotFoundException;

    Optional<Client> getOptionalById(String clientId);

    List<Client> getAllClients();

    void save(Client c);

    String getLastId();
}
