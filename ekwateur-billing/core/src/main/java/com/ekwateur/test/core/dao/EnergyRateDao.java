package com.ekwateur.test.core.dao;

import com.ekwateur.test.core.exceptions.EkwateurInternalServerErrorException;
import com.ekwateur.test.core.pojos.models.energy.EnergyRate;
import com.ekwateur.test.core.pojos.models.energy.EnergyRateCombination;

import java.util.Map;

public interface EnergyRateDao {

    Map<EnergyRateCombination, EnergyRate> loadAllRates() throws EkwateurInternalServerErrorException;
}
