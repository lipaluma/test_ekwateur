package com.ekwateur.test.core.service;

import com.ekwateur.lib.rop.Tuple;
import com.ekwateur.lib.rop.control.Try;
import com.ekwateur.test.core.dao.EnergyBillDao;
import com.ekwateur.test.core.exceptions.BillAlreadyExistsException;
import com.ekwateur.test.core.exceptions.EkwateurException;
import com.ekwateur.test.core.exceptions.invalidinput.InputCannotBeNullException;
import com.ekwateur.test.core.exceptions.invalidinput.InvalidInputException;
import com.ekwateur.test.core.exceptions.notfound.BillNotFoundException;
import com.ekwateur.test.core.exceptions.notfound.ClientNotFoundException;
import com.ekwateur.test.core.pojos.models.bill.EnergyBill;
import com.ekwateur.test.core.pojos.models.client.Client;
import com.ekwateur.test.core.pojos.models.consumption.EnergyConsumption;
import com.ekwateur.test.core.pojos.models.energy.EnergyRate;
import com.ekwateur.test.core.pojos.models.energy.EnergyType;
import com.ekwateur.test.core.pojos.wrappers.ClientConsumption;

import java.util.Objects;

public class EnergyBillingService {

    private EnergyBillDao energyBillDao;
    private ClientService clientService;
    private EnergyRateService energyRateService;
    private EnergyConsumptionService energyConsumptionService;

    public EnergyBillingService() {}
    public EnergyBillingService(EnergyBillDao energyBillDao, ClientService clientService, EnergyRateService energyRateService, EnergyConsumptionService energyConsumptionService) {
        this.energyBillDao = energyBillDao;
        this.clientService = clientService;
        this.energyRateService = energyRateService;
        this.energyConsumptionService = energyConsumptionService;
    }

    public EnergyBill getBillById(String billId) throws BillNotFoundException {
        return Try.success(billId)
            .tryFilter(Objects::nonNull, () -> new InputCannotBeNullException("'bill id'", "getting bill by id"))
            .map(id -> energyBillDao.getById(id))
            .getSuccess();
    }

    public EnergyBill getBillOfClientOnMonth(String clientId, int year, int month) throws BillNotFoundException {
        return Try.success(clientId)
            .tryFilter(Objects::nonNull, () -> new InputCannotBeNullException("'bill id'", "getting bill of client on month"))
            .map(id -> generateBillId(id, year, month))
            .map(this::getBillById)
            .getSuccess();
    }

    public EnergyBill generateBill(String clientId, int year, int month) throws ClientNotFoundException, BillAlreadyExistsException, InvalidInputException {
//        Client client = clientService.getClientById(clientId);
//        ClientConsumption consumption = energyConsumptionService.getConsumptionOnMonth(client.getId(), year, month);
//        EnergyBill bill = createBill(consumption, year, month);
//        energyBillDao.save(bill);
//        return bill;

//        Client client = null;
//        try {
//            client = clientService.getClientById(clientId);
//        } catch (Exception e) {
//            throw new RuntimeException(e);
//        }
//        if(client == null)
//            throw new ClientNotFoundException(clientId);
//        ClientConsumption consumption = null;
//        try {
//            consumption = energyConsumptionService.getConsumptionOnMonth(client.getId(), year, month);
//            EnergyBill bill = createBill(consumption, year, month);
//            energyBillDao.save(bill);
//            return bill;
//        } catch (BillAlreadyExistsException e) {
//            throw new EkwateurException(e);
//        } catch (Exception e) {
//            throw new RuntimeException(e);
//        }

//        return Optional.ofNullable(clientService.getClientById(clientId)).stream()
//            .map(client -> energyConsumptionService.getConsumptionOnMonth(client.getId(), year, month))
//            .map(cc -> createBill(cc, year, month))
//            .peek(bill -> energyBillDao.save(bill))
//            .findAny()
//            .orElseThrow(() -> new RuntimeException(""));


//        return Try.of(clientId)
//            .flatMap(tryTo(clientService::getClientById))
//            .flatMap(tryTo(client -> energyConsumptionService.getConsumptionOnMonth(client.getId(), year, month)))
//            .map(cc -> createBill(cc, year, month))
//            .flatMap(tryPeek(bill -> energyBillDao.save(bill)))
//            .throwRuntimeIfFailure();


        return Try.to(() -> clientService.getClientById(clientId))
            .throwing(ClientNotFoundException.class).throwing(InvalidInputException.class)
            .add(client -> energyConsumptionService.getConsumptionOnMonth(client.getId(), year, month).consumption())
            .map1((client, consumption) -> createBill(client, consumption, year, month))
            .andThen(bill -> energyBillDao.save(bill))
            .getSuccess();
    }

    EnergyBill createBill(ClientConsumption clientConsumption, int year, int month) {
        EnergyRate electrictyRate = energyRateService.getClientEnergyRate(clientConsumption.client(), EnergyType.ELECTRICITY);
        EnergyRate gazRate =  energyRateService.getClientEnergyRate(clientConsumption.client(), EnergyType.GAZ);
        return EnergyBill.builder()
            .id(generateBillId(clientConsumption.client().getId(), year, month))
            .client(clientConsumption.client())
            .year(year)
            .month(month)
            .gazAmount(clientConsumption.consumption().gazKWh() * gazRate.amount())
            .electricityAmount(clientConsumption.consumption().electricityKWh() * electrictyRate.amount())
            .build();
    }
    EnergyBill createBill(Client client, EnergyConsumption consumption, int year, int month){
        EnergyRate electrictyRate = energyRateService.getClientEnergyRate(client, EnergyType.ELECTRICITY);
        EnergyRate gazRate =  energyRateService.getClientEnergyRate(client, EnergyType.GAZ);
        return EnergyBill.builder()
            .id(generateBillId(client.getId(), year, month))
            .client(client)
            .year(year)
            .month(month)
            .gazAmount(consumption.gazKWh() * gazRate.amount())
            .electricityAmount(consumption.electricityKWh() * electrictyRate.amount())
            .build();
    }

    public String generateBillId(String clientId, int year, int month) {
        return new StringBuilder()
            .append("BIL_")
            .append(clientId)
            .append("_")
            .append(year)
            .append(String.format("%02d", month))
            .toString();
    }

    public void setEnergyRateService(EnergyRateService energyRateService) {
        this.energyRateService = energyRateService;
    }
    public void setClientService(ClientService clientService) {
        this.clientService = clientService;
    }
    public void setEnergyConsumptionService(EnergyConsumptionService energyConsumptionService) {
        this.energyConsumptionService = energyConsumptionService;
    }
    public void setEnergyBillDao(EnergyBillDao energyBillDao) {
        this.energyBillDao = energyBillDao;
    }

}
