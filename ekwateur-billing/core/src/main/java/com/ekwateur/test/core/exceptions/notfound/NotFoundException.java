package com.ekwateur.test.core.exceptions.notfound;

import com.ekwateur.test.core.exceptions.EkwateurException;

public class NotFoundException extends EkwateurException {
    public NotFoundException() {
    }

    public NotFoundException(String message) {
        super(message);
    }

    public NotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public NotFoundException(Throwable cause) {
        super(cause);
    }
}
