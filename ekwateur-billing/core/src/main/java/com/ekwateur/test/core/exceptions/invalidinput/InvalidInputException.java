package com.ekwateur.test.core.exceptions.invalidinput;

import com.ekwateur.test.core.exceptions.EkwateurException;

public class InvalidInputException extends EkwateurException {
    public InvalidInputException() {
    }

    public InvalidInputException(String message) {
        super(message);
    }

    public InvalidInputException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidInputException(Throwable cause) {
        super(cause);
    }
}
