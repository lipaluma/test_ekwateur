package com.ekwateur.test.core.pojos.models.client;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.As;
import com.fasterxml.jackson.annotation.JsonTypeInfo.Id;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Objects;
import java.util.regex.Pattern;

@JsonTypeInfo(use= Id.NAME, include = As.EXISTING_PROPERTY, property = "type")
@JsonSubTypes({
    @JsonSubTypes.Type(value = IndividualClient.class, name = "INDIVIDUAL"),
    @JsonSubTypes.Type(value = ProClient.class, name = "COMPANY")
})
public abstract sealed class Client permits ProClient, IndividualClient {
    private static final Logger log = LoggerFactory.getLogger(Client.class);
    private static final Pattern EKWATEUR_ID_PATTERN = Pattern.compile("EKW[0-9]{8}");

    protected String id;

    protected Client(){}
    protected Client(String id) {
        if(!isIdFormatValid(id))
            log.warn("Client loaded with a bad format of ID : "+id);
        this.id = id;
    }

    public static boolean isIdFormatValid(String id) {
        if(id == null)
            return true;
        return EKWATEUR_ID_PATTERN.matcher(id).matches();
    }

    public String getId() {
        return this.id;
    }

    public abstract ClientType getType();
    public abstract Client copyWithNewId(String id);

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Client client = (Client) o;
        return id.equals(client.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
