package com.ekwateur.test.core.pojos.models.consumption;

import com.ekwateur.test.core.pojos.models.energy.EnergyType;

public record EnergyConsumption(double gazKWh, double electricityKWh) {
    public double getConsumption(EnergyType energyType) {
        return switch (energyType) {
            case GAZ -> gazKWh;
            case ELECTRICITY -> electricityKWh;
        };
    }
}
