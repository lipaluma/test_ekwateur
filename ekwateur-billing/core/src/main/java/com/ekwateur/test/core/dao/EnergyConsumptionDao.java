package com.ekwateur.test.core.dao;

import com.ekwateur.test.core.pojos.models.consumption.EnergyConsumption;

import java.time.LocalDateTime;

public interface EnergyConsumptionDao {
    EnergyConsumption getConsumption(String clientId, LocalDateTime startMonth, LocalDateTime endMonth);
}
