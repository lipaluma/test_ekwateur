package com.ekwateur.test.core.pojos.models.energy;

public record EnergyRate(Double amount) {
}
