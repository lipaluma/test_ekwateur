package com.ekwateur.test.core.pojos.models.bill;

import com.ekwateur.test.core.pojos.models.client.Client;

import java.util.Objects;

public record EnergyBill(String id, Client client, int gazAmount, int electricityAmount, int year, int month) {
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EnergyBill that = (EnergyBill) o;
        return id.equals(that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    public int getTotal(){
        return electricityAmount + gazAmount;
    }

    public static Builder builder() {
        return new Builder();
    }
    public static class Builder {
        private String id;
        private Client client;
        private int gazAmount;
        private int electricityAmount;
        private int year;
        private int month;

        public Builder id(String id) {
            this.id = id;
            return this;
        }
        public Builder client(Client client) {
            this.client = client;
            return this;
        }

        public Builder gazAmount(double gazAmount) {
            this.gazAmount = (int)Math.round(gazAmount*100.0);
            return this;
        }

        public Builder electricityAmount(double electricityAmount) {
            this.electricityAmount = (int)Math.round(electricityAmount*100);
            return this;
        }

        public Builder year(int year) {
            this.year = year;
            return this;
        }

        public Builder month(int month) {
            this.month = month;
            return this;
        }

        public EnergyBill build() {
            return new EnergyBill(id, client, gazAmount, electricityAmount, year, month);
        }
    }
}
