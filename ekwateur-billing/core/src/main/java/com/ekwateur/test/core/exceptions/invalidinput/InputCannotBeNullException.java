package com.ekwateur.test.core.exceptions.invalidinput;

public class InputCannotBeNullException extends InvalidInputException {
    private String field;
    private String context;
    public InputCannotBeNullException(String field, String context) {
        super("The field "+field+" on "+context+" cannot be null.");
        this.field = field;
        this.context = context;
    }

    public String getField() {
        return field;
    }

    public String getContext() {
        return context;
    }
}
