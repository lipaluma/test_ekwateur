package com.ekwateur.test.core.exceptions.notfound;

public class BillNotFoundException extends NotFoundException {
    private String billId;

    public BillNotFoundException(String billId) {
        super("Bill with Id "+billId+" not found");
        this.billId = billId;
    }

    public String getBillId() {
        return billId;
    }
}
