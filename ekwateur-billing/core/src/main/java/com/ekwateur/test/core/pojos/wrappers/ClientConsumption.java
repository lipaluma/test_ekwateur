package com.ekwateur.test.core.pojos.wrappers;

import com.ekwateur.test.core.pojos.models.client.Client;
import com.ekwateur.test.core.pojos.models.consumption.EnergyConsumption;

public record ClientConsumption(Client client, EnergyConsumption consumption) {
}
