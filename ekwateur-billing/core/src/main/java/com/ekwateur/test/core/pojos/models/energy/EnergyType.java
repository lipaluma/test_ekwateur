package com.ekwateur.test.core.pojos.models.energy;

public enum EnergyType {
    GAZ,
    ELECTRICITY
}
