package com.ekwateur.test.core.service;

import com.ekwateur.lib.rop.control.Try;
import com.ekwateur.test.core.dao.EnergyRateDao;
import com.ekwateur.test.core.exceptions.notfound.ClientNotFoundException;
import com.ekwateur.test.core.pojos.models.client.Client;
import com.ekwateur.test.core.pojos.models.client.ClientType;
import com.ekwateur.test.core.pojos.models.energy.EnergyRate;
import com.ekwateur.test.core.pojos.models.energy.EnergyRateCombination;
import com.ekwateur.test.core.pojos.models.energy.EnergyType;

import java.util.Map;

public class EnergyRateService {

    private ClientService clientService;
    private EnergyRateDao energyRateDao;
    private Map<EnergyRateCombination, EnergyRate> rates;

    public EnergyRateService(EnergyRateDao energyRateDao) {
        this.energyRateDao = energyRateDao;
        this.rates = this.energyRateDao.loadAllRates();
    }
    public EnergyRateService(EnergyRateDao energyRateDao, ClientService clientService) {
        this(energyRateDao);
        this.clientService = clientService;
    }

    public void reloadRates() {
        this.rates = this.energyRateDao.loadAllRates();
    }

    public Map<EnergyRateCombination, EnergyRate> getAllRates(){
        return rates;
    }

    public EnergyRate getEnergyRate(ClientType clientType, EnergyType energyType) {
        return rates.get(EnergyRateCombination.of(energyType, clientType));
    }

    public EnergyRate getClientEnergyRate(String clientId, EnergyType energyType) throws ClientNotFoundException {
        return Try.to(() -> clientService.getClientById(clientId)).throwing(ClientNotFoundException.class)
            .map(client -> getEnergyRate(client.getType(), energyType))
            .getSuccess();
    }
    public EnergyRate getClientEnergyRate(Client client, EnergyType energyType) {
        return getEnergyRate(client.getType(), energyType);
    }

    public void setClientService(ClientService clientService) {
        this.clientService = clientService;
    }
}
