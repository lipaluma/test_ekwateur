package com.ekwateur.test.core.exceptions;

public class EkwateurInternalServerErrorException extends RuntimeException {
    public EkwateurInternalServerErrorException() {
    }

    public EkwateurInternalServerErrorException(String message) {
        super(message);
    }

    public EkwateurInternalServerErrorException(String message, Throwable cause) {
        super(message, cause);
    }

    public EkwateurInternalServerErrorException(Throwable cause) {
        super(cause);
    }
}
