package com.ekwateur.test.core.exceptions.invalidinput;

public class InvalidClientIdException extends InvalidInputException {
    private String clientId;
    public InvalidClientIdException(String clientId) {
        super("Client Id "+clientId+" hasn't a format like EKW00000000");
        this.clientId = clientId;
    }

    public String getClientId() {
        return clientId;
    }
}
