package com.ekwateur.test.core.service;

import com.ekwateur.lib.rop.ThrowableFunction0_0;
import com.ekwateur.lib.rop.ThrowableFunction0_1;
import com.ekwateur.lib.rop.Tuple;
import com.ekwateur.lib.rop.control.Try;
import com.ekwateur.test.core.dao.ClientDao;
import com.ekwateur.test.core.exceptions.EkwateurException;
import com.ekwateur.test.core.exceptions.invalidinput.InputCannotBeNullException;
import com.ekwateur.test.core.exceptions.invalidinput.InvalidClientIdException;
import com.ekwateur.test.core.exceptions.invalidinput.InvalidInputException;
import com.ekwateur.test.core.exceptions.notfound.ClientNotFoundException;
import com.ekwateur.test.core.pojos.models.client.Client;
import com.ekwateur.test.core.pojos.models.client.IndividualClient;
import com.ekwateur.test.core.pojos.models.client.ProClient;
import org.apache.commons.lang3.StringUtils;

import java.net.URL;
import java.util.*;


public class ClientService {
    private ClientDao clientDao;

    public ClientService(){}
    public ClientService(ClientDao clientDao){
        this.clientDao = clientDao;
    }

    public List<Client> getAllClients() {
        return clientDao.getAllClients();
    }
    public List<Client> getClients(String... ids) throws EkwateurException {
        return List.of(ids)
            .mapThrowing(this::getClientById)
            .toList();
    }

    public Client getClientByIdByOptional(String clientId) throws ClientNotFoundException, InvalidInputException {
        validateClientId(clientId);
        return clientDao.getOptionalById(clientId).orElseThrow(() -> new ClientNotFoundException(clientId));
    }

    public Client getClientById(String clientId) throws ClientNotFoundException, InvalidInputException {
        validateClientId(clientId);
        return clientDao.getById(clientId);
//        return Try.runThrowing(() -> this.validateClientId(clientId))
//            .map(__ -> clientDao.getById(clientId))
//            .onFailureThrow();
    }

    public Client saveClient(Client client) throws InvalidInputException {
        return Try.runThrowing(() -> validateClient(client))
            .map(__ -> this.copyClientWithGeneratedId(client))
            .andThen(c -> clientDao.save(c))
//            .andThenWhen()
            .getSuccess();
    }

    public Client copyClientWithGeneratedId(Client client) {
        long current = Long.parseLong(clientDao.getLastId().substring(3));
        String newId = String.format("%08d", current + 1);
        return client.copyWithNewId("EKW"+newId);
    }

    public void validateClientId(String clientId) throws InputCannotBeNullException, InvalidClientIdException {
        Try.success(clientId)
            .filter(Objects::nonNull, () -> new InputCannotBeNullException("'client id'", "getting client by id"))
            .filter(Client::isIdFormatValid, InvalidClientIdException::new)
            .onFailureThrow();
    }

    public void validateClient(Client client) throws InvalidInputException {
        Objects.requireNonNull(client);
        if (client instanceof IndividualClient individualClient) {
            validateIndividualClient(individualClient);
        } else {
            validateProClient((ProClient) client);
        }
    }

    private void validateProClient(ProClient client) throws InvalidInputException {
        if(StringUtils.isBlank(client.getCompanyName()))
            throw new InvalidInputException("Company name cannot be empty");
        if(StringUtils.isBlank(client.getSiret()))
            throw new InvalidInputException("Siret cannot be empty");
        if(client.getRevenue() == null)
            throw new InvalidInputException("Revenue cannot be empty");

//        Try.success(client)
//            .filter(c -> StringUtils.isNotBlank(c.getCompanyName()), () -> new InvalidInputException("Company name cannot be empty"))
//            .filter(c -> StringUtils.isNotBlank(c.getSiret()), ()-> new InvalidInputException("Siret cannot be empty"))
//            .filter(c -> c.getRevenue() != null, ()-> new InvalidInputException("Revenue cannot be empty"))
//            .onFailureThrow();
    }

    private void validateIndividualClient(IndividualClient client) throws InvalidInputException {
        Try.success(client)
            .filter(c -> StringUtils.isNotBlank(c.getName()), ()-> new InvalidInputException("Name cannot be empty"))
            .filter(c -> StringUtils.isNotBlank(c.getFirstname()), ()-> new InvalidInputException("Firstname cannot be empty"))
            .onFailureThrow();
    }


    public void setClientDao(ClientDao clientDao) {
        this.clientDao = clientDao;
    }

}
