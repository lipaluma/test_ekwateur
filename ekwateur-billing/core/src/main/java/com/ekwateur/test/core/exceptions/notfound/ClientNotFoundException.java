package com.ekwateur.test.core.exceptions.notfound;

public class ClientNotFoundException extends NotFoundException {
    private String clientId;
    public ClientNotFoundException(String clientId) {
        super("Client with clientId "+clientId+" not found");
        this.clientId = clientId;
    }

    public String getClientId() {
        return clientId;
    }
}
