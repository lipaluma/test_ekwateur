package com.ekwateur.test.core.exceptions;

import com.ekwateur.test.core.pojos.models.bill.EnergyBill;

public class BillAlreadyExistsException extends EkwateurException {
    private EnergyBill bill;
    public BillAlreadyExistsException(EnergyBill bill) {
        super("Bill with id "+bill.id()+" was found in database on saving");
        this.bill = bill;
    }

    public EnergyBill getBill() {
        return bill;
    }
}
