package com.ekwateur.test.core.exceptions;

public class EkwateurException extends Exception {
    public EkwateurException() {
    }

    public EkwateurException(String message) {
        super(message);
    }

    public EkwateurException(String message, Throwable cause) {
        super(message, cause);
    }

    public EkwateurException(Throwable cause) {
        super(cause);
    }
}
