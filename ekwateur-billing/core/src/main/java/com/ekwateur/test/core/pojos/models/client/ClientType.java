package com.ekwateur.test.core.pojos.models.client;

public enum ClientType {
    INDIVIDUAL("INDIVIDUAL"),
    PRO_REVENUE_GT_1M("COMPANY"),
    PRO_REVENUE_LT_1M("COMPANY");

    private String genericType;

    ClientType(String genericType) {
        this.genericType = genericType;
    }

    public String getGenericType() {
        return genericType;
    }
}
