package com.ekwateur.test.core.pojos.models.energy;

import com.ekwateur.test.core.pojos.models.client.ClientType;

public record EnergyRateCombination(EnergyType energyType, ClientType clientType) {
    public static EnergyRateCombination of(EnergyType energyType, ClientType clientType) {
        return new EnergyRateCombination(energyType, clientType);
    }
    public static EnergyRateCombination of(ClientType clientType, EnergyType energyType) {
        return new EnergyRateCombination(energyType, clientType);
    }
}
