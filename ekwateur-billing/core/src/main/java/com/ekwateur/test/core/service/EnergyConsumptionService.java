package com.ekwateur.test.core.service;

import com.ekwateur.lib.rop.control.Try;
import com.ekwateur.test.core.dao.EnergyConsumptionDao;
import com.ekwateur.test.core.exceptions.notfound.ClientNotFoundException;
import com.ekwateur.test.core.pojos.wrappers.ClientConsumption;

import java.time.LocalDate;
import java.time.LocalDateTime;


public class EnergyConsumptionService {

    private EnergyConsumptionDao energyConsumptionDao;

    private ClientService clientService;

    public EnergyConsumptionService(){}
    public EnergyConsumptionService(EnergyConsumptionDao energyConsumptionDao, ClientService clientService) {
        this.energyConsumptionDao = energyConsumptionDao;
        this.clientService = clientService;
    }


    public ClientConsumption getConsumptionOnLastMonth(String clientId) throws ClientNotFoundException {
        LocalDateTime startMonth = LocalDate.now().minusMonths(1).withDayOfMonth(1).atStartOfDay();
        LocalDateTime endMonth = LocalDate.now().withDayOfMonth(1).atStartOfDay().minusSeconds(1);

        return Try.to(() -> clientService.getClientById(clientId)).throwing(ClientNotFoundException.class)
            .tryMap(client -> new ClientConsumption(client, energyConsumptionDao.getConsumption(client.getId(), startMonth, endMonth)))
            .onFailureThrow();
    }
    public ClientConsumption getConsumptionOnMonth(String clientId, int year, int month) throws ClientNotFoundException {
        LocalDateTime startMonth = LocalDate.of(year, month, 1).atStartOfDay();
        LocalDateTime endMonth = LocalDate.of(year, month, 1).atStartOfDay();

        return Try.to(() -> clientService.getClientById(clientId)).throwing(ClientNotFoundException.class)
            .tryMap(client -> new ClientConsumption(client, energyConsumptionDao.getConsumption(client.getId(), startMonth, endMonth)))
            .onFailureThrow();
    }

    public void setEnergyConsumptionDao(EnergyConsumptionDao energyConsumptionDao) {
        this.energyConsumptionDao = energyConsumptionDao;
    }

    public void setClientService(ClientService clientService) {
        this.clientService = clientService;
    }
}
