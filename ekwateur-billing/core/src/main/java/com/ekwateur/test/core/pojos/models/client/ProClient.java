package com.ekwateur.test.core.pojos.models.client;

public final class ProClient extends Client {
    private String companyName;
    private String siret;
    private Long revenue;

    private ProClient(){}
    public ProClient(String id, String companyName, String siret, Long revenue) {
        super(id);
        this.companyName = companyName;
        this.siret = siret;
        this.revenue = revenue;
    }

    @Override
    public ClientType getType() {
        return revenue < 1000000 ? ClientType.PRO_REVENUE_LT_1M : ClientType.PRO_REVENUE_GT_1M;
    }

    @Override
    public Client copyWithNewId(String id) {
        return new ProClient(id, companyName, siret, revenue);
    }

    public String getCompanyName() {
        return this.companyName;
    }

    public String getSiret() {
        return siret;
    }

    public Long getRevenue() {
        return revenue;
    }

    @Override
    public String toString() {
        return "ProClient{" +
            "id='" + id + '\'' +
            ", companyName='" + companyName + '\'' +
            ", siret='" + siret + '\'' +
            ", revenue=" + revenue +
            '}';
    }
}
