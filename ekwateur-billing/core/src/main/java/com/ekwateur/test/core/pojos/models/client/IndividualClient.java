package com.ekwateur.test.core.pojos.models.client;

public final class IndividualClient extends Client {
    private String civility;
    private String name;
    private String firstname;

    private IndividualClient(){}
    public IndividualClient(String id, String civility, String name, String firstname) {
        super(id);
        this.civility = civility;
        this.name = name;
        this.firstname = firstname;
    }

    @Override
    public ClientType getType() {
        return ClientType.INDIVIDUAL;
    }

    @Override
    public Client copyWithNewId(String id) {
        return new IndividualClient(id, civility, name, firstname);
    }

    public String getCivility() {
        return civility;
    }

    public String getName() {
        return name;
    }

    public String getFirstname() {
        return firstname;
    }

    @Override
    public String toString() {
        return "IndividualClient{" +
            "id='" + id + '\'' +
            ", civility='" + civility + '\'' +
            ", name='" + name + '\'' +
            ", firstname='" + firstname + '\'' +
            '}';
    }
}
