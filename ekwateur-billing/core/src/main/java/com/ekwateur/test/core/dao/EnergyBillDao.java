package com.ekwateur.test.core.dao;

import com.ekwateur.test.core.exceptions.BillAlreadyExistsException;
import com.ekwateur.test.core.exceptions.notfound.BillNotFoundException;
import com.ekwateur.test.core.pojos.models.bill.EnergyBill;

public interface EnergyBillDao {
    EnergyBill getById(String billId) throws BillNotFoundException;

    void save(EnergyBill bill) throws BillAlreadyExistsException;
}
