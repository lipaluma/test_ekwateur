package com.ekwateur.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ImportResource;

@SpringBootApplication
@ImportResource("classpath:spring-api.xml")
public class EkwateurBillingApplication {

    public static void main(String[] args) {
        SpringApplication.run(EkwateurBillingApplication.class, args);
    }
}
