CREATE TABLE IF NOT EXISTS t_client (
    id varchar(11) PRIMARY KEY,
    type varchar(20)
);

CREATE TABLE IF NOT EXISTS t_company (
    id varchar(11) PRIMARY KEY,
    company_name varchar(255),
    siret varchar(50),
    revenue BIGINT,
    FOREIGN KEY (id) REFERENCES t_client(id)
);

CREATE TABLE IF NOT EXISTS t_individual (
    id varchar(11) PRIMARY KEY,
    civility varchar(255),
    name varchar(255),
    firstname varchar(255),
    FOREIGN KEY (id) REFERENCES t_client(id)
);

CREATE TABLE IF NOT EXISTS t_energy_rates (
    id INT PRIMARY KEY AUTO_INCREMENT,
    energy_type varchar(255),
    client_type varchar(255),
    amount REAL
);
