insert into t_client(id, type) values ('EKW11111111', 'INDIVIDUAL');
insert into t_client(id, type) values ('EKW22222222', 'COMPANY');
insert into t_client(id, type) values ('EKW33333333', 'COMPANY');


insert into t_individual(id, civility, firstname, name) values ('EKW11111111', 'Mr', 'Mario', 'Soares');

insert into t_company(id, company_name, siret, revenue) values ('EKW22222222', 'Tesla', '32154987', 400000000000l);
insert into t_company(id, company_name, siret, revenue) values ('EKW33333333', 'Tonton & Co', '31684651', 40000l);


insert into t_energy_rates (energy_type, client_type, amount) values ('ELECTRICITY', 'INDIVIDUAL', 0.121);
insert into t_energy_rates (energy_type, client_type, amount) values ('GAZ', 'INDIVIDUAL', 0.115);
insert into t_energy_rates (energy_type, client_type, amount) values ('ELECTRICITY', 'PRO_REVENUE_GT_1M', 0.114);
insert into t_energy_rates (energy_type, client_type, amount) values ('GAZ', 'PRO_REVENUE_GT_1M', 0.111);
insert into t_energy_rates (energy_type, client_type, amount) values ('ELECTRICITY', 'PRO_REVENUE_LT_1M', 0.118);
insert into t_energy_rates (energy_type, client_type, amount) values ('GAZ', 'PRO_REVENUE_LT_1M', 0.113);
