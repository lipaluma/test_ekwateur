package com.ekwateur.test.dao.mybatis.mapper;

import com.ekwateur.test.core.pojos.models.client.Client;
import com.ekwateur.test.core.pojos.models.client.IndividualClient;
import com.ekwateur.test.core.pojos.models.client.ProClient;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface ClientDaoMapper {

    @Select("""
        SELECT c.id, c.type, ci.civility, ci.name, ci.firstname, cc.company_name, cc.siret, cc.revenue
        FROM t_client c
        LEFT JOIN t_individual ci on ci.id = c.id
        LEFT JOIN t_company cc on cc.id = c.id
        WHERE c.id = #{clientId}
        """)
    @TypeDiscriminator(column = "type", cases = {
        @Case(value = "INDIVIDUAL", type = IndividualClient.class),
        @Case(value = "COMPANY", type = ProClient.class)
    })
    Client getById(String clientId);

    @Select("""
        SELECT c.id, c.type, ci.civility, ci.name, ci.firstname, cc.company_name, cc.siret, cc.revenue
        FROM t_client c 
        LEFT JOIN t_individual ci on ci.id = c.id
        LEFT JOIN t_company cc on cc.id = c.id
        """)
    @TypeDiscriminator(column = "type", cases = {
        @Case(value = "INDIVIDUAL", type = IndividualClient.class),
        @Case(value = "COMPANY", type = ProClient.class)
    })
    List<Client> getAllClients();

    @Select("select id from t_client order by id desc limit 1")
    String getLastId();

    @Insert({
        "insert into t_client(id, type) values (#{id}, 'INDIVIDUAL');",
        "insert into t_individual(id, civility, firstname, name) values (#{id}, #{civility}, #{firstname}, #{name});"
    })
    void saveIndividual(IndividualClient client);
    @Insert({"""
        insert into t_client(id, type) values (#{id}, 'COMPANY');
        insert into t_company(id, company_name, siret, revenue) values (#{id}, #{companyName}, #{siret}, #{revenue});
        """,
    })
    void saveCompany(ProClient client);
}
