package com.ekwateur.test.dao.mybatis.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "ekwateur.datasource")
public record EkwateurDatabaseProperties(String url, String username, String password, String driverClassName) {
}
