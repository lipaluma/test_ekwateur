package com.ekwateur.test.dao.mybatis.config;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;

@Configuration
@EnableConfigurationProperties(EkwateurDatabaseProperties.class)
@EnableTransactionManagement
@MapperScan("com.ekwateur.test.dao.mybatis.mapper")
public class MyBatisConfig {

    @Primary
    @Bean
    public DataSource primaryDataSource(EkwateurDatabaseProperties properties) {
        HikariConfig config = new HikariConfig();
        config.setJdbcUrl(properties.url());
        config.setUsername(properties.username());
        config.setPassword(properties.password());
        config.setConnectionTestQuery("SELECT 1");
        return new HikariDataSource(config);
    }
}