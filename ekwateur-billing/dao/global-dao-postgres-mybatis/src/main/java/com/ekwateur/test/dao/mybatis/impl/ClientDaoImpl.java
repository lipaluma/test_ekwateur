package com.ekwateur.test.dao.mybatis.impl;


import com.ekwateur.lib.rop.control.Try;
import com.ekwateur.test.core.dao.ClientDao;
import com.ekwateur.test.core.exceptions.EkwateurInternalServerErrorException;
import com.ekwateur.test.core.exceptions.notfound.ClientNotFoundException;
import com.ekwateur.test.core.pojos.models.client.Client;
import com.ekwateur.test.core.pojos.models.client.ClientType;
import com.ekwateur.test.core.pojos.models.client.IndividualClient;
import com.ekwateur.test.core.pojos.models.client.ProClient;
import com.ekwateur.test.dao.mybatis.mapper.ClientDaoMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

import static com.ekwateur.test.core.pojos.models.client.ClientType.*;

@Repository("clientDao")
public class ClientDaoImpl implements ClientDao {
    private static final Logger log = LoggerFactory.getLogger(ClientDaoImpl.class);
    @Autowired
    private ClientDaoMapper mapper;

    @Autowired
    private PlatformTransactionManager transactionManager;

    public ClientDaoImpl(){}
    ClientDaoImpl(ClientDaoMapper mapper, PlatformTransactionManager transactionManager) {
        this.mapper = mapper;
        this.transactionManager = transactionManager;
    }

    /**
     * <p>Get Client by {@code clientId}. Throws {@link ClientNotFoundException} if the client is not found.</p>
     * <br/>
     * Here is the equivalent code without lib-rop :
     * <pre>{@code
     *         try {
     *             log.info("get by id with MyBatis");
     *             Client client = mapper.getById(clientId);
     *             if(client == null)
     *                 throw new ClientNotFoundException(clientId);
     *             return client;
     *         } catch (Exception e) {
     *             throw new EkwateurInternalServerErrorException(e);
     *         }
     * }</pre>
     *
     * @param clientId the client id of the client to return
     * @return {@link Client} the client corresponding of {@code clientId}
     * @throws ClientNotFoundException if the clientId is not found in database
     */
    @Override
    public Client getById(String clientId) throws ClientNotFoundException {
        log.info("get by id with MyBatis");
        return Try.to(() -> mapper.getById(clientId))
            .map(Optional::ofNullable)
            .getOrElseThrow(EkwateurInternalServerErrorException::new)
            .orElseThrow(() -> new ClientNotFoundException(clientId));
    }

    /**
     * <p>Get Client by {@code clientId} returning an Optional instead of throwing {@link ClientNotFoundException}</p>
     * <br/>
     * Here is the equivalent code without lib-rop :
     * <pre>{@code
     *         try {
     *             log.info("get by id with MyBatis");
     *             Client client = mapper.getById(clientId)
     *             return Optional.ofNullable(client);
     *         } catch (Exception e) {
     *             throw new EkwateurInternalServerErrorException(e);
     *         }
     * }</pre>
     *
     * @param clientId
     * @return {@link Client}
     * @throws ClientNotFoundException
     */
    @Override
    public Optional<Client> getOptionalById(String clientId) {
        log.info("get by id with MyBatis");
        return Try.to(() -> mapper.getById(clientId))
            .map(Optional::ofNullable)
            .getOrElseThrow(EkwateurInternalServerErrorException::new);
    }

    /**
     * <p>Get All Clients</p>
     * <br/>
     * Here is the equivalent code without lib-rop :
     * <pre>{@code
     *         try {
     *             log.info("get all clients with MyBatis");
     *             return mapper.getAllClients();
     *         } catch (Exception e) {
     *             throw new EkwateurInternalServerErrorException(e);
     *         }
     * }</pre>
     *
     * @return List of {@link Client}
     */
    @Override
    public List<Client> getAllClients() {
        log.info("get all clients with MyBatis");
        return Try.to(() -> mapper.getAllClients())
            .getOrElseThrow(EkwateurInternalServerErrorException::new);
    }

    /**
     * <p>Save a client</p>
     * <br/>
     * Here is the equivalent code without lib-rop :
     * <pre>{@code
     *         TransactionStatus txStatus = transactionManager.getTransaction(new DefaultTransactionDefinition());
     *         try {
     *             switch(client.getType()) {
     *                 case INDIVIDUAL -> mapper.saveIndividual((IndividualClient) client);
     *                 default -> mapper.saveCompany((ProClient)client);
     *             }
     *             transactionManager.commit(txStatus);
     *         } catch (Exception e) {
     *             transactionManager.rollback(txStatus);
     *             throw new EkwateurInternalServerErrorException(e);
     *         }
     * }</pre>
     * @param client
     */
    @Override
    public void save(Client client) {
        TransactionStatus txStatus = transactionManager.getTransaction(new DefaultTransactionDefinition());
        Try.success(client.getType())
            .andThenWhen(INDIVIDUAL::equals, () -> mapper.saveIndividual((IndividualClient) client))
            .andThenWhen(type -> !type.equals(INDIVIDUAL), () -> mapper.saveCompany((ProClient) client))
            .andThen(() -> transactionManager.commit(txStatus))
            .onFailure(() -> transactionManager.rollback(txStatus))
            .getOrElseThrow(EkwateurInternalServerErrorException::new);
    }

    /**
     * <p>Get Id of the last client inserted</p>
     * <br/>
     * Here is the equivalent code without lib-rop :
     * <pre>{@code
     *         try {
     *             log.info("get Last Id with MyBatis");
     *             return mapper.getLastId();
     *         } catch (Exception e) {
     *             throw new EkwateurInternalServerErrorException(e);
     *         }
     * }</pre>
     *
     * @return last client id
     */
    @Override
    public String getLastId() {
        log.info("get Last Id with MyBatis");
        return Try.to(() -> mapper.getLastId())
            .getOrElseThrow(EkwateurInternalServerErrorException::new);
    }
}
