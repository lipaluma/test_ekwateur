package com.ekwateur.test.dao.mybatis.impl;

import com.ekwateur.lib.rop.control.Try;
import com.ekwateur.test.core.dao.EnergyRateDao;
import com.ekwateur.test.core.exceptions.EkwateurInternalServerErrorException;
import com.ekwateur.test.core.pojos.models.energy.EnergyRate;
import com.ekwateur.test.core.pojos.models.energy.EnergyRateCombination;
import com.ekwateur.test.dao.mybatis.mapper.EnergyRateDaoMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.Map;

import static com.ekwateur.test.core.pojos.models.energy.EnergyRateCombination.of;
import static java.util.stream.Collectors.toMap;

@Repository("energyRateDao")
public class EnergyRateDaoImpl implements EnergyRateDao {

    private static final Logger log = LoggerFactory.getLogger(EnergyRateDaoImpl.class);
    @Autowired
    private EnergyRateDaoMapper mapper;
    public EnergyRateDaoImpl(){}
    EnergyRateDaoImpl(EnergyRateDaoMapper mapper){
        this.mapper = mapper;
    }


    /**
     * <p>load all rates for all combination of energy and type of client</p>
     * <br/>
     * Here is the equivalent code without lib-rop :
     * <pre>{@code
     *         try {
     *             log.info("load all rates by MyBatis");
     *             return mapper.loadAllRates().stream()
     *                 .collect(toMap(
     *                     r -> of(r.energyType(), r.clientType()),
     *                     r -> new EnergyRate(r.amount())
     *                 ));
     *         } catch (PersistenceException e) {
     *             throw new EkwateurInternalServerErrorException(e);
     *         }
     * }</pre>
     *
     * @return Map of rates
     */
    @Override
    public Map<EnergyRateCombination, EnergyRate> loadAllRates() {
        log.info("load all rates by MyBatis");
        return Try.of(() -> mapper.loadAllRates())
            .map(Collection::stream)
            .map(stream -> stream.collect(toMap(
                r -> of(r.energyType(), r.clientType()),
                r -> new EnergyRate(r.amount())
            )))
            .getOrElseThrow(EkwateurInternalServerErrorException::new);
    }
}
