package com.ekwateur.test.dao.mybatis.mapper.dto;

import com.ekwateur.test.core.pojos.models.client.ClientType;
import com.ekwateur.test.core.pojos.models.energy.EnergyType;

public record RateViewDto(EnergyType energyType, ClientType clientType, double amount) {
}
