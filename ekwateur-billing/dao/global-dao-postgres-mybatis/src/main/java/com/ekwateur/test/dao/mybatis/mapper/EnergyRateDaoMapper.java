package com.ekwateur.test.dao.mybatis.mapper;

import com.ekwateur.test.dao.mybatis.mapper.dto.RateViewDto;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface EnergyRateDaoMapper {

    @Select("select energy_type, client_type, amount from t_energy_rates")
    List<RateViewDto> loadAllRates();
}
