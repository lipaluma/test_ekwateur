package com.ekwateur.test.dao.mybatis.impl;

import com.ekwateur.test.core.dao.ClientDao;
import com.ekwateur.test.core.exceptions.notfound.ClientNotFoundException;
import com.ekwateur.test.core.pojos.models.client.Client;
import com.ekwateur.test.core.pojos.models.client.IndividualClient;
import com.ekwateur.test.core.pojos.models.client.ProClient;
import com.ekwateur.test.dao.mybatis.MyBatisInitializer;
import com.ekwateur.test.dao.mybatis.mapper.ClientDaoMapper;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ClientDaoImplTest {

    @BeforeAll
    public static void init() throws Exception{
        MyBatisInitializer.init(ClientDaoMapper.class);
    }

    private PlatformTransactionManager transactionManager = new DataSourceTransactionManager(MyBatisInitializer.dataSource);
    private ClientDao clientDao = new ClientDaoImpl(MyBatisInitializer.getMapper(ClientDaoMapper.class), transactionManager);

    @Test
    public void should_save_new_individual_client() throws ClientNotFoundException {
        IndividualClient client = new IndividualClient("EKW44444444", "Mr", "Basnier", "Thomas");
        clientDao.save(client);
        assertEquals(client, clientDao.getById("EKW44444444"));
        MyBatisInitializer.reloadData();
    }

    @Test
    public void should_get_individual_client_by_id() throws ClientNotFoundException {
        IndividualClient client = (IndividualClient) clientDao.getById("EKW11111111");
        assertEquals("EKW11111111", client.getId());
        assertEquals("Mr", client.getCivility());
        assertEquals("Soares", client.getName());
        assertEquals("Mario", client.getFirstname());
    }

    @Test
    public void should_get_company_client_by_id() throws ClientNotFoundException {
        ProClient client = (ProClient) clientDao.getById("EKW22222222");
        assertEquals("EKW22222222", client.getId());
        assertEquals("Tesla", client.getCompanyName());
        assertEquals("32154987", client.getSiret());
        assertEquals(400000000000l, client.getRevenue());
    }

    @Test
    public void should_get_last_id(){
        assertEquals("EKW33333333", clientDao.getLastId());
    }

    @Test
    public void should_get_all_clients(){
        List<Client> allClients = clientDao.getAllClients();
        assertEquals(3, allClients.size());
    }

}