package com.ekwateur.test.dao.mybatis;

import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSessionFactory;
import org.flywaydb.core.Flyway;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.mapper.MapperFactoryBean;
import org.springframework.boot.jdbc.DataSourceBuilder;

import javax.sql.DataSource;
import java.util.Arrays;

public class MyBatisInitializer {
    public static DataSource dataSource = DataSourceBuilder.create()
        .driverClassName("org.h2.Driver")
        .url("jdbc:h2:mem:testdb")
        .username("sa")
        .build();
    public static SqlSessionFactory sqlSessionFactory;
    public static Flyway flyway;

    public static void init(Class<?> ... mappers) throws Exception{
        flyway = Flyway.configure().cleanDisabled(false).dataSource(dataSource).load();
        flyway.migrate();

        Configuration configuration = new Configuration();
        configuration.setMapUnderscoreToCamelCase(true);
        SqlSessionFactoryBean sqlSessionFactoryBean = new SqlSessionFactoryBean();
        sqlSessionFactoryBean.setDataSource(dataSource);
        sqlSessionFactoryBean.setConfiguration(configuration);
        sqlSessionFactory = sqlSessionFactoryBean.getObject();
        Arrays.stream(mappers).forEach(m -> sqlSessionFactory.getConfiguration().addMapper(m));
    }

    public static <T > T getMapper(Class<T> mapper) {
        try {
            MapperFactoryBean<T> mapperFactoryBean = new MapperFactoryBean<>();
            mapperFactoryBean.setMapperInterface(mapper);
            mapperFactoryBean.setSqlSessionFactory(sqlSessionFactory);
            return mapperFactoryBean.getObject();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static void reloadData() {
        flyway.clean();
        flyway.migrate();
    }
}
