package com.ekwateur.test.dao.mybatis.impl;

import com.ekwateur.test.core.dao.EnergyRateDao;
import com.ekwateur.test.core.pojos.models.client.ClientType;
import com.ekwateur.test.core.pojos.models.energy.EnergyRate;
import com.ekwateur.test.core.pojos.models.energy.EnergyRateCombination;
import com.ekwateur.test.core.pojos.models.energy.EnergyType;
import com.ekwateur.test.dao.mybatis.MyBatisInitializer;
import com.ekwateur.test.dao.mybatis.mapper.EnergyRateDaoMapper;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.Map;

import static com.ekwateur.test.core.pojos.models.energy.EnergyRateCombination.of;
import static org.junit.jupiter.api.Assertions.assertEquals;

class EnergyRateDaoImplTest {
    @BeforeAll
    public static void init() throws Exception{
        MyBatisInitializer.init(EnergyRateDaoMapper.class);
    }

    private EnergyRateDao energyRateDao = new EnergyRateDaoImpl(MyBatisInitializer.getMapper(EnergyRateDaoMapper.class));

    @Test
    public void should_load_all_rates() {
        Map<EnergyRateCombination, EnergyRate> result = energyRateDao.loadAllRates();
        System.out.println(result);
        assertEquals(0.121, result.get(of(EnergyType.ELECTRICITY, ClientType.INDIVIDUAL)).amount(), 0.0001);
        assertEquals(0.115, result.get(of(EnergyType.GAZ, ClientType.INDIVIDUAL)).amount(), 0.0001);
        assertEquals(0.114, result.get(of(EnergyType.ELECTRICITY, ClientType.PRO_REVENUE_GT_1M)).amount(), 0.0001);
        assertEquals(0.111, result.get(of(EnergyType.GAZ, ClientType.PRO_REVENUE_GT_1M)).amount(), 0.0001);
        assertEquals(0.118, result.get(of(EnergyType.ELECTRICITY, ClientType.PRO_REVENUE_LT_1M)).amount(), 0.0001);
        assertEquals(0.113, result.get(of(EnergyType.GAZ, ClientType.PRO_REVENUE_LT_1M)).amount(), 0.0001);
    }
}