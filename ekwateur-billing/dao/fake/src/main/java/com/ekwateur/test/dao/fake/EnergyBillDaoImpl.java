package com.ekwateur.test.dao.fake;

import com.ekwateur.test.core.dao.EnergyBillDao;
import com.ekwateur.test.core.exceptions.BillAlreadyExistsException;
import com.ekwateur.test.core.exceptions.notfound.BillNotFoundException;
import com.ekwateur.test.core.pojos.models.bill.EnergyBill;

import java.util.HashMap;
import java.util.Map;

public class EnergyBillDaoImpl implements EnergyBillDao {
    private Map<String, EnergyBill> data = new HashMap<>();

    @Override
    public EnergyBill getById(String billId) throws BillNotFoundException {
        EnergyBill energyBill = data.get(billId);
        if(energyBill == null)
            throw new BillNotFoundException(billId);
        return energyBill;
    }

    @Override
    public void save(EnergyBill bill) throws BillAlreadyExistsException {
        EnergyBill energyBill = data.get(bill.id());
        if(energyBill != null)
            throw new BillAlreadyExistsException(bill);
        data.put(bill.id(), bill);
    }
}
