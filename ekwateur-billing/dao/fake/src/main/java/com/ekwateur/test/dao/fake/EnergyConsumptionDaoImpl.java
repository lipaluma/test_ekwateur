package com.ekwateur.test.dao.fake;

import com.ekwateur.test.core.dao.EnergyConsumptionDao;
import com.ekwateur.test.core.pojos.models.consumption.EnergyConsumption;

import java.time.LocalDateTime;
import java.util.Random;

public class EnergyConsumptionDaoImpl implements EnergyConsumptionDao {

    @Override
    public EnergyConsumption getConsumption(String clientId, LocalDateTime startMonth, LocalDateTime endMonth) {
        Random random = new Random();
        return new EnergyConsumption(random.nextInt(500, 2000), random.nextInt(500,2000));
    }
}
