package com.ekwateur.test.dao.fake;

import com.ekwateur.test.core.dao.EnergyRateDao;
import com.ekwateur.test.core.pojos.models.client.ClientType;
import com.ekwateur.test.core.pojos.models.energy.EnergyRate;
import com.ekwateur.test.core.pojos.models.energy.EnergyRateCombination;
import com.ekwateur.test.core.pojos.models.energy.EnergyType;

import java.util.HashMap;
import java.util.Map;

import static com.ekwateur.test.core.pojos.models.energy.EnergyRateCombination.of;

public class EnergyRateDaoImpl implements EnergyRateDao {

    @Override
    public Map<EnergyRateCombination, EnergyRate> loadAllRates() {
        HashMap<EnergyRateCombination, EnergyRate> result = new HashMap<>();
        result.put(of(EnergyType.ELECTRICITY, ClientType.INDIVIDUAL), new EnergyRate(0.121));
        result.put(of(EnergyType.GAZ, ClientType.INDIVIDUAL), new EnergyRate(0.115));
        result.put(of(EnergyType.ELECTRICITY, ClientType.PRO_REVENUE_GT_1M), new EnergyRate(0.114));
        result.put(of(EnergyType.GAZ, ClientType.PRO_REVENUE_GT_1M), new EnergyRate(0.111));
        result.put(of(EnergyType.ELECTRICITY, ClientType.PRO_REVENUE_LT_1M), new EnergyRate(0.118));
        result.put(of(EnergyType.GAZ, ClientType.PRO_REVENUE_LT_1M), new EnergyRate(0.113));
        return result;
    }
}
