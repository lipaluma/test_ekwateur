package com.ekwateur.test.dao.fake;

import com.ekwateur.test.core.dao.ClientDao;
import com.ekwateur.test.core.exceptions.notfound.ClientNotFoundException;
import com.ekwateur.test.core.pojos.models.client.Client;
import com.ekwateur.test.core.pojos.models.client.IndividualClient;
import com.ekwateur.test.core.pojos.models.client.ProClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

public class ClientDaoImpl implements ClientDao {
    private static Logger log = LoggerFactory.getLogger(ClientDaoImpl.class);

    private Map<String, Client> data = new HashMap<>();

    public ClientDaoImpl(){
        data.put("EKW11111111", new IndividualClient("EKW11111111", "Mr", "Mario", "Soares"));
        data.put("EKW22222222", new ProClient("EKW22222222", "Tesla", "32154987", 400000000000l));
        data.put("EKW33333333", new ProClient("EKW33333333", "Tonton & Co", "31684651", 40000l));
    }

    @Override
    public Client getById(String clientId) throws ClientNotFoundException {
        log.info("get by id with Fake");
        Client client = data.get(clientId);
        if(client == null)
            throw new ClientNotFoundException(clientId);
        return client;
    }

    @Override
    public Optional<Client> getOptionalById(String clientId) {
        return Optional.empty();
    }

    @Override
    public List<Client> getAllClients() {
        return new ArrayList<>(data.values());
    }

    @Override
    public void save(Client client) {
        data.put(client.getId(), client);;
    }

    @Override
    public String getLastId() {
        return data.keySet().stream().sorted(Comparator.reverseOrder()).findFirst().get();
    }

}
