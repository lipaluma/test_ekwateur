# Energy Billing

'Energy Billing' is an example of a backend API managing bills for entreprise that deals with energys.
This application is written in JAVA in a hexagonal architecture and designed by the concept of DDD.

The version in the <b>master</b> includes java 9 modules.<br/>
It exists a version without in branch <b>feat/without-modules-java9</b>

## Installation

First of all, start by clonnig the projet :

```bash
cd /good/path
git clone https://gitlab.com/lipaluma/test_ekwateur.git
cd test_ekwateur
```

Then, you need to build the application.
We have a lib called lib-rop to build first : 
```bash
cd libs/lib-rop
mvn clean install
cd ../..
```
And the application
```bash
cd ekwateur_billing
mvn clean install
```

Now, you could launch the projet by docker :
```bash
cd api/api-spring-mvc
docker build . -t billing
docker run -p 8080:8080 billing
```

Or, by java 17 :
```bash
java -jar api/api-spring-mvc/target/ekwateur-billing-1.0-SNAPSHOT.jar
```

## Testing API

I have created a postman file you can find in the module api :
```bash
less api/Ekwateur Billing.postman_collection.json
```
Import it in postman.<br/>
The postman version used for the export : Postman v10.11.1
