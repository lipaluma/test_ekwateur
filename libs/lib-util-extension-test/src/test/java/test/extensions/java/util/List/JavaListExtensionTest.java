package test.extensions.java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import java.util.List;

import static org.assertj.core.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertThrows;

class JavaListExtensionTest {

    @Test
    void should_map_on_success_with_declaring_exception() throws ClassNotFoundException {
        List<? extends Class<?>> list = List.of("java.lang.String", "java.lang.Integer").mapThrowing(Class::forName).toList();
        assertThat(list).isEqualTo(List.of(String.class, Integer.class));
    }
    @Test
    void should_throws_ClassNotFoundException() {
        assertThrows(ClassNotFoundException.class, () -> List.of("java.lang.String", "NotExists").mapThrowing(Class::forName).toList());
    }
    @Test
    void should_tryMap_on_success_without_declaring_exception() {
        List<? extends Class<?>> list = List.of("java.lang.String", "java.lang.Integer").tryMap(Class::forName).toList();
        assertThat(list).isEqualTo(List.of(String.class, Integer.class));
    }

}