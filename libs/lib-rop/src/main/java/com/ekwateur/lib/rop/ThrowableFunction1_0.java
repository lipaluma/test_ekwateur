package com.ekwateur.lib.rop;

import java.io.Serializable;
import java.util.Objects;

/**
 * Represents a function with one argument and no returning values.
 *
 * @param <X> the exception which may throw
 * @param <T1> argument 1 of the function
 */
@FunctionalInterface
public interface ThrowableFunction1_0<X extends Throwable, T1> extends Serializable {
    /**
     * The <a href="https://docs.oracle.com/javase/8/docs/api/index.html">serial version uid</a>.
     */
    long serialVersionUID = 1L;

    /**
     * Performs this operation on the given argument.
     *
     * @param t1 the input argument
     */
    void apply(T1 t1) throws X;

    /**
     * Narrows the given {@code ThrowableFunction1_0<? extends Throwable, ? super T1>} to
     * {@code ThrowableFunction1_0<? extends Throwable,T1>}
     *
     * @param f A {@code ThrowableFunction1_0}
     * @param <T1> The parameter type
     * @return the given {@code f} instance as narrowed type {@code ThrowableFunction1_0<? extends Throwable,T1>}
     */
    @SuppressWarnings("unchecked")
    static <T1> ThrowableFunction1_0<? extends Throwable, T1> narrow(ThrowableFunction1_0<? extends Throwable, ? super T1> f) {
        return (ThrowableFunction1_0<? extends Throwable, T1>) f;
    }

    /**
     * Returns a composed function that first applies this function to
     * its input, and then applies the {@code after} function to the result.
     * If evaluation of either function throws an exception, it is relayed to
     * the caller of the composed function.
     *
     * @param after the function to apply after this function is applied
     * @return a composed function that first applies this function and then
     * applies the {@code after} function
     * @throws NullPointerException if after is null
     */
    default ThrowableFunction1_0<X, T1> andThen(ThrowableFunction0_0<X> after) {
        Objects.requireNonNull(after);
        return t1 -> {
            apply(t1);
            after.apply();
        };
    }
}
