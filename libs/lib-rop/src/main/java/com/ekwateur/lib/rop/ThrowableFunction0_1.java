package com.ekwateur.lib.rop;

import java.io.Serializable;
import java.util.Objects;

/**
 * Represents a function with no arguments and returning one value.
 *
 * @param <X> the exception which may throw
 * @param <R> return type 1 of the function
 */
@FunctionalInterface
public interface ThrowableFunction0_1<X extends Throwable, R> extends Serializable {

    /**
     * The <a href="https://docs.oracle.com/javase/8/docs/api/index.html">serial version uid</a>.
     */
    long serialVersionUID = 1L;

    /**
     * Gets a result.
     *
     * @return a result
     */
    R apply() throws X;

    /**
     * Narrows the given {@code ThrowableFunction0_1<? extends Throwable, ? extends R>} to
     * {@code ThrowableFunction0_1<? extends Throwable,R>}
     *
     * @param f A {@code ThrowableFunction0_1}
     * @param <R> The return type
     * @return the given {@code f} instance as narrowed type {@code ThrowableFunction0_1<? extends Throwable,R>}
     */
    @SuppressWarnings("unchecked")
    static <R> ThrowableFunction0_1<? extends Throwable, R> narrow(ThrowableFunction0_1<? extends Throwable, ? extends R> f) {
        return (ThrowableFunction0_1<? extends Throwable, R>) f;
    }

    /**
     * Returns a composed function that first applies this function to
     * its input, and then applies the {@code after} function to the result.
     * If evaluation of either function throws an exception, it is relayed to
     * the caller of the composed function.
     *
     * @param <V> the type of output of the {@code after} function, and of the
     *           composed function
     * @param after the function to apply after this function is applied
     * @return a composed function that first applies this function and then
     * applies the {@code after} function
     * @throws NullPointerException if after is null
     */
    default <V> ThrowableFunction0_1<X, V> andThen(ThrowableFunction1_1<X, ? super R, ? extends V> after) {
        Objects.requireNonNull(after);
        return () -> after.apply(apply());
    }

}
