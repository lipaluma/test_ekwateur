package com.ekwateur.lib.rop;

/**
 * A tuple of one element which can be seen as cartesian product of one component.
 *
 * @param <T1> type of the 1st element
 */
public record Tuple1<T1>(T1 t1) implements Tuple {

}
