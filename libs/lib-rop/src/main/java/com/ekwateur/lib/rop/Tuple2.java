package com.ekwateur.lib.rop;

/**
 * A tuple of two elements which can be seen as cartesian product of two components.
 *
 * @param <T1> type of the 1st element
 * @param <T2> type of the 2nd element
 */
public record Tuple2<T1, T2>(T1 t1, T2 t2) implements Tuple {
}
