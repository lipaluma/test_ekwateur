package com.ekwateur.lib.rop.control;

import com.ekwateur.lib.rop.*;

import java.io.Serializable;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.function.*;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static com.ekwateur.lib.rop.control.TryUtils.isFatal;
import static com.ekwateur.lib.rop.control.TryUtils.sneakyThrow;
import static java.util.stream.Collectors.*;

public interface Try<S> extends Serializable {

    /**
     * Creates a {@link Failure} that contains the given {@code exception}.
     *
     * <pre>{@code
     * // Returns Failure(NumberFormatException)
     * Try<Integer> failure = Try.failure(new NumberFormatException());
     * }</pre>
     *
     * @param exception An exception.
     * @param <S>       Component type of the {@code Try}.
     * @return A new {@code Failure}.
     */
    static <S> Try1<S> failure(Throwable exception) {
        return new Try1.Failure<>(exception);
    }
    /**
     * equivalent of {@link #success(Object)}
     */
    static <T1> Try1<T1> of(T1 value) {
        return new Try1.Success<>(value);
    }
    /**
     * Creates a {@link Success} with the value of an optional. If empty, create a {@link Failure} of
     * a {@link NoSuchElementException}
     *
     * <pre>{@code
     * // Returns Success(123)
     * Try.Try1<Integer> success = Try.ofOptional(Optional.of(123));
     *
     * // Returns Failure(NoSuchElementException)
     * Try<Integer> failure = Try.<Integer>ofOptional(Optional.empty());
     * }</pre>
     *
     * @param option An {@link Optional}.
     * @param <T1>   Type of the given {@code value} in the {@link Optional}.
     * @return A new {@code Success}.
     */
    static <T1> Try1<T1> ofOptional(Optional<T1> option) {
        return
            option.isPresent() ? new Try1.Success<>(option.get()) :
            new Try1.Failure<>(new NoSuchElementException("optional empty"));
    }
    /**
     * Creates a {@link Success} with null as success value.
     *
     * <pre>{@code
     * // Returns Success(null)
     * Try.Try1 success = Try.success();
     * }</pre>
     *
     * @return A new {@code Success}.
     */
    static Try1<Void> success() {
        return new Try1.Success<>(null);
    }
    /**
     * Creates a {@link Success} that contains the given {@code value}.
     *
     * <pre>{@code
     * // Returns Success(123)
     * Try.Try1<Integer> success = Try.success(123);
     * }</pre>
     *
     * @param value A value.
     * @param <T1>   Type of the given {@code value}.
     * @return A new {@code Success}.
     */
    static <T1> Try1<T1> success(T1 value) {
        return of(value);
    }
    /**
     * equivalent of {@link #success(Object, Object)}
     */
    static <T1, T2> Try2<T1, T2> of(T1 t1, T2 t2) {
        return new Try2.Success<>(t1, t2);
    }

    /**
     * Creates a {@link Success} that contains two given values {@code t1}, {@code t2} wrapped in a {@link Tuple2}.
     *
     * <pre>{@code
     * // Returns Success(Tuple2.of(123, "Hello"))
     * Try.Try2<Integer, String> success = Try.success(123, "Hello");
     * }</pre>
     *
     * @param t1 first value.
     * @param t2 second value.
     * @param <T1>   Type of the first value.
     * @param <T2>   Type of the second value.
     * @return A new {@code Success}.
     */
    static <T1, T2> Try2<T1, T2> success(T1 t1, T2 t2) {
        return of(t1, t2);
    }
    /**
     * equivalent of {@link #success(Tuple2)} by passing a Tuple
     */
    static <T1, T2> Try2<T1, T2> of(Tuple2<T1, T2> tuple) {
        return of(tuple.t1(), tuple.t2());
    }
    /**
     * equivalent of {@link #success(Object, Object)} by passing directly a {@link Tuple}
     */
    static <T1, T2> Try2<T1, T2> success(Tuple2<T1, T2> tuple) {
        return of(tuple.t1(), tuple.t2());
    }

    /**
     * Creates a Try.Try1Iterable containing an {@link Iterable} of values.
     *
     * <pre>{@code
     * // Returns Success(List.of(12, 15, 6, 18))
     * Try.Try1Iterable<List<Integer>> success = Try.iterateOn(List.of(12, 15, 6, 18));
     * }</pre>
     *
     * @throws X Exception that supplier can throw
     * @param supplier supplier providing an {@link Iterable}
     * @param <IT> Type of the {@link Iterable}
     * @param <T> Type of elements of the given {@link Iterable}
     * @return A new {@code Success}
     */
    static <X extends Throwable, IT extends Iterable<T>, T> Try1Iterable<IT, T> iterateOnThrowing(ThrowableFunction0_1<X, IT> supplier) throws X {
        Objects.requireNonNull(supplier, "supplier is null");
        return Try.iterateOn(supplier);
    }

    static <IT extends Iterable<T>, T> Try1Iterable<IT, T> iterateOn(ThrowableFunction0_1<? extends Throwable, IT> supplier) {
        Objects.requireNonNull(supplier, "supplier is null");
        try {
            return new Try1Iterable.Success<>(supplier.apply());
        } catch (Throwable e) {
            return new Try1Iterable.Failure<>(e);
        }
    }

    /**
     * Creates a Try1 from {@link ThrowableFunction1_1} using a try-with-resource {@link AutoCloseable} returned by
     * the supplier.
     *
     * <pre>{@code
     * // Returns Success(null)
     * Try.ofWithResource(() -> new FileInputStream("existing_file.txt"), (fis) -> getValueFromFile(fis));
     *
     * // Returns Failure(FileNotFoundException)
     * Try.ofWithResource(() -> new FileInputStream("not_existing_file.txt"), (fis) -> getValueFromFile(fis));
     *
     * // Returns Failure(Exception)
     * Try.ofWithResource(() -> new FileInputStream("existing_file.txt"), () -> {
     *   throw new Exception("An unknown error occurred.");
     * });
     * }</pre>
     *
     * @param supplier A throwable function that supplies the try-with-resource
     * @param function A throwable function that retrieve the success value using the supplying resource
     * @return {@code Success(null)} if no exception occurs, otherwise {@code Failure(throwable)} if an exception occurs
     * calling {@code runnable.apply()}.
     */
    static <T1 extends AutoCloseable, R> Try1<R> ofWithResource(ThrowableFunction0_1<? extends Throwable, ? extends T1> supplier, ThrowableFunction1_1<? extends Throwable, ? super T1, ? extends R> function) {
        Objects.requireNonNull(supplier, "supplier is null");
        Objects.requireNonNull(function, "function is null");
        return toWithResource(supplier, function);
    }
    /**
     * equivalent of {@link #ofWithResource(ThrowableFunction0_1, ThrowableFunction1_1)}
     */
    static <T1 extends AutoCloseable, R> Try1<R> toWithResource(ThrowableFunction0_1<? extends Throwable, ? extends T1> supplier, ThrowableFunction1_1<? extends Throwable, ? super T1, ? extends R> function) {
        Objects.requireNonNull(supplier, "supplier is null");
        Objects.requireNonNull(function, "function is null");
        return Try.to(() -> {
            try (T1 t1 = supplier.apply()) {
                return function.apply(t1);
            }
        });
    }
    /**
     * equivalent of {@link #ofWithResourceThrowing(ThrowableFunction0_1, ThrowableFunction1_1)} but declares exception that function can throw
     * @throws X1 Exception that supplier can throw
     * @throws X2 Exception that function can throw
     */
    static <X1 extends Throwable, X2 extends Throwable, T1 extends AutoCloseable, R>
        Try1<R> toWithResourceThrowing(
            ThrowableFunction0_1<X1, ? extends T1> supplier,
            ThrowableFunction1_1<X2, ? super T1, ? extends R> function) throws X1, X2 {
        Objects.requireNonNull(supplier, "supplier is null");
        Objects.requireNonNull(function, "function is null");
        return toWithResource(supplier, function);
    }

    /**
     * equivalent of {@link #ofWithResource(ThrowableFunction0_1, ThrowableFunction1_1)} but declares exception that function can throw
     * @throws X1 Exception that supplier can throw
     * @throws X2 Exception that function can throw
     */
    static <X1 extends Throwable, X2 extends Throwable, T1 extends AutoCloseable, R>
        Try1<R> ofWithResourceThrowing(
            ThrowableFunction0_1<X1, ? extends T1> supplier,
            ThrowableFunction1_1<X2, ? super T1, ? extends R> function) throws X1, X2 {
        Objects.requireNonNull(supplier, "supplier is null");
        Objects.requireNonNull(function, "function is null");
        return toWithResourceThrowing(supplier, function);
    }

    /**
     * Creates a Try1 from {@link ThrowableFunction2_1} using two try-with-resources {@link AutoCloseable} returned by
     * two suppliers.
     *
     * <pre>{@code
     * // Returns Success(null)
     * Try.ofWithResources(() -> new FileInputStream("file1.txt"), () -> new FileInputStream("file2.txt"),
     *      (fis1, fis2) -> getValueFromFiles(fis1, fis2));
     *
     * // Returns Failure(FileNotFoundException)
     * Try.ofWithResources(() -> new FileInputStream("not_exist.txt"), () -> new FileInputStream("file2.txt"),
     *      (fis1, fis2) -> getValueFromFiles(fis1, fis2));
     *
     * // Returns Failure(Exception)
     * Try.ofWithResources(() -> new FileInputStream("not_exist.txt"), () -> new FileInputStream("file2.txt"), (fis1, fis2) ->  {
     *   throw new Exception("An unknown error occurred.");
     * });
     * }</pre>
     *
     * @param supplier1 A throwable function that supplies the first try-with-resource
     * @param supplier2 A throwable function that supplies the second try-with-resource
     * @param function A throwable function that retrieve the success value using the supplying resources
     * @return {@code Success(null)} if no exception occurs, otherwise {@code Failure(throwable)} if an exception occurs
     * calling {@code runnable.apply()}.
     */
    static <T1 extends AutoCloseable, T2 extends AutoCloseable, R> Try1<R> ofWithResources(
        ThrowableFunction0_1<? extends Throwable, ? extends T1> supplier1,
        ThrowableFunction0_1<? extends Throwable, ? extends T2> supplier2,
        ThrowableFunction2_1<? extends Throwable, ? super T1, ? super T2, ? extends R> function
    ) {
        Objects.requireNonNull(supplier1, "supplier is null");
        Objects.requireNonNull(supplier2, "supplier is null");
        Objects.requireNonNull(function, "function is null");
        return Try.toWithResources(supplier1, supplier2, function);
    }

    /**
     * equivalent of {@link #ofWithResources(ThrowableFunction0_1, ThrowableFunction0_1, ThrowableFunction2_1)}
     */
    static <T1 extends AutoCloseable, T2 extends AutoCloseable, R> Try1<R> toWithResources(
        ThrowableFunction0_1<? extends Throwable, ? extends T1> supplier1,
        ThrowableFunction0_1<? extends Throwable, ? extends T2> supplier2,
        ThrowableFunction2_1<? extends Throwable, ? super T1, ? super T2, ? extends R> function
    ) {
        Objects.requireNonNull(supplier1, "supplier is null");
        Objects.requireNonNull(supplier2, "supplier is null");
        Objects.requireNonNull(function, "function is null");
        return Try.to(() -> {
            try (T1 t1 = supplier1.apply(); T2 t2 = supplier2.apply()) {
                return function.apply(t1, t2);
            }
        });
    }
    /**
     * equivalent of {@link #ofWithResourcesThrowing(ThrowableFunction0_1, ThrowableFunction0_1, ThrowableFunction2_1)}
     * but declares exception that function can throw
     * @throws X1 Exception that first supplier can throw
     * @throws X2 Exception that second supplier can throw
     * @throws X3 Exception that function can throw
     */
    static <X1 extends Throwable, X2 extends Throwable, X3 extends Throwable, T1 extends AutoCloseable, T2 extends AutoCloseable, R>
        Try1<R> toWithResourcesThrowing(
        ThrowableFunction0_1<X1, ? extends T1> supplier1,
        ThrowableFunction0_1<X2, ? extends T2> supplier2,
        ThrowableFunction2_1<X3, ? super T1, ? super T2, ? extends R> function
    ) throws X1, X2, X3 {
        return Try.toWithResources(supplier1, supplier2, function);
    }

    /**
     * equivalent of {@link #ofWithResources(ThrowableFunction0_1, ThrowableFunction0_1, ThrowableFunction2_1)}
     * but declares exception that function can throw
     * @throws X1 Exception that first supplier can throw
     * @throws X2 Exception that second supplier can throw
     * @throws X3 Exception that function can throw
     */
    static <X1 extends Throwable, X2 extends Throwable, X3 extends Throwable, T1 extends AutoCloseable, T2 extends AutoCloseable, R>
        Try1<R> ofWithResourcesThrowing(
        ThrowableFunction0_1<X1, ? extends T1> supplier1,
        ThrowableFunction0_1<X2, ? extends T2> supplier2,
        ThrowableFunction2_1<X3, ? super T1, ? super T2, ? extends R> function
    ) throws X1, X2, X3 {
        return Try.toWithResourcesThrowing(supplier1, supplier2, function);
    }

    /**
     * Creates a Try1 of a {@link ThrowableFunction0_0}.
     *
     * <pre>{@code
     * // Returns Success(null)
     * Try.run(() -> doSomeAction());
     *
     * // Returns Failure(Exception)
     * Try.run(() -> {
     *   throw new Exception("An unknown error occurred.");
     * });
     * }</pre>
     *
     * @param runnable A checked runnable
     * @return {@code Success(null)} if no exception occurs, otherwise {@code Failure(throwable)} if an exception occurs
     * calling {@code runnable.apply()}.
     */
    static Try1<Void> run(ThrowableFunction0_0<? extends Throwable> runnable) {
        Objects.requireNonNull(runnable, "runnable is null");
        try {
            runnable.apply();
            return new Try1.Success<>(null); // null represents the absence of an value, i.e. Void
        } catch (Throwable t) {
            return new Try1.Failure<>(t);
        }
    }
    /**
     * equivalent of {@link #run(ThrowableFunction0_0)} but declares exception that runnable can throw
     * @throws X Exception that runnable can throw
     */
    static <X extends Throwable> Try1<Void> runThrowing(ThrowableFunction0_0<X> runnable) throws X {
        return run(runnable);
    }

    /**
     * equivalent of {@link #of(ThrowableFunction0_1)}
     */
    static <R1> Try1<R1> to(ThrowableFunction0_1<? extends Throwable, R1> supplier) {
        Objects.requireNonNull(supplier, "supplier is null");
        try {
            return new Try1.Success<>(supplier.apply());
        } catch (Throwable e) {
            return new Try1.Failure<>(e);
        }
    }
    /**
     * Creates a Try1 of a {@link ThrowableFunction0_1} returning one success value.
     *
     * <pre>{@code
     * // Returns Success(String.class)
     * Try.of(() -> Class.forName("java.lang.String"));
     *
     * // Returns Failure(ClassNotFoundException)
     * Try.of(() -> Class.forName("not a class"));
     * }</pre>
     *
     * @param supplier A throwable supplier
     * @param <R1>     Component type
     * @return {@code Success(supplier.apply())} if no exception occurs, otherwise {@code Failure(throwable)} if an
     * exception occurs calling {@code supplier.apply()}.
     */
    static <R1> Try1<R1> of(ThrowableFunction0_1<? extends Throwable, R1> supplier) {
        Objects.requireNonNull(supplier, "supplier is null");
        return to(supplier);
    }
    /**
     * equivalent of {@link #ofThrowing(ThrowableFunction0_1)} but declares exception that supplier can throw
     *
     * @throws X Exception that supplier can throw
     */
    static <X extends Throwable, R1> Try1<R1> toThrowing(ThrowableFunction0_1<X, R1> supplier) throws X {
        Objects.requireNonNull(supplier, "supplier is null");
        return to(supplier);
    }
    /**
     * equivalent of {@link #of(ThrowableFunction0_1)} but declares exception that supplier can throw
     *
     * @throws X Exception that function can throw
     */
    static <X extends Throwable, R1> Try1<R1> ofThrowing(ThrowableFunction0_1<X, R1> supplier) throws X {
        Objects.requireNonNull(supplier, "supplier is null");
        return to(supplier);
    }
    /**
     * equivalent of {@link #of(ThrowableFunction0_2)} but declares exception that supplier can throw
     */
    static <R1, R2> Try2<R1, R2> to(ThrowableFunction0_2<? extends Throwable, R1, R2> function) {
        Objects.requireNonNull(function, "function is null");
        try {
            return new Try2.Success<>(function.apply());
        } catch (Throwable e) {
            return new Try2.Failure<>(e);
        }
    }

    /**
     * Creates a Try2 of a {@link ThrowableFunction0_2} returning two success values by a Tuple.
     *
     * <pre>{@code
     * // Returns Success(Tuple.of(123, "Hello"))
     * Try.of(() -> Tuple.of(123, "Hello"));
     * }</pre>
     *
     * @param function A throwable supplier of a Tuple2 of values
     * @param <R1>     type of first success value
     * @param <R2>     type of second success value
     * @return {@code Success(function.apply())} returning a {@link Tuple2} if no exception occurs,
     * otherwise {@code Failure(throwable)} if an exception occurs calling {@code function.apply()}.
     */
    static <R1, R2> Try2<R1, R2> of(ThrowableFunction0_2<? extends Throwable, R1, R2> function) {
        Objects.requireNonNull(function, "function is null");
        return to(function);
    }

    /**
     * equivalent of {@link #ofThrowing(ThrowableFunction0_2)} but declares exception that function can throw
     *
     * @throws X Exception that function can throw
     */
    static <X extends Throwable, R1, R2> Try2<R1, R2> toThrowing(ThrowableFunction0_2<X, R1, R2> function) throws X {
        return to(function);
    }
    /**
     * Creates a Try2 of a {@link ThrowableFunction0_2} returning two success values by a Tuple and declares exception
     * that {@code function.apply()} can throw.
     *
     * <pre>{@code
     * // Returns Success(Tuple.of(123, "Hello"))
     * Try.ofThrowing(() -> Tuple.of(123, "Hello"));
     * }</pre>
     *
     * @throws X     Exception that function can throw
     * @param function A throwable supplier of a Tuple2 of values
     * @param <R1>    Type of first success value
     * @param <R2>    Type of second success value
     * @return {@code Success(function.apply())} returning a {@link Tuple2} if no exception occurs,
     * otherwise {@code Failure(throwable)} if an exception occurs calling {@code function.apply()}.
     */
    static <X extends Throwable, R1, R2> Try2<R1, R2> ofThrowing(ThrowableFunction0_2<X, R1, R2> function) throws X {
        return toThrowing(function);
    }

    /**
     * Gets the result of this Try if this is a {@code Success} or throws if this is a {@code Failure}.
     * <p>
     * <strong>IMPORTANT! If this is a {@link Failure}, the underlying {@code cause} of type {@link Throwable} is thrown.</strong>
     * <p>
     * The thrown exception is exactly the same as the result of {@link #getFailure()}.
     *
     * @return The result of this {@code Try}.
     */
    S getSuccess();
    /**
     * Gets the failure exception if this is a Failure or throws if this is a Success.
     *
     * @return The exception cause if this is a Failure
     * @throws UnsupportedOperationException if this is a Success
     */
    Throwable getFailure();
    /**
     * Checks if this is a Success.
     *
     * @return true, if this is a Success, otherwise false, if this is a Failure
     */
    boolean isSuccess();
    /**
     * Checks if this is a Failure.
     *
     * @return true, if this is a Failure, otherwise false, if this is a Success
     */
    boolean isFailure();

    /**
     * Converts this to an {@link java.util.Optional}.
     *
     * <pre>{@code
     * // returns an Optional.empty
     * Try.of(() -> { throw new Error(); })
     *       .toJavaOptional()
     *
     * // = Optional[ok]
     * Try.of(() -> "ok")
     *     .toJavaOptional()
     * }</pre>
     *
     * @return A new {@link java.util.Optional}.
     */
    Optional<S> toOptional();


    /**
     * Converts this to a {@link CompletableFuture}
     *
     * @return A new {@link CompletableFuture} containing the value
     */
    default CompletableFuture<S> toCompletableFuture() {
        final CompletableFuture<S> completableFuture = new CompletableFuture<>();
        Try.to(this::getSuccess)
            .onSuccess(completableFuture::complete)
            .onFailure(completableFuture::completeExceptionally);
        return completableFuture;
    }

    /**
     * returns the success value if this is a Success or returns {@code elseValue} if it is a failure.
     *
     * @param elseValue value returned in case of Failure
     * @return the success value on success, elseValue otherwise
     */
    default S getOrElseGet(S elseValue) {
        return isSuccess() ? getSuccess() : elseValue;
    }

    /**
     * returns the success value if this is a Success or returns supplied value by applying {@code other.apply()}
     * if it is a failure.
     *
     * @param other function that supplies the value in case of Failure
     * @return the success value on success, of response of {@code other.apply()} otherwise
     */
    default S getOrElseGet(Supplier<S> other) {
        Objects.requireNonNull(other, "other is null");
        return isSuccess() ? getSuccess() : other.get();
    }

    /**
     * returns the success value if this is a Success or returns supplied value by applying {@code other.apply(Throwable)}
     * if it is a failure.
     *
     * @param other function that supplies the value in case of Failure
     * @return the success value on success, of response of {@code other.apply()} otherwise
     */
    default S getOrElseGet(Function<? super Throwable, ? extends S> other) {
        Objects.requireNonNull(other, "other is null");
        return isSuccess() ? getSuccess() : other.apply(getFailure());
    }
    /**
     * applies Consumer action if this is a Failure, do nothing otherwise.
     *
     * @param action action to apply on Failure
     */
    default void orElseRun(Consumer<? super Throwable> action) {
        Objects.requireNonNull(action, "action is null");
        if (isFailure()) {
            action.accept(getFailure());
        }
    }

    /**
     * returns the success value if this is a Success or throws Throwable by applying {@code orElseThrow.apply(Throwable)}
     * if it is a failure.
     *
     * @throws X exception may be thrown by given function
     * @param orElseThrow function that throws a Throwable
     * @return the success value on success, throws Exception otherwise
     */
    default <X extends Throwable> S getOrElseThrow(Function<? super Throwable, X> orElseThrow) throws X {
        Objects.requireNonNull(orElseThrow, "getOrElseThrow supplier is null");
        if(isSuccess()) {
            return getSuccess();
        } else {
            throw orElseThrow.apply(getFailure());
        }
    }

    /**
     * Equivalent of {@link #getSuccess()}
     */
    default S onFailureThrow() {
        return getSuccess();
    }

    /**
     * Folds either the {@code Failure} or the {@code Success} side of the Try value.
     *
     * @param onFailure  maps the left value if this is a {@code Failure}
     * @param onSuccess  maps the value if this is a {@code Success}
     * @param <R>        type of the folded value
     * @return A value of type R
     */
    default <R> R fold(Function<? super Throwable, ? extends R> onFailure, Function<? super S, ? extends R> onSuccess) {
        Objects.requireNonNull(onFailure, "onFailure is null");
        Objects.requireNonNull(onSuccess, "onSuccess is null");
        if (isFailure()) {
            return onFailure.apply(getFailure());
        } else {
            return onSuccess.apply(getSuccess());
        }
    }

    interface Try1<S> extends Try<S> {
        /**
         * Shortcut for {@code andThenTry(runnable)}, see {@link #andThenTry(ThrowableFunction0_0)} declaring exceptions.
         *
         * @param runnable action to do
         * @return this {@code Try} if this is a {@code Failure} or the consumer succeeded, otherwise the
         * {@code Failure} of the runnable.
         * @throws X exception may be thrown by runnable
         * @throws NullPointerException if {@code runnbale} is null
         */
        default <X extends Throwable> Try1<S> andThen(ThrowableFunction0_0<X> runnable) throws X {
            Objects.requireNonNull(runnable, "runnable is null");
            return andThenTry(runnable);
        }
        /**
         * Runs the given runnable if this is a {@code Success}, otherwise returns this {@code Failure}.
         * <p>
         * The main use case is chaining runnables using method references:
         *
         * <pre>
         * <code>
         * Try.run(A::methodRef).andThen(B::methodRef).andThen(C::methodRef);
         * </code>
         * </pre>
         *
         * Please note that these lines are semantically the same:
         *
         * <pre>
         * <code>
         * Try.run(this::doStuff)
         *    .andThen(this::doMoreStuff)
         *    .andThen(this::doEvenMoreStuff);
         *
         * Try.run(() -&gt; {
         *     doStuff();
         *     doMoreStuff();
         *     doEvenMoreStuff();
         * });
         * </code>
         * </pre>
         *
         * @param runnable A checked runnable
         * @return this {@code Try} if this is a {@code Failure} or the runnable succeeded, otherwise the
         * {@code Failure} of the run.
         * @throws NullPointerException if {@code runnable} is null
         */
        default Try1<S> andThenTry(ThrowableFunction0_0<? extends Throwable> runnable) {
            Objects.requireNonNull(runnable, "runnable is null");
            if (isFailure()) {
                return this;
            } else {
                try {
                    runnable.apply();
                    return this;
                } catch (Throwable t) {
                    return new Try1.Failure<>(t);
                }
            }
        }
        /**
         * Shortcut for {@link #andThenTryWhen(ThrowablePredicate1, ThrowableFunction0_0)} with delacring exceptions
         * and similar to {@link #andThen(ThrowableFunction0_0)} except that the runnable action is launched only if
         * predicate passes.
         *
         * @param predicate predicate to evaluate before launch the runnable
         * @param runnable action to do only if predicate returns true
         * @return this {@code Try} if this is a {@code Failure} or the runnable succeeded or the predicate fails, otherwise the
         * {@code Failure} of the runnable or the predicate.
         * @throws X1 exception that predicate may throw
         * @throws X2 exception that runnable may throw
         * @throws NullPointerException if {@code runnable} or {@code predicate} is null
         */
        default <X1 extends Throwable, X2 extends Throwable> Try1<S> andThenWhen(ThrowablePredicate1<X1, ? super S> predicate, ThrowableFunction0_0<X2> runnable) throws X1, X2 {
            Objects.requireNonNull(runnable, "runnable is null");
            return andThenTryWhen(predicate, runnable);
        }

        /**
         * Similar to {@link #andThenTry(ThrowableFunction0_0)} except that the runnable action is launched only if
         * predicate passes.
         *
         * @param predicate predicate to evaluate before launch the runnable
         * @param runnable action to do only if predicate returns true
         * @return this {@code Try} if this is a {@code Failure} or the runnable succeeded or the predicate fails, otherwise the
         * {@code Failure} of the runnable or the predicate.
         * @throws NullPointerException if {@code runnable} or {@code predicate} is null
         */
        default Try1<S> andThenTryWhen(ThrowablePredicate1<? extends Throwable, ? super S> predicate, ThrowableFunction0_0<? extends Throwable> runnable) {
            Objects.requireNonNull(predicate, "predicate is null");
            Objects.requireNonNull(runnable, "runnable is null");
            if (isFailure()) {
                return this;
            }
            try {
                if(predicate.test(getSuccess())) {
                    runnable.apply();
                }
                return this;
            } catch (Throwable t) {
                return new Try1.Failure<>(t);
            }
        }

        /**
         * Shortcut for {@code andThenTry(consumer::accept)} with declaring exceptions, see {@link #andThenTry(ThrowableFunction1_0)}.
         *
         * @param consumer A consumer
         * @return this {@code Try} if this is a {@code Failure} or the consumer succeeded, otherwise the
         * {@code Failure} of the consumption.
         * @throws X Exception that consumer may throw
         * @throws NullPointerException if {@code consumer} is null
         */
        default <X extends Throwable> Try1<S> andThen(ThrowableFunction1_0<X, ? super S> consumer) throws X {
            Objects.requireNonNull(consumer, "consumer is null");
            return andThenTry(consumer);
        }
        /**
         * Passes the result to the given {@code consumer} if this is a {@code Success}.
         * <p>
         * The main use case is chaining checked functions using method references:
         *
         * <pre><code>
         * Try.of(() -&gt; 100)
         *    .andThen(i -&gt; System.out.println(i));
         * </code></pre>
         *
         * @param consumer A checked consumer
         * @return this {@code Try} if this is a {@code Failure} or the consumer succeeded, otherwise the
         * {@code Failure} of the consumption.
         * @throws NullPointerException if {@code consumer} is null
         */
        default Try1<S> andThenTry(ThrowableFunction1_0<? extends Throwable, ? super S> consumer) {
            Objects.requireNonNull(consumer, "consumer is null");
            if (isFailure()) {
                return this;
            } else {
                try {
                    consumer.apply(getSuccess());
                    return this;
                } catch (Throwable t) {
                    return new Try1.Failure<>(t);
                }
            }
        }
        /**
         * Similar to {@link #andThenTry(ThrowableFunction1_0)} except that the consumer action is launched only if
         * predicate passes and declaring execptions that predicate and consumer can throw.
         *
         * @param predicate predicate to evaluate before launch the consumer
         * @param consumer action to do only if predicate returns true
         * @return this {@code Try} if this is a {@code Failure} or the consumer succeeded or the predicate fails, otherwise the
         * {@code Failure} of the consumption.
         * @throws X1 Exception may be thrown by {@code predicate}
         * @throws X2 Exception may be thrown by {@code consumer}
         * @throws NullPointerException if {@code consumer} or {@code predicate} is null
         */
        default <X1 extends Throwable, X2 extends Throwable> Try1<S> andThenWhen(ThrowablePredicate1<X1, ? super S> predicate, ThrowableFunction1_0<X2, ? super S> consumer) throws X1, X2 {
            Objects.requireNonNull(consumer, "consumer is null");
            Objects.requireNonNull(predicate, "predicate is null");
            return andThenTryWhen(predicate, consumer);
        }
        /**
         * Similar to {@link #andThenTryWhen(ThrowablePredicate1, ThrowableFunction0_0)} except that the runnable action is launched only if
         * predicate passes.
         *
         * @param predicate predicate to evaluate before launch the consumer
         * @param consumer action to do only if predicate returns true
         * @return this {@code Try} if this is a {@code Failure} or the consumer succeeded or the predicate fails, otherwise the
         * {@code Failure} of the consumption.
         * @throws NullPointerException if {@code runnable} or {@code predicate} is null
         */
        default Try1<S> andThenTryWhen(ThrowablePredicate1<? extends Throwable, ? super S> predicate, ThrowableFunction1_0<? extends Throwable, ? super S> consumer) {
            Objects.requireNonNull(consumer, "consumer is null");
            Objects.requireNonNull(predicate, "predicate is null");
            if (isFailure()) {
                return this;
            }
            try {
                S success = getSuccess();
                if(predicate.test(success)) {
                    consumer.apply(success);
                }
                return this;
            } catch (Throwable t) {
                return new Try1.Failure<>(t);
            }
        }
        /**
         * Runs onTrue consumer if predicate passes, onFalse consumer otherwise. This function declares all potential exception
         * that these functions may throw.
         *
         * @param predicate predicate to evaluate before launch the consumer
         * @param onTrue action to do if predicate returns true
         * @param onFalse action to do if predicate returns false
         * @return this {@code Try} if this is a {@code Failure} or the onTrue or onFalse succeeded, otherwise the
         * {@code Failure} of consumptions onTrue or onFalse.
         * @throws X1 Exception may be thrown by {@code predicate}
         * @throws X2 Exception may be thrown by {@code onTrue}
         * @throws X3 Exception may be thrown by {@code onFalse}
         * @throws NullPointerException if {@code onTrue}, {@code onFalse} or {@code predicate} is null
         */
        default <X1 extends Throwable, X2 extends Throwable, X3 extends Throwable> Try1<S> andThenWhen(ThrowablePredicate1<X1, ? super S> predicate, ThrowableFunction1_0<X2, ? super S> onFalse, ThrowableFunction1_0<X3, ? super S> onTrue) throws X1, X2, X3 {
            Objects.requireNonNull(predicate, "predicate is null");
            Objects.requireNonNull(onTrue, "onTrue is null");
            Objects.requireNonNull(onFalse, "onFalse is null");
            return andThenTryWhen(predicate, onFalse, onTrue);
        }
        /**
         * Runs onTrue consumer if predicate passes, onFalse consumer otherwise.
         *
         * @param predicate predicate to evaluate before launch the consumer
         * @param onTrue action to do if predicate returns true
         * @param onFalse action to do if predicate returns false
         * @return this {@code Try} if this is a {@code Failure} or the onTrue or onFalse succeeded, otherwise the
         * {@code Failure} of consumptions onTrue or onFalse.
         * @throws NullPointerException if {@code onTrue}, {@code onFalse} or {@code predicate} is null
         */
        default Try1<S> andThenTryWhen(ThrowablePredicate1<? extends Throwable, ? super S> predicate, ThrowableFunction1_0<? extends Throwable, ? super S> onFalse, ThrowableFunction1_0<? extends Throwable, ? super S> onTrue) {
            Objects.requireNonNull(predicate, "predicate is null");
            Objects.requireNonNull(onTrue, "onTrue is null");
            Objects.requireNonNull(onFalse, "onFalse is null");
            if (isFailure()) {
                return this;
            }
            try {
                S success = getSuccess();
                if(predicate.test(success)) {
                    onTrue.apply(success);
                } else {
                    onFalse.apply(success);
                }
                return this;
            } catch (Throwable t) {
                return new Try1.Failure<>(t);
            }
        }

        /**
         * Applies the {@code successAction} to the value if this is a Success or applies the {@code failureAction} to the failure.
         *
         * @param failureAction A Consumer for the Failure case
         * @param successAction A Consumer for the Success case
         * @return this {@code Try1}
         */
        default Try1<S> peek(Consumer<? super Throwable> failureAction, Consumer<? super S> successAction) {
            Objects.requireNonNull(failureAction, "failureAction is null");
            Objects.requireNonNull(successAction, "successAction is null");
            if (isSuccess()) {
                successAction.accept(getSuccess());
            } else {
                failureAction.accept(getFailure());
            }
            return this;
        }
        /**
         * Applies the action to the value of a Success or does nothing in the case of a Failure.
         *
         * @param action A Consumer
         * @return this {@code Try1}
         * @throws NullPointerException if {@code action} is null
         */
        default Try1<S> peek(Consumer<? super S> action) {
            Objects.requireNonNull(action, "action is null");
            if (isSuccess()) {
                action.accept(getSuccess());
            }
            return this;
        }
        /**
         * Applies an action if it is a Success or does nothing in the case of a Failure.
         *
         * @param action A Runnable
         * @return this {@code Try1}
         * @throws NullPointerException if {@code action} is null
         */
        default Try1<S> peek(Runnable action) {
            Objects.requireNonNull(action, "action is null");
            if (isSuccess()) {
                action.run();
            }
            return this;
        }

        /**
         * Shortcut for {@link #tryMap(ThrowableFunction1_1)} that declares exception that mapper function can throw.
         *
         * @param <R>    The new component type
         * @param mapper A checked function
         * @return a {@code Try}
         * @throws X Exception that mapper function may throw
         * @throws NullPointerException if {@code mapper} is null
         */
        default <X extends Throwable, R> Try1<R> map(ThrowableFunction1_1<X, ? super S, ? extends R> mapper) throws X {
            Objects.requireNonNull(mapper, "mapper is null");
            return tryMap(mapper);
        }
        /**
         * Maps the current success {@code <S>} value to another success value {@code <R>} with the given mapper function.
         * If this function throws an exception then it'll return a new
         * {@code Failure} of this exception.<br/>
         * Returns this if this is already a {@code Failure}.
         * <p>
         * The main use case is chaining checked functions using method references:
         *
         * <pre>
         * <code>
         * Try.of(() -&gt; 0)
         *    .map(x -&gt; 1 / x); // division by zero
         * </code>
         * </pre>
         *
         * @param <R>    The result component type
         * @param mapper A Throwable function
         * @return a {@code Try}
         * @throws NullPointerException if {@code mapper} is null
         */
        default <R> Try1<R> tryMap(ThrowableFunction1_1<? extends Throwable, ? super S, ? extends R> mapper) {
            Objects.requireNonNull(mapper, "mapper is null");
            if(isSuccess()) {
                try {
                    return new Try1.Success<>(mapper.apply(getSuccess()));
                } catch (Throwable e) {
                    return new Try1.Failure<>(e);
                }
            } else {
                return (Try1<R>) this;
            }
        }
        /**
         * Shortcut for {@link #tryMapWhen(ThrowablePredicate1, ThrowableFunction1_1)} that declares exception that mapper
         * and predicate functions can throw.<br/>
         * <ul>
         * Returns :
         * <li>an {@link Optional} with the result if the predicate passes and the mapper runs successfully.</li>
         * <li>an empty {@link Optional} if the predicate returns false.</li>
         * <li>a new {@code Failure} if one of the function throws an Exception.</li>
         * <li>this if this was already a {@code Failure}.</li>
         * </ul>
         *
         * @param <R>    The result component type
         * @param mapper A throwable function
         * @return a {@code Try} containing an Optional of the result
         * @throws X1 Exception that predicate may throw
         * @throws X2 Exception that mapper function may throw
         * @throws NullPointerException if {@code mapper} is null
         */
        default <X1 extends Throwable, X2 extends Throwable, R> Try1<Optional<R>> mapWhen(ThrowablePredicate1<X1, S> predicate, ThrowableFunction1_1<X2, ? super S, ? extends R> mapper) throws X1, X2 {
            Objects.requireNonNull(mapper, "mapper is null");
            return tryMapWhen(predicate, mapper);
        }
        /**
         * Maps the current success {@code <S>} value to an Optional of another success value {@code <R>} with the given
         * mapper function if the predicate returns {@code true}.
         * <ul>
         * Returns :
         * <li>an {@link Optional} with the result if the predicate passes and the mapper runs successfully.</li>
         * <li>an empty {@link Optional} if the predicate returns false.</li>
         * <li>a new {@code Failure} if one of the function throws an Exception.</li>
         * <li>this if this was already a {@code Failure}.</li>
         * </ul>
         *
         * @param <R>    The result component type
         * @param mapper A throwable function
         * @return a {@code Try} containing an Optional of the result
         * @throws NullPointerException if {@code mapper} is null
         */
        default <R> Try1<Optional<R>> tryMapWhen(ThrowablePredicate1<? extends Throwable, S> predicate, ThrowableFunction1_1<? extends Throwable, ? super S, ? extends R> mapper) {
            Objects.requireNonNull(mapper, "mapper is null");
            if(isSuccess()) {
                try {
                    if(predicate.test(getSuccess())) {
                        return new Try1.Success<>(Optional.ofNullable(mapper.apply(getSuccess())));
                    } else {
                        return Try.success(Optional.empty());
                    }
                } catch (Throwable e) {
                    return new Try1.Failure<>(e);
                }
            } else {
                return new Try1.Failure<>(getFailure());
            }
        }
        /**
         * Shortcut for {@link #tryMapWhen(ThrowablePredicate1, ThrowableFunction1_1, ThrowableFunction1_1)} that declares
         * exceptions that predicate and other mappers ({@code onTrue}, {@code onFalse}) functions can throw.<br/>
         * <ul>
         * Returns :
         * <li>a version of a success value R if the predicate passes by running onTrue function.</li>
         * <li>an other version of a success value R if the predicate returns false by running onFalse function.</li>
         * <li>a new {@code Failure} if one of the function throws an Exception.</li>
         * <li>this if this was already a {@code Failure}.</li>
         * </ul>
         *
         * @param <R>    The result component type
         * @param predicate A throwable function
         * @param onTrue A throwable function
         * @param onFalse A throwable function
         * @return a {@code Try}
         * @throws X1 Exception that predicate may throw
         * @throws X2 Exception that onFalse function may throw
         * @throws X3 Exception that onTrue function may throw
         * @throws NullPointerException if {@code mapper} is null
         */
        default <X1 extends Throwable, X2 extends Throwable, X3 extends Throwable, R> Try1<R> mapWhen(ThrowablePredicate1<X1, S> predicate, ThrowableFunction1_1<X2, ? super S, ? extends R> onFalse, ThrowableFunction1_1<X3, ? super S, ? extends R> onTrue) throws X1, X2, X3 {
            Objects.requireNonNull(predicate, "predicate is null");
            Objects.requireNonNull(onFalse, "onFalse is null");
            Objects.requireNonNull(onTrue, "onTrue is null");
            return tryMapWhen(predicate, onFalse, onTrue);
        }
        /**
         * Maps the success {@code <S>} value to another success value {@code <R>} using the onTrue mapper function if predicate returns True,
         * using the onFalse mapper function otherwise.
         * <ul>
         * Returns :
         * <li>a version of a success value R if the predicate passes by running onTrue function.</li>
         * <li>an other version of a success value R if the predicate returns false by running onFalse function.</li>
         * <li>a new {@code Failure} if one of the function throws an Exception.</li>
         * <li>this if this was already a {@code Failure}.</li>
         * </ul>
         *
         * @param <R>    The result component type
         * @param predicate A throwable function
         * @param onTrue A throwable function
         * @param onFalse A throwable function
         * @return a {@code Try}
         * @throws NullPointerException if {@code mapper} is null
         */
        default <R> Try1<R> tryMapWhen(ThrowablePredicate1<? extends Throwable, S> predicate, ThrowableFunction1_1<? extends Throwable, ? super S, ? extends R> onFalse, ThrowableFunction1_1<? extends Throwable, ? super S, ? extends R> onTrue) {
            Objects.requireNonNull(predicate, "predicate is null");
            Objects.requireNonNull(onFalse, "onFalse is null");
            Objects.requireNonNull(onTrue, "onTrue is null");
            if(isSuccess()) {
                try {
                    if(predicate.test(getSuccess())) {
                        return new Try1.Success<>(onTrue.apply(getSuccess()));
                    } else {
                        return new Try1.Success<>(onFalse.apply(getSuccess()));
                    }
                } catch (Throwable e) {
                    return new Try1.Failure<>(e);
                }
            } else {
                return new Try1.Failure<>(getFailure());
            }
        }

        /**
         * Shortcut for {@link #tryFlatMap(ThrowableFunction1_1)} except that declares exception that mapper function can throw.
         *
         * @param mapper A mapper
         * @param <R>    The new component type
         * @return a {@code Try}
         * @throws X Exception that mapper function can throw
         * @throws NullPointerException if {@code mapper} is null
         */
        default <X extends Throwable, R> Try1<R> flatMap(ThrowableFunction1_1<X, ? super S, Try1<R>> mapper) throws X {
            Objects.requireNonNull(mapper, "mapper is null");
            return tryFlatMap(mapper);
        }
        /**
         * FlatMaps the value of a Success or returns a Failure.
         *
         * @param mapper A mapper
         * @param <R>    The new component type
         * @return a {@code Try}
         * @throws NullPointerException if {@code mapper} is null
         */
        default <R> Try1<R> tryFlatMap(ThrowableFunction1_1<? extends Throwable, ? super S, Try1<R>> mapper) {
            Objects.requireNonNull(mapper, "mapper is null");
            if (isSuccess()) {
                try {
                    return mapper.apply(getSuccess());
                } catch (Throwable e) {
                    return new Try1.Failure<>(e);
                }
            } else {
                return (Try1<R>) this;
            }
        }

        /**
         * Shortcut for {@link #tryMap2(ThrowableFunction1_2)} that declares exception that mapper function can throw.
         *
         * @param <R1>      The first new component type
         * @param <R2>      The second new component type
         * @param mapper A Throwable function
         * @return a {@code Try}
         * @throws X Exception that mapper function may throw
         * @throws NullPointerException if {@code mapper} is null
         */
        default <X extends Throwable, R1, R2> Try2<R1, R2> map2(ThrowableFunction1_2<X, ? super S, ? extends R1, ? extends R2> mapper) throws X {
            Objects.requireNonNull(mapper, "mapper is null");
            return tryMap2(mapper);
        }
        /**
         * Maps the current success {@code <S>} value to 2 another success values {@code <R1>}, {@code <R2>} in a
         * {@link Tuple2} with the given mapper function.<br/>
         * If this function throws an exception then it'll return a new
         * {@code Failure} of this exception.<br/>
         * Returns this if this is already a {@code Failure}.
         *
         * <pre><code>Try.of(() -&gt; "Luke Skywalker")
         *    .map(str -&gt; str.split(" "))
         *    .map2(array -&gt; Tuple.of(array[0], array[1]));
         *    .map((firstname, lastname) -> someAction(firstname, lastname))</code></pre>
         *
         * @param <R1>    The first result component type
         * @param <R2>    The second result component type
         * @param mapper A Throwable function
         * @return a {@code Try}
         * @throws NullPointerException if {@code mapper} is null
         */
        default <R1, R2> Try2<R1, R2> tryMap2(ThrowableFunction1_2<? extends Throwable, ? super S, ? extends R1, ? extends R2> mapper) {
            Objects.requireNonNull(mapper, "mapper is null");
            if(isSuccess()) {
                try {
                    Tuple2<? extends R1, ? extends R2> tuple = mapper.apply(getSuccess());
                    return new Try2.Success<>(tuple.t1(), tuple.t2());
                } catch (Throwable e) {
                    return new Try2.Failure<>(e);
                }
            } else {
                return (Try2<R1, R2>) this;
            }
        }

        /**
         * Shortcut for {@link #tryFlatMap2(ThrowableFunction1_1)} except that declares exception that mapper function can throw.
         *
         * @param mapper A mapper
         * @param <R1>    The new first component type
         * @param <R2>    The new second component type
         * @return a {@code Try}
         * @throws X Exception that mapper function can throw
         * @throws NullPointerException if {@code mapper} is null
         */
        default <X extends Throwable, R1, R2> Try2<R1, R2> flatMap2(ThrowableFunction1_1<X, ? super S, Try2<R1, R2>> mapper) throws X {
            Objects.requireNonNull(mapper, "mapper is null");
            return tryFlatMap2(mapper);
        }
        /**
         * FlatMaps the value of a Success to 2 new success values or returns a Failure.
         *
         * @param mapper A mapper
         * @param <R1>    The new first component type
         * @param <R2>    The new second component type
         * @return a {@code Try}
         * @throws NullPointerException if {@code mapper} is null
         */
        default <R1, R2> Try2<R1, R2> tryFlatMap2(ThrowableFunction1_1<? extends Throwable, ? super S, Try2<R1, R2>> mapper) {
            Objects.requireNonNull(mapper, "mapper is null");
            if (isSuccess()) {
                try {
                    return mapper.apply(getSuccess());
                } catch (Throwable e) {
                    return new Try2.Failure<>(e);
                }
            } else {
                return (Try2<R1, R2>) this;
            }
        }

        /**
         * Shortcut for {@link #tryAdd(ThrowableFunction1_1)} that declares exception that mapper function can throw.
         *
         * @param <R>      The new component type
         * @param mapper A Throwable function
         * @return a {@code Try}
         * @throws X Exception that mapper function may throw
         * @throws NullPointerException if {@code mapper} is null
         */
        default <X extends Throwable, R> Try2<S, R> add(ThrowableFunction1_1<X, ? super S, ? extends R> mapper) throws X {
            Objects.requireNonNull(mapper, "mapper is null");
            return tryAdd(mapper);
        }
        /**
         * Maps the current success {@code <S>} value to a second success value {@code <R>} with the given mapper function
         * and keeps the previous success value in first.<br/>
         * If this function throws an exception then it'll return a new
         * {@code Failure} of this exception.<br/>
         * Returns this if this is already a {@code Failure}.
         *
         * <pre><code>
         * Try.of(() -&gt; "Luke")
         *    .tryAdd(firstname -&gt; getLastName(firstname));
         *    .map((firstname, lastname) -&gt; someAction(firstname, lastname));
         * </code></pre>
         *
         * @param <R>    The result component type
         * @param mapper A Throwable function
         * @return a {@code Try}
         * @throws NullPointerException if {@code mapper} is null
         */
        default <R> Try2<S, R> tryAdd(ThrowableFunction1_1<? extends Throwable, ? super S, ? extends R> mapper) {
            Objects.requireNonNull(mapper, "mapper is null");
            if(isSuccess()) {
                try {
                    S success = getSuccess();
                    return new Try2.Success<>(Tuple.of(success, mapper.apply(success)));
                } catch (Throwable e) {
                    return new Try2.Failure<>(e);
                }
            } else {
                return (Try2<S, R>) this;
            }
        }

        /**
         * Shortcut for {@link #tryAddWhen(ThrowablePredicate1, ThrowableFunction1_1)} that declares exception
         * that mapper or predicate functions can throw.
         *
         * @param <R>      The new component type
         * @param mapper A Throwable function
         * @return a {@code Try}
         * @throws X1 Exception that mapper function may throw
         * @throws X2 Exception that mapper function may throw
         * @throws NullPointerException if {@code mapper} or {@code predicate} is null
         */
        default <X1 extends Throwable, X2 extends Throwable, R> Try2<S, Optional<R>> addWhen(ThrowablePredicate1<X1, ? super S> predicate, ThrowableFunction1_1<X2, ? super S, ? extends R> mapper) throws X1, X2 {
            Objects.requireNonNull(predicate, "predicate is null");
            Objects.requireNonNull(mapper, "mapper is null");
            return tryAddWhen(predicate, mapper);
        }
        /**
         * Similar to {@link #tryAdd(ThrowableFunction1_1)} except that the mapper is applied only if predicate passes.<br/>
         * The second success value is an {@link Optional} of {@code <R>} is predicate returns {@code true}.
         * An empty {@link Optional} otherwise.
         *
         * @param <R>      The new component type
         * @param mapper A Throwable function
         * @return a {@code Try}
         * @throws NullPointerException if {@code mapper} or {@code predicate} is null
         */
        default <R> Try2<S, Optional<R>> tryAddWhen(ThrowablePredicate1<? extends Throwable, ? super S> predicate, ThrowableFunction1_1<? extends Throwable, ? super S, ? extends R> mapper) {
            Objects.requireNonNull(predicate, "predicate is null");
            Objects.requireNonNull(mapper, "mapper is null");
            if (isFailure()) {
                return (Try2<S, Optional<R>>) this;
            }
            try {
                S success = getSuccess();
                if(predicate.test(getSuccess())) {
                    return new Try2.Success<>(Tuple.of(success, Optional.ofNullable(mapper.apply(success))));
                } else {
                    return new Try2.Success<>(Tuple.of(success, Optional.empty()));
                }
            } catch (Throwable e) {
                return new Try2.Failure<>(e);
            }
        }

        /**
         * force the throws declaration of the given exception
         * @param clazz class of the declaration to declares to throw
         * @return this
         * @param <X> exception to declare
         * @throws X the exception to declare to throws
         */
        default <X extends Throwable> Try1<S> throwing(Class<X> clazz) throws X {
            return this;
        }

        /**
         * Runs the action runnable if this is a {@link Try.Failure}.
         *
         * <pre>{@code
         * // doAction not called
         * Try.success(1).onFailure(() -> doAction()));
         *
         * // doAction called
         * Try.failure(new Error()).onFailure(() -> doAction());
         * }</pre>
         *
         * @param action An exception consumer
         * @return this
         * @throws NullPointerException if {@code action} is null
         */
        default Try1<S> onFailure(Runnable action) {
            Objects.requireNonNull(action, "action is null");
            if (isFailure()) {
                action.run();
            }
            return this;
        }

        /**
         * Runs the action runnable if this is a {@link Try.Failure}.
         *
         * <pre>{@code
         * // (does not print anything)
         * Try.success(1).onFailure(System.out::println);
         *
         * // prints "java.lang.Error"
         * Try.failure(new Error()).onFailure(System.out::println);
         * }</pre>
         *
         * @param consumer An exception consumer
         * @return this
         * @throws NullPointerException if {@code action} is null
         */
        default Try1<S> onFailure(Consumer<? super Throwable> consumer) {
            Objects.requireNonNull(consumer, "action is null");
            if (isFailure()) {
                consumer.accept(getFailure());
            }
            return this;
        }
        /**
         * Consumes the exception if this is a {@link Try.Failure} only if the exception is instance of {@code X}.
         *
         * <pre>{@code
         * // (does not print anything)
         * Try.success(1).onFailure(Error.class, System.out::println);
         *
         * // prints "Error"
         * Try.failure(new Error())
         *    .onFailure(RuntimeException.class, x -> System.out.println("Runtime exception"))
         *    .onFailure(Error.class, x -> System.out.println("Error"));
         * }</pre>
         *
         * @param exceptionType the exception type that is handled
         * @param action an excpetion consumer
         * @param <X> the exception type that should be handled
         * @return this
         * @throws NullPointerException if {@code exceptionType} or {@code action} is null
         */
        default <X> Try1<S> onFailure(Class<X> exceptionType, Consumer<? super X> action) {
            Objects.requireNonNull(exceptionType, "exceptionType is null");
            Objects.requireNonNull(action, "action is null");
            if (isFailure() && exceptionType.isAssignableFrom(getFailure().getClass())) {
                action.accept((X)getFailure());
            }
            return this;
        }
        /**
         * Maps the cause to a new exception if this is a {@code Failure} or returns this instance if this is a {@code Success}.
         *
         * @return A new {@code Try} if this is a {@code Failure}, otherwise this.
         */
        default <X extends Throwable> Try1<S> mapFailure(Function<? super Throwable, ? extends X> mapper) {
            Objects.requireNonNull(mapper, "mapper is null");
            if (isFailure()) {
                return new Try1.Failure<>(mapper.apply(getFailure()));
            }
            return this;
        }
        /**
         * Maps the exception failure to a new exception if this is a {@code Failure} and the exception type matches the given class
         * or returns this instance if this is a {@code Success}.
         *
         * @param exceptionType the exception type that is handled.
         * @return A new {@code Try} if this is a {@code Failure}, otherwise this.
         */
        default <X extends Throwable> Try1<S> mapFailure(Class<X> exceptionType, Function<? super X, ? extends Throwable> mapper) {
            Objects.requireNonNull(exceptionType, "exceptionType is null");
            Objects.requireNonNull(mapper, "mapper is null");
            if (isFailure() && exceptionType.isAssignableFrom(getFailure().getClass())) {
                return new Try1.Failure<>(mapper.apply((X)getFailure()));
            }
            return this;
        }
        /**
         * Maps the exception failure to a new exception if this is a {@code Failure} and the exception type matches any given
         * exception types given in a list or returns this instance if this is a {@code Success}.
         *
         * @param exceptionTypes the exception type that is handled.
         * @return A new {@code Try} if this is a {@code Failure}, otherwise this.
         */
        default <X extends Throwable, Y extends Throwable> Try1<S> mapFailures(List<Class<? extends X>> exceptionTypes, Function<? super X, ? extends Y> mapper) {
            Objects.requireNonNull(exceptionTypes, "exceptionTypes is null");
            Objects.requireNonNull(mapper, "mapper is null");
            if(isFailure() && exceptionTypes.stream().anyMatch(clazz -> clazz.isAssignableFrom(getFailure().getClass()))) {
                return new Try1.Failure<>(mapper.apply((X)getFailure()));
            }
            return this;
        }

        /**
         * Equivalent of {@link #andThen(ThrowableFunction1_0)}
         *
         * @param action action to apply
         * @return this if this is a Success or a Failure or switch to a new Failure if action throws an Exception
         * @throws X exception declared that action can throw
         */
        default <X extends Throwable> Try1<S> onSuccess(ThrowableFunction1_0<X, ? super S> action) throws X {
            Objects.requireNonNull(action, "action is null");
            return andThen(action);
        }

        /**
         * Equivalent of {@link #andThenTry(ThrowableFunction1_0)}
         *
         * @param action action to apply
         * @return this if this is a Success or a Failure or switch to a new Failure if action throws an Exception
         */
        default Try1<S> onSuccessTry(ThrowableFunction1_0<? extends Throwable, ? super S> action) {
            Objects.requireNonNull(action, "action is null");
            return andThenTry(action);
        }

        /**
         * returns the success value if this is a Success or returns a new {@link Try1} with a new Success value
         * if it is a failure.
         *
         * @param other function that supplies the value in case of Failure
         * @return the success value on success, of response of {@code other.apply()} otherwise
         */
        @SuppressWarnings("unchecked")
        default Try1<S> orElse(Try1<? extends S> other) {
            Objects.requireNonNull(other, "other is null");
            return isSuccess() ? this : (Try1<S>) other;
        }

        /**
         * returns the success value if this is a Success or returns a new supplied {@link Try1} with a new Success value
         * by applying {@code supplier.get()} if it is a failure.
         *
         * @param supplier function that supplies the value in case of Failure
         * @return the success value on success, of response of {@code other.apply()} otherwise
         */
        @SuppressWarnings("unchecked")
        default Try1<S> orElse(Supplier<Try1<? extends S>> supplier) {
            Objects.requireNonNull(supplier, "supplier is null");
            return isSuccess() ? this : (Try1<S>) supplier.get();
        }

        /**
         * Shortcut for {@code filterTry(predicate::test)}, see {@link #tryFilter(ThrowablePredicate1)}}.
         *
         * @param predicate A predicate
         * @return a {@code Try} instance
         * @throws NullPointerException if {@code predicate} is null
         */
        default <X extends Throwable> Try1<S> filter(ThrowablePredicate1<X, ? super S> predicate) throws X {
            Objects.requireNonNull(predicate, "predicate is null");
            return tryFilter(predicate);
        }

        /**
         * Contrary to filter, see {@link #filter(ThrowablePredicate1)}.
         *
         * @param predicate A predicate
         * @return a {@code Try} instance
         * @throws NullPointerException if {@code predicate} is null
         */
        default <X extends Throwable> Try1<S> filterNot(ThrowablePredicate1<X, ? super S> predicate) throws X {
            Objects.requireNonNull(predicate, "predicate is null");
            return filter(predicate.negate());
        }

        /**
         * Returns {@code this} if this is a Failure or this is a Success and the value satisfies the predicate.
         * <p>
         * Returns a new Failure, if this is a Success and the value does not satisfy the Predicate or an exception
         * occurs testing the predicate. The returned Failure wraps a {@link NoSuchElementException} instance.
         *
         * @param predicate A checked predicate
         * @return a {@code Try} instance
         * @throws NullPointerException if {@code predicate} is null
         */
        default Try1<S> tryFilter(ThrowablePredicate1<? extends Throwable, ? super S> predicate) {
            Objects.requireNonNull(predicate, "predicate is null");
            return tryFilter(predicate, () -> new NoSuchElementException("Predicate does not hold for " + getSuccess()));
        }

        /**
         * Shortcut for {@code filterTry(predicate::test, throwableSupplier)}, see
         * {@link #tryFilter(ThrowablePredicate1, ThrowableFunction0_1)}}.
         *
         * @param predicate         A predicate
         * @param throwableSupplier A supplier of a throwable
         * @return a {@code Try} instance
         * @throws NullPointerException if {@code predicate} or {@code throwableSupplier} is null
         */
        default <X1 extends Throwable, X2 extends Throwable, X3 extends Throwable> Try1<S> filter(ThrowablePredicate1<X1, ? super S> predicate, ThrowableFunction0_1<X2, X3> throwableSupplier) throws X1, X2, X3 {
            Objects.requireNonNull(predicate, "predicate is null");
            Objects.requireNonNull(throwableSupplier, "throwableSupplier is null");
            return tryFilter(predicate::test, throwableSupplier);
        }

        /**
         * Contrary to filter, see {@link #filter(ThrowablePredicate1, ThrowableFunction0_1)}.
         *
         * @param predicate         A predicate
         * @param throwableSupplier A supplier of a throwable
         * @return a {@code Try} instance
         * @throws NullPointerException if {@code predicate} or {@code throwableSupplier} is null
         */
        default <X1 extends Throwable, X2 extends Throwable, X3 extends Throwable> Try1<S> filterNot(ThrowablePredicate1<X1, ? super S> predicate, ThrowableFunction0_1<X2, X3> throwableSupplier) throws X1, X2, X3 {
            Objects.requireNonNull(predicate, "predicate is null");
            return filter(predicate.negate(), throwableSupplier);
        }

        /**
         * Returns {@code this} if this is a Failure or this is a Success and the value satisfies the predicate.
         * <p>
         * Returns a new Failure, if this is a Success and the value does not satisfy the Predicate or an exception
         * occurs testing the predicate. The returned Failure wraps a Throwable instance provided by the given
         * {@code throwableSupplier}.
         *
         * @param predicate         A checked predicate
         * @param throwableSupplier A supplier of a throwable
         * @return a {@code Try} instance
         * @throws NullPointerException if {@code predicate} or {@code throwableSupplier} is null
         */
        default Try1<S> tryFilter(ThrowablePredicate1<? extends Throwable, ? super S> predicate, ThrowableFunction0_1<? extends Throwable, ? extends Throwable> throwableSupplier) {
            Objects.requireNonNull(predicate, "predicate is null");
            Objects.requireNonNull(throwableSupplier, "throwableSupplier is null");

            if (isFailure()) {
                return this;
            } else {
                try {
                    if (predicate.test(getSuccess())) {
                        return this;
                    } else {
                        return new Try1.Failure<>(throwableSupplier.apply());
                    }
                } catch (Throwable t) {
                    return new Try1.Failure<>(t);
                }
            }
        }

        /**
         * Shortcut for {@code filterTry(predicate::test, errorProvider::apply)}, see
         * {@link #tryFilter(ThrowablePredicate1, ThrowableFunction1_1)}}.
         *
         * @param predicate A predicate
         * @param errorProvider A function that provides some kind of Throwable for T
         * @return a {@code Try} instance
         * @throws NullPointerException if {@code predicate} or {@code errorProvider} is null
         */
        default <X1 extends Throwable, X2 extends Throwable, X3 extends Throwable> Try1<S> filter(ThrowablePredicate1<X1, ? super S> predicate, ThrowableFunction1_1<X2, ? super S, X3> errorProvider) throws X1, X2, X3 {
            Objects.requireNonNull(predicate, "predicate is null");
            Objects.requireNonNull(errorProvider, "errorProvider is null");
            return tryFilter(predicate::test, errorProvider::apply);
        }

        /**
         * Contrary to filter, see {@link #filter(ThrowablePredicate1, ThrowableFunction1_1)}.
         *
         * @param predicate A predicate
         * @param errorProvider A function that provides some kind of Throwable for T
         * @return a {@code Try} instance
         * @throws NullPointerException if {@code predicate} or {@code errorProvider} is null
         */
        default <X1 extends Throwable, X2 extends Throwable, X3 extends Throwable> Try1<S> filterNot(ThrowablePredicate1<X1, ? super S> predicate, ThrowableFunction1_1<X2, ? super S, X3> errorProvider) throws X1, X2, X3 {
            Objects.requireNonNull(predicate, "predicate is null");
            return filter(predicate.negate(), errorProvider);
        }

        /**
         * Returns {@code this} if this is a Failure or this is a Success and the value satisfies the predicate.
         * <p>
         * Returns a new Failure, if this is a Success and the value does not satisfy the Predicate or an exception
         * occurs testing the predicate. The returned Failure wraps a Throwable instance provided by the given
         * {@code errorProvider}.
         *
         * @param predicate         A checked predicate
         * @param errorProvider     A provider of a throwable
         * @return a {@code Try} instance
         * @throws NullPointerException if {@code predicate} or {@code errorProvider} is null
         */
        default Try1<S> tryFilter(ThrowablePredicate1<? extends Throwable, ? super S> predicate, ThrowableFunction1_1<? extends Throwable, ? super S, ? extends Throwable> errorProvider) {
            Objects.requireNonNull(predicate, "predicate is null");
            Objects.requireNonNull(errorProvider, "errorProvider is null");
            return tryFlatMap(value -> predicate.test(value) ? this : new Try1.Failure<>(errorProvider.apply(value)));
        }

        /**
         * Returns {@code this}, if this is a {@code Success}, otherwise recovers the failure with the value.
         *
         * <pre>{@code
         * // = Success(13)
         * Try.of(() -> 27/2).recover(0);
         *
         * // = Success(0)
         * Try.of(() -> 1/0)
         *    .recover(0)
         * }</pre>
         *
         * @param value   The recovery value
         * @return a {@code Try1}
         */
        default Try1<S> recover(S value) {
            return (isFailure()) ? Try.success(value) : this;
        }


        /**
         * Returns {@code this}, if this is a {@code Success} or this is a {@code Failure} and the failure is not assignable
         * from {@code failure.getClass()}. Otherwise, recovers the failure's exception with the new value.
         *
         * <pre>{@code
         * // = Success(13)
         * Try.of(() -> 27/2).recover(ArithmeticException.class, x -> Integer.MAX_VALUE);
         *
         * // = Success(2147483647)
         * Try.of(() -> 1/0)
         *    .recover(Error.class, x -> -1)
         *    .recover(ArithmeticException.class, x -> Integer.MAX_VALUE);
         *
         * // = Failure(java.lang.ArithmeticException: / by zero)
         * Try.of(() -> 1/0).recover(Error.class, x -> Integer.MAX_VALUE);
         * }</pre>
         *
         * @param <X>           Exception type
         * @param exceptionType The specific exception type that should be handled
         * @param value         the recovery value
         * @return a {@code Try1}
         * @throws NullPointerException if {@code exceptionType} is null.
         */
        default <X extends Throwable> Try1<S> recover(Class<X> exceptionType, S value) {
            Objects.requireNonNull(exceptionType, "exceptionType is null");
            return (isFailure() && exceptionType.isAssignableFrom(getFailure().getClass()))
                ? Try.success(value)
                : this;
        }

        /**
         * Returns {@code this}, if this is a {@code Success}, otherwise tries to recover the failure's exception
         * with a throwable function {@code f}, i.e. calling {@code Try.of(() -> f.apply(throwable))} and declaring
         * the exception may throw.
         *
         * <pre>{@code
         * // = Success(13)
         * Try.of(() -> 27/2).recover(ex -> Integer.MAX_VALUE);
         *
         * // = Success(2147483647)
         * Try.of(() -> 1/0).recover(ex -> Integer.MAX_VALUE);
         * }</pre>
         *
         * @param f A recovery throwable function taking a Throwable
         * @return a {@code Try1}
         * @throws NullPointerException if {@code f} is null
         */
        default <X extends Throwable> Try1<S> recover(ThrowableFunction1_1<X, ? super Throwable, ? extends S> f) throws X {
            Objects.requireNonNull(f, "f is null");
            return tryRecover(f);
        }

        /**
         * Returns {@code this}, if this is a {@code Success}, otherwise tries to recover the failure's exception
         * with a throwable function {@code f}, i.e. calling {@code Try.of(() -> f.apply(throwable))}.
         *
         * <pre>{@code
         * // = Success(13)
         * Try.of(() -> 27/2).recover(ex -> Integer.MAX_VALUE);
         *
         * // = Success(2147483647)
         * Try.of(() -> 1/0).recover(ex -> Integer.MAX_VALUE);
         * }</pre>
         *
         * @param f A recovery throwable function taking a Throwable
         * @return a {@code Try1}
         * @throws NullPointerException if {@code f} is null
         */
        default Try1<S> tryRecover(ThrowableFunction1_1<? extends Throwable, ? super Throwable, ? extends S> f) {
            Objects.requireNonNull(f, "f is null");
            if (isFailure()) {
                try {
                    return Try.success(f.apply(getFailure()));
                } catch (Throwable e) {
                    return new Try1.Failure<>(e);
                }
            } else {
                return this;
            }
        }

        /**
         * Returns {@code this}, if this is a {@code Success} or this is a {@code Failure} and the cause is not assignable
         * from {@code cause.getClass()}. Otherwise tries to recover the failure's exception with a throwable function
         * {@code f}, i.e. calling {@code Try.of(() -> f.apply((X) getCause())} and declaring the exception that f may throw.
         *
         * <pre>{@code
         * // = Success(13)
         * Try.of(() -> 27/2).recover(ArithmeticException.class, x -> Integer.MAX_VALUE);
         *
         * // = Success(2147483647)
         * Try.of(() -> 1/0)
         *    .recover(Error.class, x -> -1)
         *    .recover(ArithmeticException.class, x -> Integer.MAX_VALUE);
         *
         * // = Failure(java.lang.ArithmeticException: / by zero)
         * Try.of(() -> 1/0).recover(Error.class, x -> Integer.MAX_VALUE);
         * }</pre>
         *
         * @param <X1>           Exception type which the failure is assigned
         * @param <X2>           Exception type may be thrown by f
         * @param exceptionType The specific exception type that should be handled
         * @param f             A recovery function taking an exception of type {@code X1}
         * @return a {@code Try1}
         * @throws NullPointerException if {@code exception} is null or {@code f} is null
         */
        default <X1 extends Throwable, X2 extends Throwable> Try1<S> recover(Class<X1> exceptionType, ThrowableFunction1_1<X2, ? super X1, ? extends S> f) throws X2 {
            Objects.requireNonNull(exceptionType, "exceptionType is null");
            Objects.requireNonNull(f, "f is null");
            return tryRecover(exceptionType, f);
        }

        /**
         * Returns {@code this}, if this is a {@code Success} or this is a {@code Failure} and the cause is not assignable
         * from {@code cause.getClass()}. Otherwise tries to recover the failure's exception with a throwable function
         * {@code f}, i.e. calling {@code Try.of(() -> f.apply((X) getCause())}.
         *
         * <pre>{@code
         * // = Success(13)
         * Try.of(() -> 27/2).recover(ArithmeticException.class, x -> Integer.MAX_VALUE);
         *
         * // = Success(2147483647)
         * Try.of(() -> 1/0)
         *    .recover(Error.class, x -> -1)
         *    .recover(ArithmeticException.class, x -> Integer.MAX_VALUE);
         *
         * // = Failure(java.lang.ArithmeticException: / by zero)
         * Try.of(() -> 1/0).recover(Error.class, x -> Integer.MAX_VALUE);
         * }</pre>
         *
         * @param <X>           Exception type which the failure is assigned
         * @param exceptionType The specific exception type that should be handled
         * @param f             A recovery function taking an exception of type {@code X1}
         * @return a {@code Try1}
         * @throws NullPointerException if {@code exception} is null or {@code f} is null
         */
        default <X extends Throwable> Try1<S> tryRecover(Class<X> exceptionType, ThrowableFunction1_1<? extends Throwable, ? super X, ? extends S> f) {
            Objects.requireNonNull(exceptionType, "exceptionType is null");
            Objects.requireNonNull(f, "f is null");
            if (isFailure()) {
                final Throwable cause = getFailure();
                if (exceptionType.isAssignableFrom(cause.getClass())) {
                    try {
                        return new Try1.Success<>(f.apply((X) cause));
                    } catch (Throwable e) {
                        return new Try1.Failure<>(e);
                    }
                }
            }
            return this;
        }

        /**
         * Equivalent of {@link #orElse(Try1)}
         *
         * @param recovered {@link Try1} to return if this is a {@code Failure}
         * @return this if this is a Success, given {@code recovered} parameter otherwise
         */
        default Try1<S> recoverWith(Try1<? extends S> recovered){
            Objects.requireNonNull(recovered, "recovered is null");
            return (isFailure()) ? (Try1<S>) recovered : this;
        }
        /**
         * Similar to {@link #recoverWith(Try1)} except that the recovered value is returned if this is a Failure and
         * the Failure is instance of the given {@code exceptionType}.
         *
         * @param exceptionType Exception Type handled by the recover
         * @param recovered {@link Try1} to return when recovering a Failure
         * @return this if this is a Success or a Failure not assignable by {@code exceptionType}.
         * The given parameter {@code recovered} is returned otherwise.
         */
        default <X extends Throwable> Try1<S> recoverWith(Class<X> exceptionType, Try1<? extends S> recovered){
            Objects.requireNonNull(exceptionType, "exceptionType is null");
            Objects.requireNonNull(recovered, "recovered is null");
            return (isFailure() && exceptionType.isAssignableFrom(getFailure().getClass()))
                ? (Try1<S>) recovered
                : this;
        }

        /**
         * Shortcut of {@link #tryRecoverWith(ThrowableFunction1_1)} but declares exception of the given function may throw.
         *
         * @param f function to apply if this is a Failure
         * @return A {@link Try1} supplied by the given function in case of Failure
         * @throws X exception that the given function may throw
         */
        default <X extends Throwable> Try1<S> recoverWith(ThrowableFunction1_1<X, ? super Throwable, Try1<? extends S>> f) throws X {
            Objects.requireNonNull(f, "f is null");
            return tryRecoverWith(f);
        }
        /**
         * Recover the current {@link Try1} if this is a Failure to a new Success by applying the given function taking
         * the exception Failure. <br/>
         * Returns this if this is already a Success.
         *
         * @param f function to apply if this is a Failure
         * @return A {@link Try1} supplied by the given function in case of Failure
         */
        default Try1<S> tryRecoverWith(ThrowableFunction1_1<? extends Throwable, ? super Throwable, Try1<? extends S>> f) {
            Objects.requireNonNull(f, "f is null");
            if (isFailure()) {
                try {
                    return (Try1<S>) f.apply(getFailure());
                } catch (Throwable t) {
                    return new Try1.Failure<>(t);
                }
            } else {
                return this;
            }
        }

        /**
         * Returns {@code this}, if this is a {@code Success} or this is a {@code Failure} and the cause is not assignable
         * from {@code failure.getClass()}. Otherwise tries to recover the failure's exception with a throwable function
         * {@code f} <b>which returns Try1</b> and the exception may throw is declared.
         * If {@link Try#isFailure()} returned by {@code f} function is <code>true</code> it means that
         * recovery cannot take place due to some circumstances.
         *
         * <pre>{@code
         * // = Success(13)
         * Try.of(() -> 27/2).recoverWith(ArithmeticException.class, x -> Try.success(Integer.MAX_VALUE));
         *
         * // = Success(2147483647)
         * Try.of(() -> 1/0)
         *    .recoverWith(Error.class, x -> Try.success(-1))
         *    .recoverWith(ArithmeticException.class, x -> Try.success(Integer.MAX_VALUE));
         *
         * // = Failure(java.lang.ArithmeticException: / by zero)
         * Try.of(() -> 1/0).recoverWith(Error.class, x -> Try.success(Integer.MAX_VALUE));
         * }</pre>
         *
         * @param <X1>          Exception type which has to be assignable to the failure
         * @param <X2>          Exception type that recovery function f may throw
         * @param exceptionType The specific exception type that should be handled
         * @param f             A recovery function taking an exception of type {@code X1} and returning Try as a result of recovery and may throw {@code X2}.
         *                      If Try is {@link Try#isSuccess()} then recovery ends up successfully. Otherwise the function was not able to recover.
         * @return a {@code Try1}
         * @throws NullPointerException if {@code exceptionType} or {@code f} is null
         */
        default <X1 extends Throwable, X2 extends Throwable> Try1<S> recoverWith(Class<X1> exceptionType, ThrowableFunction1_1<X2, ? super X1, Try1<? extends S>> f) throws X2 {
            Objects.requireNonNull(exceptionType, "exceptionType is null");
            Objects.requireNonNull(f, "f is null");
            return tryRecoverWith(exceptionType, f);
        }

        /**
         * Returns {@code this}, if this is a {@code Success} or this is a {@code Failure} and the cause is not assignable
         * from {@code failure.getClass()}. Otherwise tries to recover the failure's exception with a throwable function
         * {@code f} <b>which returns Try1</b>.
         * If {@link Try#isFailure()} returned by {@code f} function is <code>true</code> it means that
         * recovery cannot take place due to some circumstances.
         *
         * <pre>{@code
         * // = Success(13)
         * Try.of(() -> 27/2).recoverWith(ArithmeticException.class, x -> Try.success(Integer.MAX_VALUE));
         *
         * // = Success(2147483647)
         * Try.of(() -> 1/0)
         *    .recoverWith(Error.class, x -> Try.success(-1))
         *    .recoverWith(ArithmeticException.class, x -> Try.success(Integer.MAX_VALUE));
         *
         * // = Failure(java.lang.ArithmeticException: / by zero)
         * Try.of(() -> 1/0).recoverWith(Error.class, x -> Try.success(Integer.MAX_VALUE));
         * }</pre>
         *
         * @param <X>          Exception type which has to be assignable to the failure
         * @param exceptionType The specific exception type that should be handled
         * @param f             A recovery function taking an exception of type {@code X} and returning Try as a result of recovery.
         *                      If Try is {@link Try#isSuccess()} then recovery ends up successfully. Otherwise the function was not able to recover.
         * @return a {@code Try1}
         * @throws NullPointerException if {@code exceptionType} or {@code f} is null
         */
        @SuppressWarnings("unchecked")
        default <X extends Throwable> Try1<S> tryRecoverWith(Class<X> exceptionType, ThrowableFunction1_1<? extends Throwable, ? super X, Try1<? extends S>> f){
            Objects.requireNonNull(exceptionType, "exceptionType is null");
            Objects.requireNonNull(f, "f is null");
            if(isFailure()){
                final Throwable cause = getFailure();
                if (exceptionType.isAssignableFrom(cause.getClass())) {
                    try {
                        return (Try1<S>) f.apply((X) cause);
                    } catch (Throwable t) {
                        return new Try1.Failure<>(t);
                    }
                }
            }
            return this;
        }

        default <IR extends Collection<R>, R> Try1Iterable<IR, R> iterate(IR collection) {
            if (isSuccess()) {
                if(Iterable.class.isAssignableFrom(getSuccess().getClass())) {
                    collection.addAll(StreamSupport.stream(((Iterable<R>)getSuccess()).spliterator(), false).toList());
                    return new Try1Iterable.Success<>(collection);
                } else
                    throw new UnsupportedOperationException("Cannot iterate on "+getSuccess().getClass());
            } else {
                return new Try1Iterable.Failure<>(getFailure());
            }
        }
        default Either.Either1<Throwable, S> toEither() {
            if (isFailure()) {
                return new Either.Either1.Failure<>(getFailure());
            } else {
                return new Either.Either1.Success<>(getSuccess());
            }
        }
        default <E> Either.Either1<E, S> toEither(E error) {
            if (isFailure()) {
                return new Either.Either1.Failure<>(error);
            } else {
                return new Either.Either1.Success<>(getSuccess());
            }
        }
        default <E> Either.Either1<E, S> toEither(Supplier<? extends E> supplier) {
            if (isFailure()) {
                return new Either.Either1.Failure<>(supplier.get());
            } else {
                return new Either.Either1.Success<>(getSuccess());
            }
        }
        default <E> Either.Either1<E, S> toEither(Function<? super Throwable, ? extends E> function) {
            if (isFailure()) {
                return new Either.Either1.Failure<>(function.apply(getFailure()));
            } else {
                return new Either.Either1.Success<>(getSuccess());
            }
        }

        default <X extends Throwable> Try1<S> andFinally(ThrowableFunction0_0<X> runnable) throws X {
            Objects.requireNonNull(runnable, "runnable is null");
            return andFinallyTry(runnable);
        }
        default <X extends Throwable> Try1<S> andFinally(ThrowableFunction1_0<X, ? super S> runnable) throws X {
            Objects.requireNonNull(runnable, "runnable is null");
            return andFinallyTry(runnable);
        }

        default Try1<S> andFinallyTry(ThrowableFunction0_0<? extends Throwable> runnable) {
            Objects.requireNonNull(runnable, "runnable is null");
            try {
                runnable.apply();
                return this;
            } catch (Throwable t) {
                return new Try1.Failure<>(t);
            }
        }
        default Try1<S> andFinallyTry(ThrowableFunction1_0<? extends Throwable, ? super S> function) {
            Objects.requireNonNull(function, "function is null");
            try {
                function.apply(getSuccess());
                return this;
            } catch (Throwable t) {
                return new Try1.Failure<>(t);
            }
        }

        final class Success<S> extends Try.Success<S> implements Try1<S> {
            Success(S value) {
                super(value);
            }
        }

        final class Failure<S> extends Try.Failure<S> implements Try1<S>, Try<S> {
            Failure(Throwable value) {
                super(value);
            }
        }
    }

    interface Try2<S1, S2> extends Try<Tuple2<S1, S2>> {
        default <X extends Throwable> Try2<S1, S2> andThen(ThrowableFunction0_0<X> runnable) throws X {
            Objects.requireNonNull(runnable, "runnable is null");
            return andThenTry(runnable);
        }
        default Try2<S1, S2> andThenTry(ThrowableFunction0_0<? extends Throwable> runnable) {
            Objects.requireNonNull(runnable, "runnable is null");
            if (isFailure()) {
                return this;
            } else {
                try {
                    runnable.apply();
                    return this;
                } catch (Throwable t) {
                    return new Try2.Failure<>(t);
                }
            }
        }
        default <X1 extends Throwable, X2 extends Throwable> Try2<S1, S2> andThenWhen(ThrowablePredicate2<X1, ? super S1, ? super S2> predicate, ThrowableFunction0_0<X2> runnable) throws X1, X2 {
            Objects.requireNonNull(runnable, "runnable is null");
            return andThenTryWhen(predicate, runnable);
        }
        default Try2<S1, S2> andThenTryWhen(ThrowablePredicate2<? extends Throwable, ? super S1, ? super S2> predicate, ThrowableFunction0_0<? extends Throwable> runnable) {
            Objects.requireNonNull(runnable, "runnable is null");
            if (isFailure()) {
                return this;
            }
            try {
                Tuple2<S1, S2> success = getSuccess();
                if(predicate.test(success.t1(), success.t2())) {
                    runnable.apply();
                }
                return this;
            } catch (Throwable t) {
                return new Try2.Failure<>(t);
            }
        }

        default <X extends Throwable> Try2<S1, S2> andThen(ThrowableFunction2_0<X, ? super S1, ? super S2> consumer) throws X {
            Objects.requireNonNull(consumer, "consumer is null");
            return andThenTry(consumer);
        }
        default Try2<S1, S2> andThenTry(ThrowableFunction2_0<? extends Throwable, ? super S1, ? super S2> consumer) {
            Objects.requireNonNull(consumer, "consumer is null");
            if (isFailure()) {
                return this;
            } else {
                try {
                    Tuple2<S1, S2> success = getSuccess();
                    consumer.apply(success.t1(), success.t2());
                    return this;
                } catch (Throwable t) {
                    return new Try2.Failure<>(t);
                }
            }
        }
        default <X1 extends Throwable, X2 extends Throwable> Try2<S1, S2> andThenWhen(ThrowablePredicate2<X1, ? super S1, ? super S2> predicate, ThrowableFunction2_0<X2, ? super S1, ? super S2> consumer) throws X1, X2 {
            Objects.requireNonNull(consumer, "consumer is null");
            return andThenTryWhen(predicate, consumer);
        }
        default Try2<S1, S2> andThenTryWhen(ThrowablePredicate2<? extends Throwable, ? super S1, ? super S2> predicate, ThrowableFunction2_0<? extends Throwable, ? super S1, ? super S2> consumer) {
            Objects.requireNonNull(consumer, "consumer is null");
            if (isFailure()) {
                return this;
            } else {
                try {
                    Tuple2<S1, S2> success = getSuccess();
                    if(predicate.test(success.t1(), success.t2())) {
                        consumer.apply(success.t1(), success.t2());
                    }
                    return this;
                } catch (Throwable t) {
                    return new Try2.Failure<>(t);
                }
            }
        }

        default Try2<S1, S2> peek(Consumer<? super Throwable> failureAction, BiConsumer<? super S1, ? super S2> successAction) {
            Objects.requireNonNull(failureAction, "failureAction is null");
            Objects.requireNonNull(successAction, "successAction is null");
            if (isSuccess()) {
                Tuple2<S1, S2> success = getSuccess();
                successAction.accept(success.t1(), success.t2());
            } else {
                failureAction.accept(getFailure());
            }
            return this;
        }
        default Try2<S1, S2> peek(BiConsumer<? super S1, ? super S2> action) {
            Objects.requireNonNull(action, "action is null");
            if (isSuccess()) {
                Tuple2<S1, S2> success = getSuccess();
                action.accept(success.t1(), success.t2());
            }
            return this;
        }

        default <X extends Throwable, R1, R2> Try2<R1, R2> map(ThrowableFunction2_2<X, ? super S1, ? super S2, ? extends R1, ? extends R2> mapper) throws X {
            Objects.requireNonNull(mapper, "mapper is null");
            return tryMap(mapper);
        }
        default <R1, R2> Try2<R1, R2> tryMap(ThrowableFunction2_2<? extends Throwable, ? super S1, ? super S2, ? extends R1, ? extends R2> mapper) {
            Objects.requireNonNull(mapper, "mapper is null");
            if(isSuccess()) {
                try {
                    Tuple2<S1, S2> success = getSuccess();
                    return (Try2<R1, R2>)new Try2.Success<>(mapper.apply(success.t1(), success.t2()));
                } catch (Throwable e) {
                    return new Try2.Failure<>(e);
                }
            } else {
                return (Try2<R1, R2>) this;
            }
        }
        default <X extends Throwable, R1, R2> Try2<R1, R2> flatMap(ThrowableFunction2_1<X, ? super S1, ? super S2, Try2<? extends R1, ? extends R2>> mapper) throws X {
            Objects.requireNonNull(mapper, "mapper is null");
            return tryFlatMap(mapper);
        }

        default <R1, R2> Try2<R1, R2> tryFlatMap(ThrowableFunction2_1<? extends Throwable, ? super S1, ? super S2, Try2<? extends R1, ? extends R2>> mapper) {
            Objects.requireNonNull(mapper, "mapper is null");
            if (isSuccess()) {
                try {
                    Tuple2<S1, S2> success = getSuccess();
                    return (Try2<R1, R2>)mapper.apply(success.t1(), success.t2());
                } catch (Throwable e) {
                    return new Try2.Failure<>(e);
                }
            } else {
                return (Try2<R1, R2>) this;
            }
        }

        default <X extends Throwable, R> Try1<R> map1(ThrowableFunction2_1<X, ? super S1, ? super S2, ? extends R> mapper) throws X {
            Objects.requireNonNull(mapper, "mapper is null");
            return tryMap1(mapper);
        }
        default <R> Try1<R> tryMap1(ThrowableFunction2_1<? extends Throwable, ? super S1, ? super S2, ? extends R> mapper) {
            Objects.requireNonNull(mapper, "mapper is null");
            if(isSuccess()) {
                try {
                    Tuple2<S1, S2> success = getSuccess();
                    return new Try1.Success<>(mapper.apply(success.t1(), success.t2()));
                } catch (Throwable e) {
                    return new Try1.Failure<>(e);
                }
            } else {
                return (Try1<R>) this;
            }
        }

        default <X extends Throwable, R> Try1<R> flatMap1(ThrowableFunction2_1<X, ? super S1, ? super S2, Try1<? extends R>> mapper) throws X {
            Objects.requireNonNull(mapper, "mapper is null");
            return tryFlatMap1(mapper::apply);
        }

        default <R> Try1<R> tryFlatMap1(ThrowableFunction2_1<? extends Throwable, ? super S1, ? super S2, Try1<? extends R>> mapper) {
            Objects.requireNonNull(mapper, "mapper is null");
            if (isSuccess()) {
                try {
                    Tuple2<S1, S2> success = getSuccess();
                    return (Try1<R>)mapper.apply(success.t1(), success.t2());
                } catch (Throwable e) {
                    return new Try1.Failure<>(e);
                }
            } else {
                return (Try1<R>) this;
            }
        }

        default <X extends Throwable> Try2<S1, S2> throwing(Class<X> clazz) throws X {
            return this;
        }

        default Try2<S1, S2> onFailure(Runnable action) {
            Objects.requireNonNull(action, "action is null");
            if (isFailure()) {
                action.run();
            }
            return this;
        }

        default Try2<S1, S2> onFailure(Consumer<? super Throwable> action) {
            Objects.requireNonNull(action, "action is null");
            if (isFailure()) {
                action.accept(getFailure());
            }
            return this;
        }
        default <X> Try2<S1, S2> onFailure(Class<X> clazz, Consumer<? super X> action) {
            Objects.requireNonNull(action, "action is null");
            if (isFailure() && clazz.isAssignableFrom(getFailure().getClass())) {
                action.accept((X)getFailure());
            }
            return this;
        }
        default <X extends Throwable> Try2<S1, S2> mapFailure(Function<? super Throwable, ? extends X> mapper) {
            Objects.requireNonNull(mapper, "mapper is null");
            if (isFailure()) {
                return new Try2.Failure<>(mapper.apply(getFailure()));
            }
            return this;
        }
        default <X extends Throwable> Try2<S1, S2> mapFailure(Class<X> clazz, Function<? super X, ? extends Throwable> mapper) {
            Objects.requireNonNull(mapper, "mapper is null");
            if (isFailure() && clazz.isAssignableFrom(getFailure().getClass())) {
                return new Try2.Failure<>(mapper.apply((X)getFailure()));
            }
            return this;
        }
        default <X extends Throwable, Y extends Throwable> Try2<S1, S2> mapFailures(List<Class<? extends X>> clazzes, Function<? super X, ? extends Y> mapper) {
            Objects.requireNonNull(mapper, "mapper is null");
            if(isFailure() && clazzes.stream().anyMatch(clazz -> clazz.isAssignableFrom(getFailure().getClass()))) {
                return new Try2.Failure<>(mapper.apply((X)getFailure()));
            }
            return this;
        }

        default <X extends Throwable> Try2<S1, S2> onSuccess(ThrowableFunction2_0<X, ? super S1, ? super S2> action) throws X {
            Objects.requireNonNull(action, "action is null");
            return andThen(action);
        }
        default Try2<S1, S2> onSuccessTry(ThrowableFunction2_0<? extends Throwable, ? super S1, ? super S2> action) {
            Objects.requireNonNull(action, "action is null");
            return andThenTry(action);
        }

        default Try2<S1, S2> orElse(Try2<? extends S1, ? extends S2> other) {
            Objects.requireNonNull(other, "other is null");
            return isSuccess() ? this : (Try2<S1, S2>) other;
        }

        default Try2<S1, S2> orElse(Supplier<Try2<? extends S1, ? extends S2>> supplier) {
            Objects.requireNonNull(supplier, "supplier is null");
            return isSuccess() ? this : (Try2<S1, S2>) supplier.get();
        }

        /**
         * Shortcut for {@code filterTry(predicate::test)}, see {@link #tryFilter(ThrowablePredicate2)}}.
         *
         * @param predicate A predicate
         * @return a {@code Try} instance
         * @throws NullPointerException if {@code predicate} is null
         */
        default <X extends Throwable> Try2<S1, S2> filter(ThrowablePredicate2<X, ? super S1, ? super S2> predicate) throws X {
            Objects.requireNonNull(predicate, "predicate is null");
            return tryFilter(predicate);
        }

        /**
         * Contrary to filter, see {@link #filter(ThrowablePredicate2)}.
         *
         * @param predicate A predicate
         * @return a {@code Try} instance
         * @throws NullPointerException if {@code predicate} is null
         */
        default <X extends Throwable> Try2<S1, S2> filterNot(ThrowablePredicate2<X, ? super S1, ? super S2> predicate) throws X {
            Objects.requireNonNull(predicate, "predicate is null");
            return filter(predicate.negate());
        }

        /**
         * Returns {@code this} if this is a Failure or this is a Success and the value satisfies the predicate.
         * <p>
         * Returns a new Failure, if this is a Success and the value does not satisfy the Predicate or an exception
         * occurs testing the predicate. The returned Failure wraps a {@link NoSuchElementException} instance.
         *
         * @param predicate A checked predicate
         * @return a {@code Try} instance
         * @throws NullPointerException if {@code predicate} is null
         */
        default Try2<S1, S2> tryFilter(ThrowablePredicate2<? extends Throwable, ? super S1, ? super S2> predicate) {
            Objects.requireNonNull(predicate, "predicate is null");
            return tryFilter(predicate, () -> new NoSuchElementException("Predicate does not hold for " + getSuccess()));
        }

        /**
         * Shortcut for {@code filterTry(predicate::test, throwableSupplier)}, see
         * {@link #tryFilter(ThrowablePredicate2, Supplier)}}.
         *
         * @param predicate         A predicate
         * @param throwableSupplier A supplier of a throwable
         * @return a {@code Try} instance
         * @throws NullPointerException if {@code predicate} or {@code throwableSupplier} is null
         */
        default <X extends Throwable> Try2<S1, S2> filter(BiPredicate<? super S1, ? super S2> predicate, Supplier<X> throwableSupplier) throws X {
            Objects.requireNonNull(predicate, "predicate is null");
            Objects.requireNonNull(throwableSupplier, "throwableSupplier is null");
            return tryFilter(predicate::test, throwableSupplier);
        }

        /**
         * Shortcut for {@code filterTry(predicate::test, errorProvider::apply)}, see
         * {@link #tryFilter(ThrowablePredicate2, ThrowableFunction2_1)}}.
         *
         * @param predicate A predicate
         * @param errorProvider A function that provides some kind of Throwable for T
         * @return a {@code Try} instance
         * @throws NullPointerException if {@code predicate} or {@code errorProvider} is null
         */
        default <X extends Throwable> Try2<S1, S2> filter(BiPredicate<? super S1, ? super S2> predicate, BiFunction<? super S1, ? super S2, X> errorProvider) throws  X{
            Objects.requireNonNull(predicate, "predicate is null");
            Objects.requireNonNull(errorProvider, "errorProvider is null");
            return tryFilter(predicate::test, errorProvider::apply);
        }

        /**
         * Contrary to filter, see {@link #filter(BiPredicate, Supplier)}.
         *
         * @param predicate         A predicate
         * @param throwableSupplier A supplier of a throwable
         * @return a {@code Try} instance
         * @throws NullPointerException if {@code predicate} or {@code throwableSupplier} is null
         */
        default <X extends Throwable> Try2<S1, S2> filterNot(BiPredicate<? super S1, ? super S2> predicate, Supplier<X> throwableSupplier) throws X {
            Objects.requireNonNull(predicate, "predicate is null");
            return filter(predicate.negate(), throwableSupplier);
        }


        /**
         * Contrary to filter, see {@link #filter(BiPredicate, BiFunction)}.
         *
         * @param predicate A predicate
         * @param errorProvider A function that provides some kind of Throwable for T
         * @return a {@code Try} instance
         * @throws NullPointerException if {@code predicate} or {@code errorProvider} is null
         */
        default <X extends Throwable> Try2<S1, S2> filterNot(BiPredicate<? super S1, ? super S2> predicate, BiFunction<? super S1, ? super S2, X> errorProvider) throws X {
            Objects.requireNonNull(predicate, "predicate is null");
            return filter(predicate.negate(), errorProvider);
        }

        /**
         * Returns {@code this} if this is a Failure or this is a Success and the value satisfies the predicate.
         * <p>
         * Returns a new Failure, if this is a Success and the value does not satisfy the Predicate or an exception
         * occurs testing the predicate. The returned Failure wraps a Throwable instance provided by the given
         * {@code throwableSupplier}.
         *
         * @param predicate         A checked predicate
         * @param throwableSupplier A supplier of a throwable
         * @return a {@code Try} instance
         * @throws NullPointerException if {@code predicate} or {@code throwableSupplier} is null
         */
        default Try2<S1, S2> tryFilter(ThrowablePredicate2<? extends Throwable, ? super S1, ? super S2> predicate, Supplier<? extends Throwable> throwableSupplier) {
            Objects.requireNonNull(predicate, "predicate is null");
            Objects.requireNonNull(throwableSupplier, "throwableSupplier is null");

            if (isFailure()) {
                return this;
            } else {
                try {
                    Tuple2<S1, S2> success = getSuccess();
                    if (predicate.test(success.t1(), success.t2())) {
                        return this;
                    } else {
                        return new Try2.Failure<>(throwableSupplier.get());
                    }
                } catch (Throwable t) {
                    return new Try2.Failure<>(t);
                }
            }
        }

        /**
         * Returns {@code this} if this is a Failure or this is a Success and the value satisfies the predicate.
         * <p>
         * Returns a new Failure, if this is a Success and the value does not satisfy the Predicate or an exception
         * occurs testing the predicate. The returned Failure wraps a Throwable instance provided by the given
         * {@code errorProvider}.
         *
         * @param predicate         A checked predicate
         * @param errorProvider     A provider of a throwable
         * @return a {@code Try} instance
         * @throws NullPointerException if {@code predicate} or {@code errorProvider} is null
         */
        default Try2<S1, S2> tryFilter(ThrowablePredicate2<? extends Throwable, ? super S1, ? super S2> predicate, ThrowableFunction2_1<? extends Throwable, ? super S1, ? super S2, ? extends Throwable> errorProvider) {
            Objects.requireNonNull(predicate, "predicate is null");
            Objects.requireNonNull(errorProvider, "errorProvider is null");
            return tryFlatMap((s1, s2) -> predicate.test(s1, s2) ? this : new Try2.Failure<>(errorProvider.apply(s1, s2)));
        }

        default <X extends Throwable> Try2<S1, S2> recover(Class<X> exceptionType, S1 value1, S2 value2) {
            Objects.requireNonNull(exceptionType, "exceptionType is null");
            return (isFailure() && exceptionType.isAssignableFrom(getFailure().getClass()))
                ? Try.success(value1, value2)
                : this;
        }

        default Try2<S1, S2> recover(S1 value1, S2 value2) {
            return (isFailure()) ? Try.success(value1, value2) : this;
        }


        default <X extends Throwable> Try2<S1, S2> recover(Class<X> exceptionType, Function<? super X, Tuple2<? extends S1, ? extends S2>> f) {
            Objects.requireNonNull(exceptionType, "exceptionType is null");
            Objects.requireNonNull(f, "f is null");
            if (isFailure()) {
                final Throwable cause = getFailure();
                if (exceptionType.isAssignableFrom(cause.getClass())) {
                    return new Try2.Success<>((Tuple2<S1, S2>)f.apply((X) cause));
                }
            }
            return this;
        }

        default Try2<S1, S2> recover(Function<? super Throwable, Tuple2<? extends S1, ? extends S2>> f) {
            Objects.requireNonNull(f, "f is null");
            if (isFailure()) {
                return Try.success((Tuple2<S1, S2>)f.apply(getFailure()));
            } else {
                return this;
            }
        }

        default <X extends Throwable> Try2<S1, S2> recoverWith(Class<X> exceptionType,  Try2<? extends S1, ? extends S2> recovered){
            Objects.requireNonNull(exceptionType, "exeptionType is null");
            Objects.requireNonNull(recovered, "recovered is null");
            return (isFailure() && exceptionType.isAssignableFrom(getFailure().getClass()))
                ? (Try2<S1, S2>) recovered
                : this;
        }

        default Try2<S1, S2> recoverWith(Try2<? extends S1, ? extends S2> recovered){
            Objects.requireNonNull(recovered, "recovered is null");
            return (isFailure()) ? (Try2<S1, S2>) recovered : this;
        }

        default <X extends Throwable> Try2<S1, S2> recoverWith(Class<X> exceptionType, Function<? super X, Try2<? extends S1, ? extends S2>> f){
            Objects.requireNonNull(exceptionType, "exceptionType is null");
            Objects.requireNonNull(f, "f is null");
            if(isFailure()){
                final Throwable cause = getFailure();
                if (exceptionType.isAssignableFrom(cause.getClass())) {
                    try {
                        return (Try2<S1, S2>) f.apply((X) cause);
                    } catch (Throwable t) {
                        return new Try2.Failure<>(t);
                    }
                }
            }
            return this;
        }

        default Try2<S1, S2> recoverWith(Function<? super Throwable, Try2<? extends S1, ? extends S2>> f) {
            Objects.requireNonNull(f, "f is null");
            if (isFailure()) {
                try {
                    return (Try2<S1, S2>) f.apply(getFailure());
                } catch (Throwable t) {
                    return new Try2.Failure<>(t);
                }
            } else {
                return this;
            }
        }

        default Either.Either2<Throwable, S1, S2> toEither() {
            if (isFailure()) {
                return new Either.Either2.Failure<>(getFailure());
            } else {
                return new Either.Either2.Success<>(getSuccess());
            }
        }

        default <E> Either.Either2<E, S1, S2> toEither(E error) {
            if (isFailure()) {
                return new Either.Either2.Failure<>(error);
            } else {
                return new Either.Either2.Success<>(getSuccess());
            }
        }
        default <E> Either.Either2<E, S1, S2> toEither(Supplier<? extends E> supplier) {
            if (isFailure()) {
                return new Either.Either2.Failure<>(supplier.get());
            } else {
                return new Either.Either2.Success<>(getSuccess());
            }
        }
        default <E> Either.Either2<E, S1, S2> toEither(Function<? super Throwable, ? extends E> function) {
            if (isFailure()) {
                return new Either.Either2.Failure<>(function.apply(getFailure()));
            } else {
                return new Either.Either2.Success<>(getSuccess());
            }
        }

        default <X extends Throwable> Try2<S1, S2> andFinally(ThrowableFunction0_0<X> runnable) throws X {
            Objects.requireNonNull(runnable, "runnable is null");
            return andFinallyTry(runnable);
        }

        default Try2<S1, S2> andFinallyTry(ThrowableFunction0_0<? extends Throwable> runnable) {
            Objects.requireNonNull(runnable, "runnable is null");
            try {
                runnable.apply();
                return this;
            } catch (Throwable t) {
                return new Try2.Failure<>(t);
            }
        }
        default Try2<S1, S2> andFinallyTry(ThrowableFunction2_0<? extends Throwable, ? super S1, ? super S2> function) {
            Objects.requireNonNull(function, "function is null");
            try {
                Tuple2<S1, S2> success = getSuccess();
                function.apply(success.t1(), success.t2());
                return this;
            } catch (Throwable t) {
                return new Try2.Failure<>(t);
            }
        }

        final class Success<S1, S2> extends Try.Success<Tuple2<S1, S2>> implements Try2<S1, S2> {
            Success(S1 t1, S2 t2) {
                super(Tuple.of(t1, t2));
            }
            Success(Tuple2<S1, S2> value) {
                super(value);
            }
        }

        final class Failure<S1, S2> extends Try.Failure<Tuple2<S1, S2>> implements Try2<S1, S2> {
            Failure(Throwable cause) {
                super(cause);
            }
        }
    }

    interface Try1Iterable<IS extends Iterable<S>, S> extends Try1<IS> {
        default Try1Iterable<IS, S> peekEach(Consumer<? super S> action) {
            Objects.requireNonNull(action, "action is null");
            return andThenTryEach(action::accept);
        }
        default <X extends Throwable> Try1Iterable<IS, S> andThenEach(ThrowableFunction1_0<X, ? super S> action) throws X {
            Objects.requireNonNull(action, "action is null");
            return andThenTryEach(action);
        }
        default Try1Iterable<IS, S> andThenTryEach(ThrowableFunction1_0<? extends Throwable, ? super S> action) {
            Objects.requireNonNull(action, "action is null");
            if (isSuccess()) {
                try {
                    for (S item : getSuccess()) {
                        action.apply(item);
                    }
                    return this;
                } catch (Throwable e) {
                    return new Try1Iterable.Failure<>(e);
                }
            } else {
                return this;
            }
        }

        default <X extends Throwable, R> Try1Iterable<? extends Collection<R>, R> mapEach(ThrowableFunction1_1<X, ? super S, ? extends R> mapper) throws X {
            Objects.requireNonNull(mapper, "mapper is null");
            return tryMapEach(mapper);
        }
        default <R> Try1Iterable<? extends Collection <R>,R> tryMapEach(ThrowableFunction1_1<? extends Throwable, ? super S, ? extends R> mapper) {
            Objects.requireNonNull(mapper, "mapper is null");
            return tryMapEach(mapper, new Vector<>());
        }
        default <X extends Throwable, IR extends Collection<R>, R> Try1Iterable<IR, R> mapEach(ThrowableFunction1_1<X, ? super S, ? extends R> mapper, IR target) throws X {
            Objects.requireNonNull(mapper, "mapper is null");
            return tryMapEach(mapper, target);
        }
        default <IR extends Collection<R>, R> Try1Iterable<IR, R> tryMapEach(ThrowableFunction1_1<? extends Throwable, ? super S, ? extends R> mapper, IR target) {
            Objects.requireNonNull(mapper, "mapper is null");
            if (isSuccess()) {
                try {
                    for (S item : getSuccess()) {
                        target.add(mapper.apply(item));
                    }
                    return new Try1Iterable.Success<>(target);
                } catch (Throwable e) {
                    return new Try1Iterable.Failure<>(e);
                }
            } else {
                return (Try1Iterable<IR, R>) this;
            }
        }

        default <X1 extends Throwable, X2 extends Throwable, X3 extends Throwable, R> Try1Iterable<? extends Collection<R>, R> mapEachWhen(ThrowablePredicate1<X1, ? super S> predicate, ThrowableFunction1_1<X2, ? super S, ? extends R> onFalse, ThrowableFunction1_1<X3, ? super S, ? extends R> onTrue) throws X1, X2, X3 {
            Objects.requireNonNull(predicate, "predicate is null");
            Objects.requireNonNull(onFalse, "onFalse is null");
            Objects.requireNonNull(onTrue, "onTrue is null");
            return tryMapEachWhen(predicate, onFalse, onTrue);
        }
        default <R> Try1Iterable<? extends Collection <R>,R> tryMapEachWhen(ThrowablePredicate1<? extends Throwable, ? super S> predicate, ThrowableFunction1_1<? extends Throwable, ? super S, ? extends R> onFalse, ThrowableFunction1_1<? extends Throwable, ? super S, ? extends R> onTrue) {
            Objects.requireNonNull(predicate, "predicate is null");
            Objects.requireNonNull(onFalse, "onFalse is null");
            Objects.requireNonNull(onTrue, "onTrue is null");
            return tryMapEachWhen(predicate, onFalse, onTrue, new Vector<>());
        }
        default <X1 extends Throwable, X2 extends Throwable, X3 extends Throwable, IR extends Collection<R>, R> Try1Iterable<IR, R> mapEachWhen(ThrowablePredicate1<X1, ? super S> predicate, ThrowableFunction1_1<X2, ? super S, ? extends R> onFalse, ThrowableFunction1_1<X2, ? super S, ? extends R> onTrue, IR target) throws X1, X2, X3 {
            Objects.requireNonNull(predicate, "predicate is null");
            Objects.requireNonNull(onFalse, "onFalse is null");
            Objects.requireNonNull(onTrue, "onTrue is null");
            return tryMapEachWhen(predicate, onFalse, onTrue, target);
        }
        default <IR extends Collection<R>, R> Try1Iterable<IR, R> tryMapEachWhen(ThrowablePredicate1<? extends Throwable, ? super S> predicate, ThrowableFunction1_1<? extends Throwable, ? super S, ? extends R> onFalse, ThrowableFunction1_1<? extends Throwable, ? super S, ? extends R> onTrue, IR target) {
            Objects.requireNonNull(predicate, "predicate is null");
            Objects.requireNonNull(onFalse, "onFalse is null");
            Objects.requireNonNull(onTrue, "onTrue is null");
            if (isSuccess()) {
                try {
                    for (S item : getSuccess()) {
                        if(predicate.test(item))
                            target.add(onTrue.apply(item));
                        else
                            target.add(onFalse.apply(item));
                    }
                    return new Try1Iterable.Success<>(target);
                } catch (Throwable e) {
                    return new Try1Iterable.Failure<>(e);
                }
            } else {
                return (Try1Iterable<IR, R>) this;
            }
        }

        default <X extends Throwable, R1, R2> Try2Iterable<? extends Collection<Tuple2<? extends R1, ? extends R2>>, R1, R2> map2Each(ThrowableFunction1_2<X, ? super S, ? extends R1, ? extends R2> mapper) throws X {
            Objects.requireNonNull(mapper, "mapper is null");
            return tryMap2Each(mapper);
        }
        default <R1, R2> Try2Iterable<? extends Collection<Tuple2<? extends R1, ? extends R2>>, R1, R2> tryMap2Each(ThrowableFunction1_2<? extends Throwable, ? super S, ? extends R1, ? extends R2> mapper) {
            Objects.requireNonNull(mapper, "mapper is null");
            return tryMap2Each(mapper, new Vector<>());
        }
        default <X extends Throwable, IR extends Collection<Tuple2<? extends R1, ? extends R2>>, R1, R2> Try2Iterable<IR, R1, R2> map2Each(ThrowableFunction1_2<X, ? super S, ? extends R1, ? extends R2> mapper, IR target) throws X {
            Objects.requireNonNull(mapper, "mapper is null");
            Objects.requireNonNull(target, "target is null");
            return tryMap2Each(mapper, target);
        }
        default <IR extends Collection<Tuple2<? extends R1, ? extends R2>>, R1, R2> Try2Iterable<IR, R1, R2> tryMap2Each(ThrowableFunction1_2<? extends Throwable, ? super S, ? extends R1, ? extends R2> mapper, IR target) {
            Objects.requireNonNull(mapper, "mapper is null");
            Objects.requireNonNull(target, "target is null");
            if (isSuccess()) {
                try {
                    for (S item : getSuccess()) {
                        target.add(mapper.apply(item));
                    }
                    return new Try2Iterable.Success<>(target);
                } catch (Throwable e) {
                    return new Try2Iterable.Failure<>(e);
                }
            } else {
                return new Try2Iterable.Failure<>(getFailure());
            }
        }

        default <X extends Throwable, R> Try2Iterable<? extends Collection<Tuple2<? extends S, ? extends R>>, S, R> addOnEach(ThrowableFunction1_1<X, ? super S, ? extends R> mapper) throws X {
            Objects.requireNonNull(mapper, "mapper is null");
            return tryAddOnEach(mapper);
        }
        default <R> Try2Iterable<? extends Collection<Tuple2<? extends S, ? extends R>>, S, R> tryAddOnEach(ThrowableFunction1_1<? extends Throwable, ? super S, ? extends R> mapper) {
            Objects.requireNonNull(mapper, "mapper is null");
            return tryAddOnEach(mapper, new Vector<>());
        }
        default <X extends Throwable, IR extends Collection<Tuple2<? extends S, ? extends R>>, R> Try2Iterable<IR, S, R> addOnEach(ThrowableFunction1_1<X, ? super S, ? extends R> mapper, IR target) throws X {
            Objects.requireNonNull(mapper, "mapper is null");
            Objects.requireNonNull(target, "target is null");
            return tryAddOnEach(mapper, target);
        }
        default <IR extends Collection<Tuple2<? extends S, ? extends R>>, R> Try2Iterable<IR, S, R> tryAddOnEach(ThrowableFunction1_1<? extends Throwable, ? super S, ? extends R> mapper, IR target) {
            Objects.requireNonNull(mapper, "mapper is null");
            Objects.requireNonNull(target, "target is null");
            if (isSuccess()) {
                try {
                    for (S item : getSuccess()) {
                        target.add(Tuple.of(item, mapper.apply(item)));
                    }
                    return new Try2Iterable.Success<>(target);
                } catch (Throwable e) {
                    return new Try2Iterable.Failure<>(e);
                }
            } else {
                return new Try2Iterable.Failure<>(getFailure());
            }
        }

        default <X1 extends Throwable, X2 extends Throwable, R> Try2Iterable<? extends Collection<Tuple2<? extends S, ? extends Optional<? extends R>>>, S, ? extends Optional<? extends R>> addOnEachWhen(ThrowablePredicate1<X1, ? super S> predicate, ThrowableFunction1_1<X2, ? super S, ? extends R> mapper) throws X1, X2 {
            Objects.requireNonNull(mapper, "predicate is null");
            Objects.requireNonNull(mapper, "mapper is null");
            return (Try2Iterable<? extends Collection<Tuple2<? extends S, ? extends Optional<? extends R>>>, S, ? extends Optional<? extends R>>) tryAddOnEachWhen(predicate, mapper);
        }
        default <R> Try2Iterable<? extends Collection<Tuple2<? extends S, ? extends Optional<? extends R>>>, S, ? extends Optional<? extends R>> tryAddOnEachWhen(ThrowablePredicate1<? extends Throwable, ? super S> predicate, ThrowableFunction1_1<? extends Throwable, ? super S, ? extends R> mapper) {
            Objects.requireNonNull(mapper, "predicate is null");
            Objects.requireNonNull(mapper, "mapper is null");
            return tryAddOnEachWhen(predicate, mapper, new Vector<>());
        }
        default <X1 extends Throwable, X2 extends Throwable, IR extends Collection<Tuple2<? extends S, ? extends Optional<? extends R>>>, R> Try2Iterable<IR, S, ? extends Optional<? extends R>> addOnEachWhen(ThrowablePredicate1<X1, ? super S> predicate, ThrowableFunction1_1<X2, ? super S, ? extends R> mapper, IR target) throws X1, X2 {
            Objects.requireNonNull(mapper, "predicate is null");
            Objects.requireNonNull(mapper, "mapper is null");
            Objects.requireNonNull(target, "target is null");
            return tryAddOnEachWhen(predicate, mapper, target);
        }
        default <IR extends Collection<Tuple2<? extends S, ? extends Optional<? extends R>>>, R> Try2Iterable<IR, S, ? extends Optional<? extends R>> tryAddOnEachWhen(ThrowablePredicate1<? extends Throwable, ? super S> predicate, ThrowableFunction1_1<? extends Throwable, ? super S, ? extends R> mapper, IR target) {
            Objects.requireNonNull(mapper, "predicate is null");
            Objects.requireNonNull(mapper, "mapper is null");
            Objects.requireNonNull(target, "target is null");
            if (isSuccess()) {
                try {
                    for (S item : getSuccess()) {
                        if(predicate.test(item))
                            target.add(Tuple.of(item, Optional.ofNullable(mapper.apply(item))));
                        else
                            target.add(Tuple.of(item, Optional.empty()));
                    }
                    return new Try2Iterable.Success<>(target);
                } catch (Throwable e) {
                    return new Try2Iterable.Failure<>(e);
                }
            } else {
                return new Try2Iterable.Failure<>(getFailure());
            }
        }

        default <X extends Throwable, R> Try1Iterable<? extends Collection<R>, R> flatMapEach(ThrowableFunction1_1<X, ? super S, Try1Iterable<? extends Collection<R>, R>> mapper) throws X {
            Objects.requireNonNull(mapper, "mapper is null");
            return tryFlatMapEach(mapper);
        }
        default <R> Try1Iterable<? extends Collection<R>, R> tryFlatMapEach(ThrowableFunction1_1<? extends Throwable, ? super S, Try1Iterable<? extends Collection<R>, R>> mapper) {
            Objects.requireNonNull(mapper, "mapper is null");
            if (isSuccess()) {
                try {
                    Vector<R> target = new Vector<>();
                    for (S item : getSuccess()) {
                        Try1Iterable<? extends Collection<R>, R> trying = mapper.apply(item);
                        if(trying.isSuccess())
                            target.addAll(trying.getSuccess());
                        else
                            return trying;
                    }
                    return new Try1Iterable.Success<>(target);
                } catch (Throwable e) {
                    return new Try1Iterable.Failure<>(e);
                }
            } else {
                return (Try1Iterable<? extends Collection<R>, R>) this;
            }
        }

        default <X extends Throwable, IR extends Collection<R>, R> Try1Iterable<IR, R> flatMapEach(ThrowableFunction1_1<X, ? super S, Try1Iterable<IR, R>> mapper, IR target) throws X {
            Objects.requireNonNull(mapper, "mapper is null");
            return tryFlatMapEach(mapper, target);
        }
        default <IR extends Collection<R>, R> Try1Iterable<IR,R> tryFlatMapEach(ThrowableFunction1_1<? extends Throwable, ? super S, Try1Iterable<IR, R>> mapper, IR target) {
            Objects.requireNonNull(mapper, "mapper is null");
            if (isSuccess()) {
                try {
                    for (S item : getSuccess()) {
                        Try1Iterable<IR, R> trying = mapper.apply(item);
                        if(trying.isSuccess())
                            target.addAll(trying.getSuccess());
                        else
                            return trying;
                    }
                    return new Try1Iterable.Success<>(target);
                } catch (Throwable e) {
                    return new Try1Iterable.Failure<>(e);
                }
            } else {
                return (Try1Iterable<IR, R>) this;
            }
        }

        default <X extends Throwable> Try1Iterable<? extends Collection<S>, S> filterEach(ThrowablePredicate1<X, ? super S> predicate) throws X {
            Objects.requireNonNull(predicate, "predicate is null");
            return tryFilterEach(predicate);
        }
        default Try1Iterable<? extends Collection<S>, S> tryFilterEach(ThrowablePredicate1<? extends Throwable, ? super S> predicate) {
            Objects.requireNonNull(predicate, "predicate is null");
            return tryFilterEach(predicate, new Vector<>());
        }
        default <X extends Throwable, IT extends Collection<S>> Try1Iterable<IT, S> filterEach(ThrowablePredicate1<X, ? super S> predicate, IT target) throws X {
            Objects.requireNonNull(predicate, "predicate is null");
            return tryFilterEach(predicate, target);
        }
        default <IT extends Collection<S>> Try1Iterable<IT, S> tryFilterEach(ThrowablePredicate1<? extends Throwable, ? super S> predicate, IT target) {
            Objects.requireNonNull(predicate, "predicate is null");
            if (isSuccess()) {
                try {
                    for (S item : getSuccess()) {
                        if (predicate.test(item))
                            target.add(item);
                    }
                    return new Try1Iterable.Success<>(target);
                } catch (Throwable e) {
                    return new Try1Iterable.Failure<>(e);
                }
            } else {
                return (Try1Iterable<IT, S>) this;
            }
        }

        default <X extends Throwable> void forEach(ThrowableFunction1_0<X, ? super S> action) throws X {
            Objects.requireNonNull(action, "action is null");
            forEachTry(action);
        }
        default void forEachTry(ThrowableFunction1_0<? extends Throwable, ? super S> action) {
            Objects.requireNonNull(action, "action is null");
            if (isSuccess()) {
                try {
                    for (S item : getSuccess()) {
                        action.apply(item);
                    }
                } catch (Throwable e) {
                    sneakyThrow(e);
                }
            } else {
                sneakyThrow(getFailure());
            }
        }

        default <X extends Throwable, R> Try1<R> reduce(R identity, ThrowableFunction2_1<X, ? super R, ? super S, ? extends R> accumulator) throws X {
            Objects.requireNonNull(identity, "identity is null");
            Objects.requireNonNull(accumulator, "accumulator is null");
            return tryReduce(identity, accumulator);
        }
        default <R> Try1<R> tryReduce(R identity, ThrowableFunction2_1<? extends Throwable, ? super R, ? super S, ? extends R> accumulator) {
            Objects.requireNonNull(identity, "identity is null");
            Objects.requireNonNull(accumulator, "accumulator is null");
            if (isSuccess()) {
                try {
                    R current = identity;
                    for (S item : getSuccess()) {
                        current = accumulator.apply(current, item);
                    }
                    return new Try1.Success<>(current);
                } catch (Throwable e) {
                    return new Try1.Failure<>(e);
                }
            } else {
                return new Try1.Failure<>(getFailure());
            }
        }

        default <X extends Throwable> Try1<Optional<S>> reduce(ThrowableFunction2_1<X, ? super S, ? super S, ? extends S> accumulator) throws X {
            Objects.requireNonNull(accumulator, "accumulator is null");
            return tryReduce(accumulator);
        }
        default Try1<Optional<S>> tryReduce(ThrowableFunction2_1<? extends Throwable, ? super S, ? super S, ? extends S> accumulator) {
            Objects.requireNonNull(accumulator, "accumulator is null");
            if (isSuccess()) {
                try {
                    boolean foundAny = false;
                    S result = null;
                    for (S item : getSuccess()) {
                        if (!foundAny) {
                            foundAny = true;
                            result = item;
                        } else
                            result = accumulator.apply(result, item);
                    }
                    return foundAny ?
                        new Try1.Success<>(Optional.ofNullable(result)) :
                        new Try1.Success<>(Optional.empty());
                } catch (Throwable e) {
                    return new Try1.Failure<>(e);
                }
            } else {
                return new Try1.Failure<>(getFailure());
            }
        }

        default <R> Try2Iterable<List<Tuple2<? extends S, ? extends R>>, S, R> zip(Iterable<R> other) {
            Objects.requireNonNull(other, "other is null");
            if (isSuccess()) {
                try {
                    Iterator<S> sIterator = getSuccess().iterator();
                    Iterator<R> otherIterator = other.iterator();

                    Vector<Tuple2<? extends S, ? extends R>> result = new Vector<>();
                    while(sIterator.hasNext() && otherIterator.hasNext()) {
                        result.add(Tuple.of(sIterator.next(), otherIterator.next()));
                    }
                    if(sIterator.hasNext() || otherIterator.hasNext()) {
                        throw new IndexOutOfBoundsException("Sizes of iterables to be zipped must be equals : \n"+getSuccess()+"\n"+other);
                    }
                    return new Try2Iterable.Success<>(result);
                } catch (Throwable e) {
                    return new Try2Iterable.Failure<>(e);
                }
            } else {
                return new Try2Iterable.Failure<>(getFailure());
            }
        }

        default Try2Iterable<List<Tuple2<? extends Integer, ? extends S>>, Integer, S> zipWithIndex() {
            if (isSuccess()) {
                try {
                    Iterator<S> sIterator = getSuccess().iterator();
                    int index = 0;
                    Vector<Tuple2<? extends Integer, ? extends S>> result = new Vector<>();
                    while(sIterator.hasNext()) {
                        result.add(Tuple.of(index++, sIterator.next()));
                    }
                    return new Try2Iterable.Success<>(result);
                } catch (Throwable e) {
                    return new Try2Iterable.Failure<>(e);
                }
            } else {
                return new Try2Iterable.Failure<>(getFailure());
            }
        }

        default List<S> toList() {
            if(isSuccess())
                return StreamSupport.stream(getSuccess().spliterator(), false).toList();
            else {
                return sneakyThrow(getFailure());
            }
        }
        default Set<S> toSet() {
            if(isSuccess())
                return StreamSupport.stream(getSuccess().spliterator(), false).collect(Collectors.toSet());
            else {
                return sneakyThrow(getFailure());
            }
        }
        default <R, A> R collect(Collector<? super S, A, R> collector) {
            if(isSuccess())
                return StreamSupport.stream(getSuccess().spliterator(), false).collect(collector);
            else {
                return sneakyThrow(getFailure());
            }
        }

        final class Success<IS extends Iterable<S>, S> extends Try.Success<IS> implements Try1Iterable<IS, S> {
            private Success(IS value) {
                super(value);
            }
        }

        final class Failure<IS extends Iterable<S>, S> extends Try.Failure<IS> implements Try1Iterable<IS, S> {
            private Failure(Throwable cause) {
                super(cause);
            }
        }
    }

    interface Try2Iterable<IS extends Iterable<Tuple2<? extends S1, ? extends S2>>, S1, S2> extends Try1<IS> {
        default Try2Iterable<IS, S1, S2> peekEach(BiConsumer<? super S1, ? super S2> action) {
            Objects.requireNonNull(action, "action is null");
            return andThenTryEach(action::accept);
        }
        default <X extends Throwable> Try2Iterable<IS, S1, S2> andThenEach(ThrowableFunction2_0<X, ? super S1, ? super S2> action) throws X {
            Objects.requireNonNull(action, "action is null");
            return andThenTryEach(action);
        }
        default Try2Iterable<IS, S1, S2> andThenTryEach(ThrowableFunction2_0<? extends Throwable, ? super S1, ? super S2> action) {
            Objects.requireNonNull(action, "action is null");
            if (isSuccess()) {
                try {
                    for (Tuple2<? extends S1, ? extends S2> item : getSuccess()) {
                        action.apply(item.t1(), item.t2());
                    }
                    return this;
                } catch (Throwable e) {
                    return new Try2Iterable.Failure<>(e);
                }
            } else {
                return this;
            }
        }

        default <X extends Throwable, R1, R2> Try2Iterable<? extends Collection<Tuple2<? extends R1, ? extends R2>>, R1, R2> mapEach(ThrowableFunction2_2<X, ? super S1, ? super S2, ? extends R1, ? extends R2> mapper) throws X {
            Objects.requireNonNull(mapper, "mapper is null");
            return tryMapEach(mapper);
        }
        default <R1, R2> Try2Iterable<? extends Collection<Tuple2<? extends R1, ? extends R2>>, R1, R2> tryMapEach(ThrowableFunction2_2<? extends Throwable, ? super S1, ? super S2, ? extends R1, ? extends R2> mapper) {
            Objects.requireNonNull(mapper, "mapper is null");
            return tryMapEach(mapper, new Vector<>());
        }
        default <X extends Throwable, IR extends Collection<Tuple2<? extends R1, ? extends R2>>, R1, R2> Try2Iterable<IR, R1, R2> mapEach(ThrowableFunction2_2<X, ? super S1, ? super S2, ? extends R1, ? extends R2> mapper, IR target) throws X {
            Objects.requireNonNull(mapper, "mapper is null");
            Objects.requireNonNull(target, "target is null");
            return tryMapEach(mapper, target);
        }
        default <IR extends Collection<Tuple2<? extends R1, ? extends R2>>, R1, R2> Try2Iterable<IR, R1, R2> tryMapEach(ThrowableFunction2_2<? extends Throwable, ? super S1, ? super S2, ? extends R1, ? extends R2> mapper, IR target) {
            Objects.requireNonNull(mapper, "mapper is null");
            Objects.requireNonNull(target, "target is null");
            if (isSuccess()) {
                try {
                    for (Tuple2<? extends S1, ? extends S2> item : getSuccess()) {
                        target.add(mapper.apply(item.t1(), item.t2()));
                    }
                    return new Success<>(target);
                } catch (Throwable e) {
                    return new Try2Iterable.Failure<>(e);
                }
            } else {
                return new Try2Iterable.Failure<>(getFailure());
            }
        }

        default <X extends Throwable, R> Try1Iterable<? extends Collection<R>, R> map1Each(ThrowableFunction2_1<X, ? super S1, ? super S2, ? extends R> mapper) throws X {
            Objects.requireNonNull(mapper, "mapper is null");
            return tryMap1Each(mapper);
        }
        default <R> Try1Iterable<? extends Collection<R>, R> tryMap1Each(ThrowableFunction2_1<? extends Throwable, ? super S1, ? super S2, ? extends R> mapper) {
            Objects.requireNonNull(mapper, "mapper is null");
            return tryMap1Each(mapper, new Vector<>());
        }
        default <X extends Throwable, IR extends Collection<R>, R> Try1Iterable<IR, R> map1Each(ThrowableFunction2_1<X, ? super S1, ? super S2, ? extends R> mapper, IR target) throws X {
            Objects.requireNonNull(mapper, "mapper is null");
            Objects.requireNonNull(target, "target is null");
            return tryMap1Each(mapper, target);
        }
        default <IR extends Collection<R>, R> Try1Iterable<IR, R> tryMap1Each(ThrowableFunction2_1<? extends Throwable, ? super S1, ? super S2, ? extends R> mapper, IR target) {
            Objects.requireNonNull(mapper, "mapper is null");
            Objects.requireNonNull(target, "target is null");
            if (isSuccess()) {
                try {
                    for (Tuple2<? extends S1, ? extends S2> item : getSuccess()) {
                        target.add(mapper.apply(item.t1(), item.t2()));
                    }
                    return new Try1Iterable.Success<>(target);
                } catch (Throwable e) {
                    return new Try1Iterable.Failure<>(e);
                }
            } else {
                return new Try1Iterable.Failure<>(getFailure());
            }
        }

        default <X extends Throwable, R1, R2> Try2Iterable<? extends Collection<Tuple2<? extends R1, ? extends R2>>, R1, R2> flatMapEach(ThrowableFunction2_1<X, ? super S1, ? super S2, Try2Iterable<? extends Collection<Tuple2<? extends R1, ? extends R2>>, R1, R2>> mapper) throws X {
            Objects.requireNonNull(mapper, "mapper is null");
            return tryFlatMapEach(mapper);
        }
        default <R1, R2> Try2Iterable<? extends Collection<Tuple2<? extends R1, ? extends R2>>, R1, R2> tryFlatMapEach(ThrowableFunction2_1<? extends Throwable, ? super S1, ? super S2, Try2Iterable<? extends Collection<Tuple2<? extends R1, ? extends R2>>, R1, R2>> mapper) {
            Objects.requireNonNull(mapper, "mapper is null");
            if (isSuccess()) {
                try {
                    Vector<Tuple2<? extends R1, ? extends R2>> target = new Vector<>();
                    for (Tuple2<? extends S1, ? extends S2> item : getSuccess()) {
                        Try2Iterable<? extends Collection<Tuple2<? extends R1, ? extends R2>>, R1, R2> trying = mapper.apply(item.t1(), item.t2());
                        if(trying.isSuccess())
                            target.addAll(trying.getSuccess());
                        else
                            return trying;
                    }
                    return new Try2Iterable.Success<>(target);
                } catch (Throwable e) {
                    return new Try2Iterable.Failure<>(e);
                }
            } else {
                return new Try2Iterable.Failure<>(getFailure());
            }
        }
        default <X extends Throwable, IR extends Collection<Tuple2<? extends R1, ? extends R2>>, R1, R2> Try2Iterable<IR, R1, R2> flatMapEach(ThrowableFunction2_1<X, ? super S1, ? super S2, Try2Iterable<IR, R1, R2>> mapper, IR target) throws X {
            Objects.requireNonNull(mapper, "mapper is null");
            Objects.requireNonNull(target, "target is null");
            return tryFlatMapEach(mapper, target);
        }
        default <IR extends Collection<Tuple2<? extends R1, ? extends R2>>, R1, R2> Try2Iterable<IR, R1, R2> tryFlatMapEach(ThrowableFunction2_1<? extends Throwable, ? super S1, ? super S2, Try2Iterable<IR, R1, R2>> mapper, IR target) {
            Objects.requireNonNull(mapper, "mapper is null");
            Objects.requireNonNull(target, "target is null");
            if (isSuccess()) {
                try {
                    for (Tuple2<? extends S1, ? extends S2> item : getSuccess()) {
                        Try2Iterable<IR, R1, R2> trying = mapper.apply(item.t1(), item.t2());
                        if(trying.isSuccess())
                            target.addAll(trying.getSuccess());
                        else
                            return trying;
                    }
                    return new Try2Iterable.Success<>(target);
                } catch (Throwable e) {
                    return new Try2Iterable.Failure<>(e);
                }
            } else {
                return new Try2Iterable.Failure<>(getFailure());
            }
        }

        default <X extends Throwable> Try2Iterable<? extends Collection<Tuple2<? extends S1, ? extends S2>>, S1, S2> filterEach(ThrowablePredicate2<X, ? super S1, ? super S2> predicate) throws X {
            Objects.requireNonNull(predicate, "predicate is null");
            return tryFilterEach(predicate);
        }
        default Try2Iterable<? extends Collection<Tuple2<? extends S1, ? extends S2>>, S1, S2> tryFilterEach(ThrowablePredicate2<? extends Throwable, ? super S1, ? super S2> predicate) {
            Objects.requireNonNull(predicate, "predicate is null");
            return tryFilterEach(predicate, new Vector<>());
        }
        default <X extends Throwable, IT extends Collection<Tuple2<? extends S1, ? extends S2>>> Try2Iterable<IT, S1, S2> filterEach(ThrowablePredicate2<X, ? super S1, ? super S2> predicate, IT target) throws X {
            Objects.requireNonNull(predicate, "predicate is null");
            Objects.requireNonNull(target, "target is null");
            return tryFilterEach(predicate, target);
        }
        default <IT extends Collection<Tuple2<? extends S1, ? extends S2>>> Try2Iterable<IT, S1, S2> tryFilterEach(ThrowablePredicate2<? extends Throwable, ? super S1, ? super S2> predicate, IT target) {
            Objects.requireNonNull(predicate, "predicate is null");
            Objects.requireNonNull(target, "target is null");
            if (isSuccess()) {
                try {
                    for (Tuple2<? extends S1, ? extends S2> item : getSuccess()) {
                        if (predicate.test(item.t1(), item.t2()))
                            target.add(item);
                    }
                    return new Try2Iterable.Success<>(target);
                } catch (Throwable e) {
                    return new Try2Iterable.Failure<>(e);
                }
            } else {
                return new Try2Iterable.Failure<>(getFailure());
            }
        }

        default <X extends Throwable> void forEach(ThrowableFunction2_0<? extends Throwable, ? super S1, ? super S2> action) throws X {
            Objects.requireNonNull(action, "action is null");
            forEachTry(action);
        }
        default void forEachTry(ThrowableFunction2_0<? extends Throwable, ? super S1, ? super S2> action) {
            Objects.requireNonNull(action, "action is null");
            if (isSuccess()) {
                try {
                    Iterable<Tuple2<? extends S1, ? extends S2>> success = getSuccess();
                    for (Tuple2<? extends S1, ? extends S2> item : success) {
                        action.apply(item.t1(), item.t2());
                    }
                } catch (Throwable e) {
                    sneakyThrow(e);
                }
            } else {
                sneakyThrow(getFailure());
            }
        }

        default <X1 extends Throwable, X2 extends Throwable, R> Try1<R> reduce(R identity, ThrowableFunction2_1<X1, ? super S1, ? super S2, ? extends R> accumulator, ThrowableFunction2_1<X2, ? super R, ? super R, ? extends R> combiner) throws X1, X2 {
            Objects.requireNonNull(identity, "identity is null");
            Objects.requireNonNull(accumulator, "accumulator is null");
            Objects.requireNonNull(combiner, "combiner is null");
            return tryReduce(identity, accumulator, combiner);
        }
        default <R> Try1<R> tryReduce(R identity, ThrowableFunction2_1<? extends Throwable, ? super S1, ? super S2, ? extends R> accumulator, ThrowableFunction2_1<? extends Throwable, ? super R, ? super R, ? extends R> combiner) {
            Objects.requireNonNull(identity, "identity is null");
            Objects.requireNonNull(accumulator, "accumulator is null");
            Objects.requireNonNull(combiner, "combiner is null");
            if (isSuccess()) {
                try {
                    R current = identity;
                    for (Tuple2<? extends S1, ? extends S2> item : getSuccess()) {
                        current = combiner.apply(current, accumulator.apply(item.t1(), item.t2()));
                    }
                    return new Try1.Success<>(current);
                } catch (Throwable e) {
                    return new Try1.Failure<>(e);
                }
            } else {
                return new Try1.Failure<>(getFailure());
            }
        }

        default <X1 extends Throwable, X2 extends Throwable, R1, R2> Try2<R1, R2> reduce(R1 identity1, R2 identity2, ThrowableFunction2_2<X1, ? super S1, ? super S2, ? extends R1, ? extends R2> accumulator, ThrowableFunction2_1<X2, Tuple2<R1, R2>, Tuple2<? extends R1, ? extends R2>, Tuple2<R1, R2>> combiner) throws X1, X2 {
            Objects.requireNonNull(identity1, "identity1 is null");
            Objects.requireNonNull(identity2, "identity2 is null");
            Objects.requireNonNull(accumulator, "accumulator is null");
            Objects.requireNonNull(combiner, "combiner is null");
            return tryReduce(identity1, identity2, accumulator, combiner);
        }
        default <R1, R2> Try2<R1, R2> tryReduce(R1 identity1, R2 identity2, ThrowableFunction2_2<? extends Throwable, ? super S1, ? super S2, ? extends R1, ? extends R2> accumulator, ThrowableFunction2_1<? extends Throwable, Tuple2<R1, R2>, Tuple2<? extends R1, ? extends R2>, Tuple2<R1, R2>> combiner) {
            Objects.requireNonNull(identity1, "identity1 is null");
            Objects.requireNonNull(identity2, "identity2 is null");
            Objects.requireNonNull(accumulator, "accumulator is null");
            Objects.requireNonNull(combiner, "combiner is null");
            if (isSuccess()) {
                try {
                    Tuple2<R1, R2> current = Tuple.of(identity1, identity2);
                    for (Tuple2<? extends S1, ? extends S2> item : getSuccess()) {
                        current = combiner.apply(current, accumulator.apply(item.t1(), item.t2()));
                    }
                    return new Try2.Success<>(current);
                } catch (Throwable e) {
                    return new Try2.Failure<>(e);
                }
            } else {
                return new Try2.Failure<>(getFailure());
            }
        }

        default Map<S1, S2> toMap() {
            if(isSuccess())
                return StreamSupport.stream(getSuccess().spliterator(), false)
                    .collect(Collectors.toMap(Tuple2::t1, Tuple2::t2));
            else
                return sneakyThrow(getFailure());
        }
        default <R, A> Map<S1, R> toMap(Collector<S2, A, R> collector) {
            if(isSuccess())
                return StreamSupport.stream(getSuccess().spliterator(), false)
                    .collect(groupingBy(Tuple2::t1, mapping(Tuple2::t2, collector)));
            else
                return sneakyThrow(getFailure());
        }

        default Map<S1, List<S2>> groupBy() {
            if(isSuccess())
                return StreamSupport.stream(getSuccess().spliterator(), false)
                    .collect(groupingBy(Tuple2::t1, mapping(tuple2 -> (S2)tuple2.t2(), Collectors.toList())));
            else
                return sneakyThrow(getFailure());
        }
        default <K, V> Map<K, List<V>> groupBy(Function<? super S1, ? extends K> keyMapper, Function<? super S2, ? extends V> valueMapper) {
            if(isSuccess())
                return StreamSupport.stream(getSuccess().spliterator(), false)
                    .collect(groupingBy(
                        tuple -> keyMapper.apply(tuple.t1()),
                        mapping(tuple-> (V)valueMapper.apply(tuple.t2()), Collectors.toList())));
            else
                return sneakyThrow(getFailure());
        }
        default <K, V> Map<K, List<V>> groupBy(BiFunction<? super S1, ? super S2, ? extends K> keyMapper, BiFunction<? super S1, ? super S2, ? extends V> valueMapper) {
            if(isSuccess())
                return StreamSupport.stream(getSuccess().spliterator(), false)
                    .collect(groupingBy(
                        tuple -> keyMapper.apply(tuple.t1(), tuple.t2()),
                        mapping(tuple-> (V)valueMapper.apply(tuple.t1(), tuple.t2()), Collectors.toList())));
            else
                return sneakyThrow(getFailure());
        }

        default <R, A> R collect(Collector<Tuple2<? extends S1, ? extends S2>, A, R> collector) {
            if(isSuccess())
                return StreamSupport.stream(getSuccess().spliterator(), false).collect(collector);
            else {
                return sneakyThrow(getFailure());
            }
        }

        final class Success<IS extends Iterable<Tuple2<? extends S1, ? extends S2>>, S1, S2> extends Try.Success<IS> implements Try2Iterable<IS, S1, S2> {
            Success(IS value) {
                super(value);
            }
        }

        final class Failure<IS extends Iterable<Tuple2<? extends S1, ? extends S2>>, S1, S2> extends Try.Failure<IS> implements Try2Iterable<IS, S1, S2> {
            Failure(Throwable cause) {
                super(cause);
            }
        }

    }

    abstract class Success<S> implements Try<S> {
        protected S value;
        protected Success(S value) {
            this.value = value;
        }
        @Override
        public S getSuccess() {
            return value;
        }
        @Override
        public Throwable getFailure() {
            throw new UnsupportedOperationException("No getSuccess method on Failure");
        }
        @Override
        public boolean isSuccess() {
            return true;
        }
        @Override
        public boolean isFailure() {
            return false;
        }
        @Override
        public boolean equals(Object obj) {
            return (obj == this) || (obj instanceof Success && Objects.equals(value, ((Success<?>) obj).value));
        }
        @Override
        public int hashCode() {
            return Objects.hashCode(value);
        }
        @Override
        public String toString() {
            return "Success(" + value + ")";
        }

        @Override
        public Optional<S> toOptional() {
            return Optional.ofNullable(value);
        }
    }

    abstract class Failure<S> implements Try<S> {
        protected Throwable cause;
        protected Failure(Throwable cause) {
            Objects.requireNonNull(cause, "cause is null");
            if (isFatal(cause)) {
                sneakyThrow(cause);
            }
            this.cause = cause;
        }
        @Override
        public S getSuccess() {
            return sneakyThrow(cause);
        }
        @Override
        public Throwable getFailure() {
            return cause;
        }
        @Override
        public boolean isSuccess() {
            return false;
        }
        @Override
        public boolean isFailure() {
            return true;
        }
        @Override
        public boolean equals(Object obj) {
            return (obj == this) || (obj instanceof Failure && Arrays.deepEquals(cause.getStackTrace(), ((Failure<?>) obj).cause.getStackTrace()));
        }
        @Override
        public int hashCode() {
            return Arrays.hashCode(cause.getStackTrace());
        }

        @Override
        public String toString() {
            return "Failure(" + cause + ")";
        }
        @Override
        public Optional<S> toOptional() {
            return Optional.empty();
        }
    }
}

interface TryUtils {
    static boolean isFatal(Throwable throwable) {
        return throwable instanceof InterruptedException
            || throwable instanceof LinkageError
            || throwable instanceof ThreadDeath
            || throwable instanceof VirtualMachineError;
    }

    @SuppressWarnings("unchecked")
    static <T extends Throwable, R> R sneakyThrow(Throwable t) throws T {
        throw (T) t;
    }
}