package com.ekwateur.lib.rop.control;

import com.ekwateur.lib.rop.Tuple;
import com.ekwateur.lib.rop.Tuple2;

import java.io.Serializable;
import java.util.Objects;
import java.util.Optional;
import java.util.function.*;

/**
 * Either represents a value of two possible types. An {@link Either} is either a Failure or a Success.
 *
 * @param <F> The type of the Failure value of an Either.
 * @param <S> The type of the Success value of an Either.
 */
public interface Either<F, S> extends Serializable {

    /**
     * Constructs a {@link Either1.Failure}
     *
     * <pre>{@code
     * // Creates Either instance initiated with failure value "error message"
     * Either<String, ?> either = Either.failure("error message");
     * }</pre>
     *
     * @param value The failure value.
     * @param <F>  Type of failure value.
     * @param <S>  Type of success value.
     * @return A new {@code Either.Failure} instance.
     */
    static <F, S> Either1.Failure<F, S> failure(F value) {
        return new Either1.Failure<>(value);
    }

    /**
     * Constructs a {@link Either1.Success}
     *
     * <pre>{@code
     * // Creates Either instance initiated with success value 1
     * Either<?, Integer> either = Either.success(1);
     * }</pre>
     *
     * @param value The success value.
     * @param <F>   Type of failure value.
     * @param <S>   Type of success value.
     * @return A new {@code Either.Success} instance.
     */
    static <F, S> Either1.Success<F, S> success(S value) {
        return new Either1.Success<>(value);
    }

    /**
     * Equivalent to {@link Either#success(T1)}
     *
     * @param t1 The success value.
     * @param <F>   Type of failure value.
     * @param <T1>   Type of success value.
     * @return A new {@code Either.Success} instance.
     */
    static <F, T1> Either1<F, T1> of(T1 t1) {
        return success(t1);
    }

    /**
     * Constructs a {@link Either2.Success} with a {@link Tuple} of two success values
     *
     * <pre>{@code
     * // Creates Either instance initiated with success values : 1 and "hello"
     * Either.Either2<?, Integer, String> either = Either.success(Tuple.of(1, "hello"));
     * }</pre>
     *
     * @param value The success value.
     * @param <F>   Type of failure value.
     * @param <T1>   Type of first success value.
     * @param <T2>   Type of second success value.
     * @return A new {@code Either.Success} instance.
     */
    static <F, T1, T2> Either2<F, T1, T2> success(Tuple2<T1, T2> value) {
        return new Either2.Success<>(value);
    }

    /**
     * Equivalent to {@link Either#success(Tuple2)}
     *
     * @param value The success value.
     * @param <F>   Type of left value.
     * @param <T1>   Type of first success value.
     * @param <T2>   Type of second success value.
     * @return A new {@code Either.Success} instance.
     */
    static <F, T1, T2> Either2<F, T1, T2> of(Tuple2<T1, T2> value) {
        return success(value);
    }

    /**
     * Constructs a {@link Either2.Success} with two success values
     *
     * <pre>{@code
     * // Creates Either instance initiated with success values : 1 and "hello"
     * Either.Either2<?, Integer, String> either = Either.success(1, "hello");
     * }</pre>
     *
     * @param t1 The first success value.
     * @param t2 The second success value.
     * @param <F>   Type of failure value.
     * @param <T1>   Type of first success value.
     * @param <T2>   Type of second success value.
     * @return A new {@code Either.Success} instance.
     */
    static <F, T1, T2> Either2<F, T1, T2> success(T1 t1, T2 t2) {
        return new Either2.Success<>(Tuple.of(t1, t2));
    }

    /**
     * Equivalent to {@link Either#success(T1, T2)}
     *
     * @param t1 The success value.
     * @param t2 The success value.
     * @param <F>   Type of left value.
     * @param <T1>   Type of the first value.
     * @param <T2>   Type of right value.
     * @return A new {@code Either.Success} instance.
     */
    static <F, T1, T2> Either2<F, T1, T2> of(T1 t1, T2 t2) {
        return success(t1, t2);
    }

    /**
     * Gets the success value if this is a {@code Success} or throws if this is a {@code Failure}.
     *
     * @return the success value
     * @throws UnsupportedOperationException if this is a {@code Failure}.
     */
    S getSuccess();

    /**
     * Returns the failure value.
     *
     * <pre>{@code
     * // prints "error"
     * System.out.println(Either.failure("error").getFailure());
     *
     * // throws UnsupportedOperationException
     * System.out.println(Either.success(42).getFailure());
     * }</pre>
     *
     * @return The failure value.
     * @throws UnsupportedOperationException if this is a {@code Success}.
     */
    F getFailure();

    /**
     * Returns whether this Either is a Success.
     *
     * <pre>{@code
     * // prints "true"
     * System.out.println(Either.success(42).isSuccess());
     *
     * // prints "false"
     * System.out.println(Either.failure("error").isSuccess());
     * }</pre>
     *
     * @return true, if this is a Success, false otherwise
     */
    boolean isSuccess();

    /**
     * Returns whether this Either is a Failure.
     *
     * <pre>{@code
     * // prints "true"
     * System.out.println(Either.failure("error").isFailure());
     *
     * // prints "false"
     * System.out.println(Either.success(42).isFailure());
     * }</pre>
     *
     * @return true, if this is a Failure, false otherwise
     */
    boolean isFailure();

    Optional<S> toOptional();

    default S getOrElseGet(S elseValue) {
        return isSuccess() ? getSuccess() : elseValue;
    }
    default S getOrElseGet(Supplier<? extends S> other) {
        Objects.requireNonNull(other, "other is null");
        return isSuccess() ? getSuccess() : other.get();
    }

    /**
     * Gets the Success value or an alternate value, if the projected Either is a Failure.
     *
     * <pre>{@code
     * // prints "42"
     * System.out.println(Either.success(42).getOrElseGet(l -> -1));
     *
     * // prints "13"
     * System.out.println(Either.failure("error message").getOrElseGet(String::length));
     * }</pre>
     *
     * @param other a function which converts a failure value to an alternative success value
     * @return the success value, if the underlying Either is a Success or else the alternative success value provided by
     * {@code other} by applying the Failure value.
     */
    default S getOrElseGet(Function<? super F, ? extends S> other) {
        Objects.requireNonNull(other, "other is null");
        return isSuccess() ? getSuccess() : other.apply(getFailure());
    }

    /**
     * Runs an action in the case this is a projection on a Failure value.
     *
     * <pre>{@code
     * // prints "no value found"
     * Either.failure("no value found").orElseRun(System.out::println);
     * }</pre>
     *
     * @param action an action which consumes a Failure value
     */
    default void orElseRun(Consumer<? super F> action) {
        Objects.requireNonNull(action, "action is null");
        if (isFailure()) {
            action.accept(getFailure());
        }
    }

    /**
     * Gets the Success value or throws, if the projected Either is a Failure.
     *
     * <pre>{@code
     * Function<String, RuntimeException> exceptionFunction = RuntimeException::new;
     * // prints "42"
     * System.out.println(Either.<String, Integer>success(42).getOrElseThrow(exceptionFunction));
     *
     * // throws RuntimeException("no value found")
     * Either.failure("no value found").getOrElseThrow(exceptionFunction);
     * }</pre>
     *
     * @param <X>         a throwable type
     * @param function    a function which creates an exception based on a Failure value
     * @return the success value, if the underlying Either is a Success or else throws the exception provided by
     * {@code function} by applying the Failure value.
     * @throws X if the projected Either is a Failure
     */
    default <X extends Throwable> S getOrElseThrow(Function<? super F, X> function) throws X {
        Objects.requireNonNull(function, "getOrElseThrow supplier is null");
        if(isSuccess()) {
            return getSuccess();
        } else {
            throw function.apply(getFailure());
        }
    }

    /**
     * Folds either the failure or the success side of this disjunction.
     *
     * <pre>{@code
     * Either.Either1<Exception, Integer> success = Either.success(3);
     * // prints "Users updated: 3"
     * System.out.println(success.fold(Exception::getMessage, count -> "Users updated: " + count));
     *
     * Either.Either1<Exception, Integer> failure = Either.failure(new Exception("Failed to update users"));
     * // prints "Failed to update users"
     * System.out.println(failure.fold(Exception::getMessage, count -> "Users updated: " + count));
     * }</pre>
     *
     * @param onFailure     maps the left value if this is a Left
     * @param onSuccess     maps the right value if this is a Right
     * @param <R>           type of the folded value
     * @return A value of type R
     */
    default <R> R fold(Function<? super F, ? extends R> onFailure, Function<? super S, ? extends R> onSuccess) {
        Objects.requireNonNull(onFailure, "onFailure function is null");
        Objects.requireNonNull(onSuccess, "onSuccess function is null");
        if(isSuccess()) {
            return onSuccess.apply(getSuccess());
        } else {
            return onFailure.apply(getFailure());
        }
    }

    interface Either1<F, S> extends Either<F, S> {

        default Either1<F, S> peek(Consumer<? super S> f) {
            Objects.requireNonNull(f, "consumer function on peek is null");
            if(isSuccess()) {
                f.accept(getSuccess());
                return new Either1.Success<>(getSuccess());
            } else {
                return this;
            }
        }
        default Either1<F, S> peek(Consumer<? super F> onFailure, Consumer<? super S> onSuccess) {
            Objects.requireNonNull(onFailure, "onFailure is null");
            Objects.requireNonNull(onSuccess, "onSuccess is null");
            if (isSuccess()) {
                onSuccess.accept(getSuccess());
            } else {
                onFailure.accept(getFailure());
            }
            return this;
        }
        default Either1<F, S> peekFailure(Consumer<? super F> action) {
            Objects.requireNonNull(action, "action is null");
            if (isFailure()) {
                action.accept(getFailure());
            }
            return this;
        }

        default <R> Either1<F, R> map(Function<? super S, ? extends R> f) {
            Objects.requireNonNull(f, "map function is null");
            if(isSuccess()) {
                R apply = f.apply(getSuccess());
                return new Either1.Success<>(apply);
            } else {
                return (Either1<F, R>) this;
            }
        }

        default <R> Either1<F, R> flatMap(Function<? super S, Either1<F, ? extends R>> f) {
            Objects.requireNonNull(f, "flatMap function is null");
            if(isSuccess()) {
                return (Either1<F, R>)f.apply(getSuccess());
            } else {
                return (Either1<F, R>) this;
            }
        }

        /**
         * Maps either the failure or the success side of this disjunction.
         *
         * <pre>{@code
         * Either.Either1<?, AtomicInteger> success = Either.success(new AtomicInteger(42));
         * // prints "Success(42)"
         * System.out.println(success.bimap(Function1.identity(), AtomicInteger::get));
         *
         * Either.Either1<Exception, ?> failure = Either.failure(new Exception("error"));
         * // prints "Failure(error)"
         * System.out.println(failure.bimap(Exception::getMessage, Function1.identity()));
         * }</pre>
         *
         * @param onFailure  maps the failure value if this is a Failure
         * @param onSuccess maps the success value if this is a Success
         * @param <E>         The new failure type of the resulting Either
         * @param <R>         The new success type of the resulting Either
         * @return A new Either instance
         */
        default <E, R> Either1<E, R> bimap(Function<? super F, ? extends E> onFailure, Function<? super S, ? extends R> onSuccess) {
            Objects.requireNonNull(onFailure, "onFailure function is null");
            Objects.requireNonNull(onSuccess, "onSuccess function is null");
            if(isSuccess()) {
                R apply = onSuccess.apply(getSuccess());
                return new Either1.Success<>(apply);
            } else {
                return new Either1.Failure<>(onFailure.apply(getFailure()));
            }
        }

        default <R1, R2> Either2<F, R1, R2> map2(Function<? super S, Tuple2<? extends R1, ? extends R2>> f) {
            Objects.requireNonNull(f, "map2 function is null");
            if(isSuccess()) {
                return (Either2<F, R1, R2>)new Either2.Success<>(f.apply(getSuccess()));
            } else {
                return (Either2<F, R1, R2>) this;
            }
        }

        default <R1, R2> Either2<F, R1, R2> flatMap2(Function<? super S, Either2<F, ? extends R1, ? extends R2>> f) {
            Objects.requireNonNull(f, "flatMap2 function is null");
            if(isSuccess()) {
                return (Either2<F, R1, R2>)f.apply(getSuccess());
            } else {
                return (Either2<F, R1, R2>) this;
            }
        }

        /**
         * Calls recoveryFunction if the projected Either is a Failure, performs no operation if this is a Success. This is
         * similar to {@code getOrElseGet}, but where the fallback method also returns an Either.
         *
         * <pre>{@code
         * Either<Integer, String> tryGetString() { return Either.failure(1); }
         *
         * Either<Integer, String> tryGetStringAnotherWay(Integer lvalue) { return Either.success("yo " + lvalue); }
         *
         * // = Right("yo 1")
         * tryGetString().recoverWith(this::tryGetStringAnotherWay);
         * }</pre>
         *
         * @param recoveryFunction a function which accepts a Failure value and returns an Either
         * @return an {@code Either<F, S>} instance
         * @throws NullPointerException if the given {@code recoveryFunction} is null
         */
        default Either1<F, S> recoverWith(Function<? super F, Either1<? extends F, ? extends S>> recoveryFunction) {
            Objects.requireNonNull(recoveryFunction, "recoveryFunction is null");
            if (isFailure()) {
                return (Either1<F, S>)recoveryFunction.apply(getFailure());
            } else {
                return this;
            }
        }

        /**
         * Calls recoveryFunction if the projected Either is a Failure, performs no operation if this is a Success. This is
         * similar to {@code getOrElseGet}, but where the fallback method also returns an Either.
         *
         * <pre>{@code
         * Either<Integer, String> tryGetString() { return Either.failure(1); }
         *
         * Either<Integer, String> tryGetStringAnotherWay(Integer lvalue) { return Either.success("yo " + lvalue); }
         *
         * // = Right("yo 1")
         * tryGetString().recover(this::tryGetStringAnotherWay);
         * }</pre>
         *
         * @param recoveryFunction a function which accepts a Failure value and returns an Either
         * @return an {@code Either<F, S>} instance
         * @throws NullPointerException if the given {@code recoveryFunction} is null
         */
        default Either1<F, S> recover(Function<? super F, ? extends S> recoveryFunction) {
            Objects.requireNonNull(recoveryFunction, "recoveryFunction is null");
            if (isFailure()) {
                return new Either1.Success<>(recoveryFunction.apply(getFailure()));
            } else {
                return this;
            }
        }

        default <E> Either1<E, S> mapFailure(Function<? super F, ? extends E> failureMapper) {
            Objects.requireNonNull(failureMapper, "failureMapper is null");
            if (isFailure()) {
                return new Either1.Failure<>(failureMapper.apply(getFailure()));
            } else {
                return (Either1<E, S>) this;
            }
        }

        default Optional<Either1<F, S>> filter(Predicate<? super S> predicate) {
            Objects.requireNonNull(predicate, "predicate is null");
            return isFailure() || predicate.test(getSuccess()) ? Optional.of(this) : Optional.empty();
        }

        default Optional<Either1<F, S>> filterNot(Predicate<? super S> predicate) {
            Objects.requireNonNull(predicate, "predicate is null");
            return filter(predicate.negate());
        }

        /**
         * Filters this right-biased {@code Either} by testing a predicate.
         * If the {@code Either} is a {@code Right} and the predicate doesn't match, the
         * {@code Either} will be turned into a {@code Left} with contents computed by applying
         * the zero function to the {@code Either} value.
         *
         * <pre>{@code
         * // = Left("bad: a")
         * Either.right("a").filterOrElse(i -> false, val -> "bad: " + val);
         *
         * // = Right("a")
         * Either.right("a").filterOrElse(i -> true, val -> "bad: " + val);
         * }</pre>
         *
         * @param predicate A predicate
         * @param zero      A function that turns a right value into a left value if the right value does not make it through the filter.
         * @return an {@code Either} instance
         * @throws NullPointerException if {@code predicate} is null
         */
        default Either1<F, S> filterOrElse(Predicate<? super S> predicate, Function<? super S, ? extends F> zero) {
            Objects.requireNonNull(predicate, "predicate is null");
            Objects.requireNonNull(zero, "zero is null");
            if (isFailure() || predicate.test(getSuccess())) {
                return this;
            } else {
                return Either.failure(zero.apply(getSuccess()));
            }
        }

        default Either1<F, S> filterOrElse(Predicate<? super S> predicate, F reason) {
            Objects.requireNonNull(predicate, "predicate is null");
            Objects.requireNonNull(reason, "reason is null");
            if (isFailure() || predicate.test(getSuccess())) {
                return this;
            } else {
                return Either.failure(reason);
            }
        }

        default Either1<F, S> filter(Predicate<? super S> predicate, Supplier<? extends F> zero) {
            Objects.requireNonNull(predicate, "predicate is null");
            Objects.requireNonNull(zero, "zero is null");
            if (isFailure() || predicate.test(getSuccess())) {
                return this;
            } else {
                return new Either1.Failure<>(zero.get());
            }
        }
        default Either1<F, S> filter(Predicate<? super S> predicate, Function<? super S, ? extends F> zero) {
            Objects.requireNonNull(predicate, "predicate is null");
            Objects.requireNonNull(zero, "zero is null");
            if (isFailure() || predicate.test(getSuccess())) {
                return this;
            } else {
                return new Either1.Failure<>(zero.apply(getSuccess()));
            }
        }

        default Either1<F, S> orElse(Either1<? extends F, ? extends S> other) {
            Objects.requireNonNull(other, "other is null");
            return isSuccess() ? this : (Either1<F, S>)other;
        }

        default Either1<F, S> orElse(Supplier<? extends Either1<F, S>> supplier) {
            Objects.requireNonNull(supplier, "supplier is null");
            return isSuccess() ? this : supplier.get();
        }

        default Try.Try1<S> toTry(Function<? super F, ? extends Throwable> onFailure) {
            Objects.requireNonNull(onFailure, "onFailure is null");
            return mapFailure(onFailure).fold(Try.Try1.Failure::new, Try.Try1.Success::new);
        }

        class Success<F, S> extends Either.Success<F, S> implements Either1<F, S> {
            Success(S value) {
                super(value);
            }
        }

        class Failure<F, S> extends Either.Failure<F, S> implements Either1<F, S> {
            Failure(F value) {
                super(value);
            }
        }
    }

    interface Either2<F, S1, S2> extends Either<F, Tuple2<S1, S2>> {

        default Either2<F, S1, S2> peek(BiConsumer<S1, S2> f) {
            Objects.requireNonNull(f, "consumer function on peek is null");
            if(isSuccess()) {
                Tuple2<S1, S2> success = getSuccess();
                f.accept(success.t1(), success.t2());
            }
            return this;
        }
        default Either2<F, S1, S2> peek(Consumer<? super F> onFailure, BiConsumer<? super S1, ? super S2> onSuccess) {
            Objects.requireNonNull(onFailure, "onFailure is null");
            Objects.requireNonNull(onSuccess, "onSuccess is null");
            if (isSuccess()) {
                Tuple2<S1, S2> success = getSuccess();
                onSuccess.accept(success.t1(), success.t2());
            } else {
                onFailure.accept(getFailure());
            }
            return this;
        }
        default Either2<F, S1, S2> peekFailure(Consumer<? super F> action) {
            Objects.requireNonNull(action, "action is null");
            if (isFailure()) {
                action.accept(getFailure());
            }
            return this;
        }

        default <R1, R2> Either2<F, R1, R2> map(BiFunction<S1, S2, Tuple2<R1, R2>> f) {
            Objects.requireNonNull(f, "map function is null");
            if(isSuccess()) {
                Tuple2<S1, S2> success = getSuccess();
                return new Either2.Success<>(f.apply(success.t1(), success.t2()));
            } else {
                return (Either2<F, R1, R2>) this;
            }
        }
        default <R1, R2> Either2<F, R1, R2> flatMap(BiFunction<S1, S2, Either2<F, R1, R2>> f) {
            Objects.requireNonNull(f, "flatMap function is null");
            if(isSuccess()) {
                Tuple2<S1, S2> success = getSuccess();
                return f.apply(success.t1(), success.t2());
            } else {
                return (Either2<F, R1, R2>) this;
            }
        }

        default <R> Either1<F, R> map1(BiFunction<S1, S2, R> f) {
            Objects.requireNonNull(f, "map1 function is null");
            if(isSuccess()) {
                Tuple2<S1, S2> success = getSuccess();
                return new Either1.Success<>(f.apply(success.t1(), success.t2()));
            } else {
                return (Either1<F, R>) this;
            }
        }

        default <R> Either1<F, R> flatMap1(BiFunction<S1, S2, Either1<F, R>> f) {
            Objects.requireNonNull(f, "flatMap1 function is null");
            if(isSuccess()) {
                Tuple2<S1, S2> success = getSuccess();
                return f.apply(success.t1(), success.t2());
            } else {
                return (Either1<F, R>) this;
            }
        }

        default <E, R1, R2> Either2<? extends E, ? extends R1, ? extends R2> bimap(Function<? super F, ? extends E> onFailure, BiFunction<? super S1, ? super S2, ? extends Tuple2<? extends R1, ? extends R2>> onSuccess) {
            Objects.requireNonNull(onFailure, "onFailure function is null");
            Objects.requireNonNull(onSuccess, "onSuccess function is null");
            if(isSuccess()) {
                Tuple2<S1, S2> success = getSuccess();
                return new Either2.Success<>(onSuccess.apply(success.t1(), success.t2()));
            } else {
                return new Either2.Failure<>(onFailure.apply(getFailure()));
            }
        }

        default Either2<F, S1, S2> recoverWith(Function<? super F, ? extends Either2<F, S1, S2>> recoveryFunction) {
            Objects.requireNonNull(recoveryFunction, "recoveryFunction is null");
            if (isFailure()) {
                return recoveryFunction.apply(getFailure());
            } else {
                return this;
            }
        }
        default Either2<F, S1, S2> recover(Function<? super F, Tuple2<S1, S2>> recoveryFunction) {
            Objects.requireNonNull(recoveryFunction, "recoveryFunction is null");
            if (isFailure()) {
                return new Either2.Success<>(recoveryFunction.apply(getFailure()));
            } else {
                return this;
            }
        }

        default <E> Either2<E, S1, S2> mapFailure(Function<? super F, ? extends E> failureMapper) {
            Objects.requireNonNull(failureMapper, "failureMapper is null");
            if (isFailure()) {
                return new Either2.Failure<>(failureMapper.apply(getFailure()));
            } else {
                return (Either2<E, S1, S2>) this;
            }
        }

        default Optional<Either2<F, S1, S2>> filter(BiPredicate<? super S1, ? super S2> predicate) {
            Objects.requireNonNull(predicate, "predicate is null");
            Tuple2<S1, S2> success = getSuccess();
            return isFailure() || predicate.test(success.t1(), success.t2()) ? Optional.of(this) : Optional.empty();
        }

        default Optional<Either2<F, S1, S2>> filterNot(BiPredicate<? super S1, ? super S2> predicate) {
            Objects.requireNonNull(predicate, "predicate is null");
            return filter(predicate.negate());
        }

        default Either2<F, S1, S2> filterOrElse(BiPredicate<? super S1, ? super S2> predicate, F reason) {
            Objects.requireNonNull(predicate, "predicate is null");
            Objects.requireNonNull(reason, "reason is null");
            Tuple2<S1, S2> success = getSuccess();
            if (isFailure() || predicate.test(success.t1(), success.t2())) {
                return this;
            } else {
                return new Either2.Failure<>(reason);
            }
        }
        default Either2<F, S1, S2> filterOrElse(BiPredicate<? super S1, ? super S2> predicate, BiFunction<? super S1, ? super S2, ? extends F> zero) {
            Objects.requireNonNull(predicate, "predicate is null");
            Objects.requireNonNull(zero, "zero is null");
            Tuple2<S1, S2> success = getSuccess();
            if (isFailure() || predicate.test(success.t1(), success.t2())) {
                return this;
            } else {
                return new Either2.Failure<>(zero.apply(success.t1(), success.t2()));
            }
        }

        default Either2<F, S1, S2> orElse(Either2<F, S1, S2> other) {
            Objects.requireNonNull(other, "other is null");
            return isSuccess() ? this : other;
        }

        default Either2<F, S1, S2> orElse(Supplier<? extends Either2<F, S1, S2>> supplier) {
            Objects.requireNonNull(supplier, "supplier is null");
            return isSuccess() ? this : supplier.get();
        }

        default Try.Try2<S1, S2> toTry(Function<? super F, ? extends Throwable> onFailure) {
            Objects.requireNonNull(onFailure, "onFailure is null");
            return mapFailure(onFailure).fold(Try.Try2.Failure::new, Try.Try2.Success::new);
        }

        final class Success<F, S1, S2> extends Either.Success<F, Tuple2<S1, S2>> implements Either2<F, S1, S2> {
            Success(S1 s1, S2 s2) {
                super(Tuple.of(s1, s2));
            }
            Success(Tuple2<S1, S2> tuple) {
                super(tuple);
            }
        }

        final class Failure<F, S1, S2> extends Either.Failure<F, Tuple2<S1, S2>> implements Either2<F, S1, S2> {
            Failure(F value) {
                super(value);
            }
        }


    }


    abstract class Success<F, S> implements Either<F, S> {
        protected S value;
        protected Success(S value) {
            this.value = value;
        }
        @Override
        public S getSuccess() {
            return value;
        }
        @Override
        public F getFailure() {
            throw new UnsupportedOperationException("No getSuccess method on Failure");
        }
        @Override
        public boolean isSuccess() {
            return true;
        }
        @Override
        public boolean isFailure() {
            return false;
        }
        @Override
        public String toString() {
            return  "Success(" + value + ")";
        }
        @Override
        public boolean equals(Object obj) {
            return (this == obj) || (obj instanceof Either.Success<?,?> success && Objects.equals(value, success.value));
        }
        @Override
        public int hashCode() {
            return Objects.hashCode(value);
        }

        @Override
        public Optional<S> toOptional() {
            return Optional.ofNullable(value);
        }
    }

    abstract class Failure<F, S> implements Either<F, S> {
        protected F value;

        protected Failure(F value) {
            this.value = value;
        }

        @Override
        public S getSuccess() {
            throw new UnsupportedOperationException("No getSuccess method on Failure");
        }

        @Override
        public F getFailure() {
            return value;
        }

        @Override
        public boolean isSuccess() {
            return false;
        }

        @Override
        public boolean isFailure() {
            return true;
        }

        @Override
        public String toString() {
            return "Failure(" + value + ")";
        }
        @Override
        public boolean equals(Object obj) {
            return (this == obj) || (obj instanceof Either.Failure<?, ?> failure && Objects.equals(value, failure.value));
        }
        @Override
        public int hashCode() {
            return Objects.hashCode(value);
        }

        @Override
        public Optional<S> toOptional() {
            return Optional.empty();
        }
    }

}

