package com.ekwateur.lib.rop;

import java.io.Serializable;
import java.util.Objects;

/**
 * Represents a function with no arguments and no returning values.
 *
 * @param <X> the exception which may throw
 */
@FunctionalInterface
public interface ThrowableFunction0_0<X extends Throwable> extends Serializable {

    /**
     * The <a href="https://docs.oracle.com/javase/8/docs/api/index.html">serial version uid</a>.
     */
    long serialVersionUID = 1L;

    /**
     * Applies this function.
     */
    void apply() throws X;

    /**
     * Narrows the given {@code ThrowableFunction0_0<? extends Throwable>} to {@code ThrowableFunction0_0<? extends Throwable>}
     *
     * @param f A {@code ThrowableFunction0_0}
     * @return the given {@code f} instance as narrowed type {@code ThrowableFunction0_0<? extends Throwable>}
     */
    static ThrowableFunction0_0<? extends Throwable> narrow(ThrowableFunction0_0<? extends Throwable> f) {
        return f;
    }

    /**
     * Returns a composed function that first applies this function to
     * its input, and then applies the {@code after} function to the result.
     * If evaluation of either function throws an exception, it is relayed to
     * the caller of the composed function.
     *
     * @param after the function to apply after this function is applied
     * @return a composed function that first applies this function and then
     * applies the {@code after} function
     * @throws NullPointerException if after is null
     */
    default ThrowableFunction0_0<X> andThen(ThrowableFunction0_0<X> after) {
        Objects.requireNonNull(after);
        return () -> {
            apply();
            after.apply();
        };
    }

}
