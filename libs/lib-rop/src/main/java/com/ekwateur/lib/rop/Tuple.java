package com.ekwateur.lib.rop;


import java.util.Map;
import java.util.Objects;

/**
 * The base interface of all tuples.
 */
public interface Tuple {
    /**
     * Creates a {@code Tuple2} from a {@link Map.Entry}.
     *
     * @param <T1>  Type of first component (entry key)
     * @param <T2>  Type of second component (entry value)
     * @param entry A {@link java.util.Map.Entry}
     * @return a new {@code Tuple2} containing key and value of the given {@code entry}
     */
    static <T1, T2> Tuple2<T1, T2> fromEntry(Map.Entry<? extends T1, ? extends T2> entry) {
        Objects.requireNonNull(entry, "entry is null");
        return new Tuple2<>(entry.getKey(), entry.getValue());
    }

    /**
     * Creates a tuple of one element.
     *
     * @param <T1> type of the 1st element
     * @param t1   the 1st element
     * @return a tuple of one element.
     */
    static <T1> Tuple1<T1> of(T1 t1) {
        return new Tuple1<>(t1);
    }

    /**
     * Creates a tuple of two elements.
     *
     * @param <T1> type of the 1st element
     * @param <T2> type of the 2nd element
     * @param t1   the 1st element
     * @param t2   the 2nd element
     * @return a tuple of two elements.
     */
    static <T1, T2> Tuple2<T1, T2> of(T1 t1, T2 t2) {
        return new Tuple2<>(t1, t2);
    }
}

//    /**
//     * Creates a tuple of three elements.
//     *
//     * @param <T1> type of the 1st element
//     * @param <T2> type of the 2nd element
//     * @param <T3> type of the 3rd element
//     * @param t1 the 1st element
//     * @param t2 the 2nd element
//     * @param t3 the 3rd element
//     * @return a tuple of three elements.
//     */
//    static <T1, T2, T3> Tuple3<T1, T2, T3> of(T1 t1, T2 t2, T3 t3) {
//        return new Tuple3<>(t1, t2, t3);
//    }
