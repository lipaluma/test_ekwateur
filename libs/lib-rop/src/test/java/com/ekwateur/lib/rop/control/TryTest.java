/* ____  ______________  ________________________  __________
 * \   \/   /      \   \/   /   __/   /      \   \/   /      \
 *  \______/___/\___\______/___/_____/___/\___\______/___/\___\
 *
 * The MIT License (MIT)
 *
 * Copyright 2023 Vavr, https://vavr.io
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.ekwateur.lib.rop.control;

import com.ekwateur.lib.rop.ThrowablePredicate1;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;
import java.util.function.Supplier;

import static org.assertj.core.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class TryTest {

    private static final String OK = "ok";
    private static final String FAILURE = "failure";

    // -- Try

    // -- andFinally

    @Test
    public void should_execute_onFinally_on_success(){
        final AtomicInteger count = new AtomicInteger();
        Try.run(() -> count.set(0)).andFinally(() -> count.set(1));
        assertThat(count.get()).isEqualTo(1);
    }

    @Test
    public void should_execute_onFinallyTry_on_success(){
        final AtomicInteger count = new AtomicInteger();
        Try.run(() -> count.set(0)).andFinallyTry(() -> count.set(1));
        assertThat(count.get()).isEqualTo(1);
    }

    @Test
    public void should_execute_onFinally_on_failure(){
        final AtomicInteger count = new AtomicInteger();
        Try.run(() -> { throw new IllegalStateException(FAILURE); })
                .andFinallyTry(() -> count.set(1));
        assertThat(count.get()).isEqualTo(1);
    }

    @Test
    public void should_execute_onFinallyTry_on_failure(){
        final AtomicInteger count = new AtomicInteger();
        Try.run(() -> {throw new IllegalStateException(FAILURE);})
                .andFinallyTry(() -> count.set(1));
        assertThat(count.get()).isEqualTo(1);
    }

    @Test
    public void should_execute_andFinallyTry_on_failure_resulting_in_failure_too() {
        Try.Try1<String> result = Try.<String>of(() -> {
                throw new IllegalStateException(FAILURE);
            })
            .andFinallyTry(() -> {
                throw new IllegalStateException(FAILURE);
            });
        assertThat(result.isFailure()).isTrue();
    }

    // -- orElse

    @Test
    public void shouldReturnSelfOnOrElseIfSuccess() {
        final Try.Try1<Integer> success = Try.success(42);
        assertThat(success.orElse(Try.success(0))).isSameAs(success);
    }

    @Test
    public void shouldReturnSelfOnOrElseSupplierIfSuccess() {
        final Try.Try1<Integer> success = Try.success(42);
        assertThat(success.orElse(() -> Try.success(0))).isSameAs(success);
    }

    @Test
    public void shouldReturnAlternativeOnOrElseIfFailure() {
        final Try.Try1<Integer> success = Try.success(42);
        assertThat(Try.failure(new RuntimeException()).orElse(success)).isSameAs(success);
    }

    @Test
    public void shouldReturnAlternativeOnOrElseSupplierIfFailure() {
        final Try.Try1<Integer> success = Try.success(42);
        assertThat(Try.failure(new RuntimeException()).orElse(() -> success)).isSameAs(success);
    }

    // -- Try.to

    @Test
    public void shouldCreateSuccessWhenCallingTryOfCheckedFunction0() {
        assertThat(Try.of(() -> 1) instanceof Try.Success).isTrue();
    }

    @Test
    public void shouldCreateFailureWhenCallingTryOfCheckedFunction0() {
        assertThat(Try.of(() -> {
            throw new Error("error");
        }) instanceof Try.Failure).isTrue();
    }

    // -- Try.fold

    @Test
    public void shouldReturnValueIfSuccess() {
        final Try<Integer> success = Try.success(42);
        assertThat(success.fold(t -> {
            throw new AssertionError("Not expected to be called");
        }, Function.identity())).isEqualTo(42);
    }

    @Test
    public void shouldReturnAlternateValueIfFailure() {
        final Try<Integer> success = Try.failure(new NullPointerException("something was null"));
        assertThat(success.<Integer>fold(t -> 42, a -> {
            throw new AssertionError("Not expected to be called");
        })).isEqualTo(42);
    }

    // -- Try.run

    @Test
    public void shouldCreateSuccessWhenCallingTryRunCheckedRunnable() {
        assertThat(Try.run(() -> {
        }) instanceof Try.Success).isTrue();
    }

    @Test
    public void shouldCreateFailureWhenCallingTryRunCheckedRunnable() {
        assertThat(Try.run(() -> {
            throw new Error("error");
        }) instanceof Try.Failure).isTrue();
    }

    @Test
    public void shouldThrowNullPointerExceptionWhenCallingTryRunCheckedRunnable() {
        assertThatThrownBy(() -> Try.run(null)).isInstanceOf(NullPointerException.class).hasMessage("runnable is null");
    }


    // -- Try.withResources

    @SuppressWarnings("try")/* https://bugs.openjdk.java.net/browse/JDK-8155591 */
    static class Closeable<T> implements AutoCloseable {

        final T value;
        boolean isClosed = false;

        static <T> Closeable<T> of(T value) {
            return new Closeable<>(value);
        }

        Closeable(T value) {
            this.value = value;
        }

        @Override
        public void close() {
            isClosed = true;
        }
    }
    @Test
    public void shouldCreateSuccessTryWithResources1() {
        final Closeable<Integer> closeable1 = Closeable.of(1);
        final Try.Try1<String> actual = Try.ofWithResource(() -> closeable1, i1 -> "" + i1.value);
        assertThat(actual).isEqualTo(Try.success("1"));
        assertThat(closeable1.isClosed).isTrue();
    }

    @Test
    public void shouldCreateFailureTryWithResources1() {
        final Closeable<Integer> closeable1 = Closeable.of(1);
        final Try.Try1<?> actual = Try.ofWithResource(() -> closeable1, i -> { throw new Error(); });
        assertThat(actual.isFailure()).isTrue();
        assertThat(closeable1.isClosed).isTrue();
    }

    @Test
    public void shouldCreateSuccessTryWithResources2() {
        final Closeable<Integer> closeable1 = Closeable.of(1);
        final Closeable<Integer> closeable2 = Closeable.of(2);
        final Try.Try1<String> actual = Try.ofWithResources(() -> closeable1, () -> closeable2, (i1, i2) -> "" + i1.value + i2.value);
        assertThat(actual).isEqualTo(Try.success("12"));
        assertThat(closeable1.isClosed).isTrue();
        assertThat(closeable2.isClosed).isTrue();
    }

    @Test
    public void shouldCreateFailureTryWithResources2() {
        final Closeable<Integer> closeable1 = Closeable.of(1);
        final Closeable<Integer> closeable2 = Closeable.of(2);
        final Try<?> actual = Try.ofWithResources(() -> closeable1, () -> closeable2, (i1, i2) -> { throw new Error(); });
        assertThat(actual.isFailure()).isTrue();
        assertThat(closeable1.isClosed).isTrue();
        assertThat(closeable2.isClosed).isTrue();
    }
    // -- Failure.Cause

    @Test
    public void shouldRethrowInterruptedException() {
        assertThatThrownBy(() -> Try.failure(new InterruptedException())).isInstanceOf(InterruptedException.class);
    }

    @Test
    public void shouldRethrowOutOfMemoryError() {
        assertThatThrownBy(() -> Try.failure(new OutOfMemoryError())).isInstanceOf(OutOfMemoryError.class);
    }

    @Test
    public void shouldDetectNonFatalException() {
        final Exception exception = new Exception();
        assertThat(Try.failure(exception).getFailure()).isSameAs(exception);
    }

    @Test
    public void shouldSubsequentlyHandOverCause() {
        final Supplier<?> inner = () -> {
            throw new UnknownError("\uD83D\uDCA9");
        };
        final Supplier<?> outer = () -> Try.of(inner::get).getSuccess();
        try {
            Try.of(outer::get).getSuccess();
            Assertions.fail("Exception expected");
        } catch (UnknownError x) {
            assertThat(x.getMessage()).isEqualTo("\uD83D\uDCA9");
        } catch (Throwable x) {
            Assertions.fail("Unexpected exception type: " + x.getClass().getName());
        }
    }

    // -- Failure.NonFatal

    @Test
    public void shouldReturnAndNotThrowOnNonFatal() {
        assertThat(Try.failure(new Exception())).isNotNull();
    }

    // -- Failure.Fatal

    @Test
    public void shouldReturnToStringOnFatal() {
        try {
            Try.of(() -> {
                throw new UnknownError("test");
            });
            fail("Exception Expected");
        } catch (UnknownError x) {
            assertThat(x.getMessage()).isEqualTo("test");
        }
    }

    @Test
    public void shouldReturnEqualsOnFatal() {
        UnknownError error = new UnknownError();
        try {
            Try.of(() -> {
                throw error;
            });
            fail("Exception Expected");
        } catch (UnknownError x) {
            try {
                Try.of(() -> {
                    throw error;
                });
                fail("Exception Expected");
            } catch (UnknownError fatal) {
                assertThat(x.equals(fatal)).isEqualTo(true);
            }
        }
    }

    // -- Failure

    @Test
    public void shouldDetectFailureOfRunnable() {
        assertThat(Try.of(() -> {
            throw new RuntimeException();
        }).isFailure()).isTrue();
    }

    @Test
    public void shouldPassThroughFatalException() {
        assertThrows(UnknownError.class, () -> Try.of(() -> {
            throw new UnknownError();
        }));
    }

    // -- isFailure

    @Test
    public void shouldDetectFailureOnNonFatalException() {
        assertThat(Try.failure(new RuntimeException()).isFailure()).isTrue();
    }

    // -- isSuccess

    @Test
    public void shouldDetectNonSuccessOnFailure() {
        assertThat(Try.failure(new RuntimeException()).isSuccess()).isFalse();
    }

    // -- get

    @Test
    public void shouldThrowWhenGetOnFailure() {
        assertThrows(RuntimeException.class, () -> Try.failure(new RuntimeException()).getSuccess());
    }


    // -- getOrElse

    @Test
    public void shouldReturnElseWhenOrElseOnFailure() {
        assertThat(Try.failure(new RuntimeException()).getOrElseGet(OK)).isEqualTo(OK);
    }

    // -- getOrElseGet

    @Test
    public void should_return_else_when_getOrElseGet_on_failure() {
        assertThat(Try.failure(new RuntimeException()).getOrElseGet(OK)).isEqualTo(OK);
    }
    @Test
    public void should_return_else_when_getOrElseGet_by_supplier_on_failure() {
        assertThat(Try.failure(new RuntimeException()).getOrElseGet(()->OK)).isEqualTo(OK);
    }
    @Test
    public void should_return_else_supplied_when_getOrElseGet_on_failure() {
        assertThat(Try.failure(new RuntimeException()).getOrElseGet(x -> OK)).isEqualTo(OK);
    }

    // -- getOrElseThrow

    @Test
    public void shouldThrowOtherWhenGetOrElseThrowOnFailure() {
        assertThrows(IllegalStateException.class, () -> Try.failure(new RuntimeException()).getOrElseThrow(x -> new IllegalStateException(OK)));
    }

    // -- orElseRun

    @Test
    public void shouldRunElseWhenOrElseRunOnFailure() {
        final String[] result = new String[1];
        Try.failure(new RuntimeException()).orElseRun(x -> result[0] = OK);
        assertThat(result[0]).isEqualTo(OK);
    }

    // -- recover(Class, Function)

    @Test
    public void shouldRecoverWhenFailureMatchesExactly() {
        final Try.Try1<String> testee = Try.failure(new RuntimeException());
        assertThat(testee.recover(RuntimeException.class, x -> OK).isSuccess()).isTrue();
    }

    @Test
    public void shouldRecoverWhenFailureIsAssignableFrom() {
        final Try.Try1<String> testee = Try.failure(new UnsupportedOperationException());
        assertThat(testee.recover(RuntimeException.class, x -> OK).isSuccess()).isTrue();
    }

    @Test
    public void shouldReturnThisWhenRecoverDifferentTypeOfFailure() {
        final Try.Try1<String> testee = Try.failure(new RuntimeException());
        assertThat(testee.recover(NullPointerException.class, x -> OK)).isSameAs(testee);
    }

    @Test
    public void shouldReturnThisWhenRecoverSpecificFailureOnSuccess() {
        final Try.Try1<String> testee = Try.of("ok");
        assertThat(testee.recover(RuntimeException.class, x -> OK)).isSameAs(testee);
    }

    // -- recover(Class, Object)

    @Test
    public void shouldRecoverWithSuccessWhenFailureMatchesExactly() {
        final Try.Try1<String> testee = Try.failure(new RuntimeException());
        assertThat(testee.recover(RuntimeException.class, OK).isSuccess()).isTrue();
    }

    @Test
    public void shouldRecoverWithSuccessWhenFailureIsAssignableFrom() {
        final Try.Try1<String> testee = Try.failure(new UnsupportedOperationException());
        assertThat(testee.recover(RuntimeException.class, OK).isSuccess()).isTrue();
    }

    @Test
    public void shouldReturnThisWhenRecoverWithSuccessDifferentTypeOfFailure() {
        final Try.Try1<String> testee = Try.failure(new RuntimeException());
        assertThat(testee.recover(NullPointerException.class, OK)).isSameAs(testee);
    }

    @Test
    public void shouldReturnThisWhenRecoverWithSuccessSpecificFailureOnSuccess() {
        final Try.Try1<String> testee = Try.of("ok");
        assertThat(testee.recover(RuntimeException.class, OK)).isSameAs(testee);
    }

    // -- recover(Function)

    @Test
    public void shouldRecoverOnFailure() {
        assertThat(Try.failure(new RuntimeException()).recover(x -> OK).getSuccess()).isEqualTo(OK);
    }

    @Test
    public void shouldReturnThisWhenRecoverOnSuccess() {
        final Try.Try1<String> testee = Try.of("Ok");
        assertThat(testee.recover(x -> OK)).isSameAs(testee);
    }

    // -- recoverWith(Function)

    @Test
    public void shouldRecoverWithOnFailure() {
        assertThat(Try.failure(new RuntimeException()).recoverWith(x -> Try.of(OK)).getSuccess()).isEqualTo(OK);
    }

    @Test
    public void shouldRecoverWithThrowingOnFailure() {
        final RuntimeException error = new RuntimeException();
        assertThat(Try.failure(new RuntimeException()).recoverWith(x -> {
            throw error;
        })).isEqualTo(Try.failure(error));
    }

    // -- recoverWith(Class, Function)

    @Test
    public void shouldNotTryToRecoverWhenItIsNotNeeded(){
        assertThat(Try.of(() -> OK).recoverWith(RuntimeException.class, x -> Try.<String>failure(new RuntimeException())).getSuccess()).isEqualTo(OK);
    }

    @Test
    public void shouldReturnExceptionWhenRecoveryWasNotSuccess(){
        final Try<?> testee = Try.<Object>of(() -> { throw new RuntimeException("error"); })
            .recoverWith(IOException.class, x -> Try.failure(new RuntimeException()));
        assertThatThrownBy(testee::getSuccess).isInstanceOf(RuntimeException.class).hasMessage("error");
    }

    @Test
    public void shouldReturnErrorOfRecoveryWhenRecoveryFails(){
        final Error error = new Error();
        final Throwable actual = Try.failure(new IOException()).recoverWith(IOException.class, x -> { throw error; }).getFailure();
        assertThat(actual).isSameAs(error);
    }

    @Test
    public void shouldReturnRecoveredValue(){
        assertThat(
            Try.<Object>of(() -> {throw new RuntimeException();})
                .recoverWith(RuntimeException.class, x -> Try.of(()->OK)).getSuccess()
        ).isEqualTo(OK);
    }

    @Test
    public void shouldHandleErrorDuringRecovering(){
        final Try<?> t = Try.<Object>of(() -> {throw new IllegalArgumentException(OK);}).recoverWith(IOException.class, x -> { throw new IllegalStateException(FAILURE);});
        assertThatThrownBy(t::getSuccess).isInstanceOf(IllegalArgumentException.class);
    }

    // -- recoverWith(Class, Try)

    @Test
    public void shouldNotReturnRecoveredValueOnSuccess(){
        assertThat(Try.of(() -> OK).recoverWith(IOException.class, Try.failure(new RuntimeException())).getSuccess()).isEqualTo(OK);
    }

    @Test
    public void shouldReturnRecoveredValueOnFailure(){
        assertThat(Try.<Object>of(() -> {throw new IllegalStateException(FAILURE);}).recoverWith(IllegalStateException.class, Try.success(OK)).getSuccess()).isEqualTo(OK);
    }

    @Test
    public void shouldNotRecoverFailureWhenExceptionTypeIsntAssignable(){
        final Throwable error = new IllegalStateException(FAILURE);
        assertThat(Try.<Object>of(() -> { throw error; }).recoverWith(Error.class, Try.of(OK)).getFailure()).isSameAs(error);
    }

    // -- onFailure

    @Test
    public void shouldConsumeThrowableWhenCallingOnFailureGivenFailure() {
        final String[] result = new String[] { FAILURE };
        Try.failure(new RuntimeException()).onFailure(x -> result[0] = OK);
        assertThat(result[0]).isEqualTo(OK);
    }

    @Test
    public void shouldConsumeThrowableWhenCallingOnFailureWithMatchingExceptionTypeGivenFailure() {
        final String[] result = new String[] { FAILURE };
        Try.failure(new RuntimeException()).onFailure(RuntimeException.class, x -> result[0] = OK);
        assertThat(result[0]).isEqualTo(OK);
    }

    @Test
    public void shouldNotConsumeThrowableWhenCallingOnFailureWithNonMatchingExceptionTypeGivenFailure() {
        final String[] result = new String[] { OK };
        Try.failure(new RuntimeException()).onFailure(Error.class, x -> result[0] = FAILURE);
        assertThat(result[0]).isEqualTo(OK);
    }

    // -- toEither

    @Test
    public void shouldConvertFailureToEither() {
        assertThat(Try.failure(new RuntimeException()).toEither().isFailure()).isTrue();
    }

    @Test
    public void shouldConvertFailureToEitherLeft() {
        assertThat(Try.failure(new RuntimeException()).toEither("test").isFailure()).isTrue();
    }

    @Test
    public void shouldConvertFailureToEitherLeftSupplier() {
        assertThat(Try.failure(new RuntimeException()).toEither(() -> "test").isFailure()).isTrue();
    }

    // -- toCompletableFuture

    @Test
    public void shouldConvertSuccessToCompletableFuture() {
        final CompletableFuture<String> future = Try.of(OK).toCompletableFuture();
        assertThat(future.isDone());
        assertThat(Try.of(() -> future.get()).getSuccess()).isEqualTo(OK);
    }

    @Test
    public void shouldConvertFailureToFailedCompletableFuture() {
        final CompletableFuture<Object> future = Try.failure(new RuntimeException()).toCompletableFuture();
        assertThat(future.isDone());
        assertThat(future.isCompletedExceptionally());
        assertThatThrownBy(future::get)
                .isExactlyInstanceOf(ExecutionException.class)
                .hasCauseExactlyInstanceOf(RuntimeException.class);
    }


    // -- toJavaOptional

    @Test
    public void shouldConvertFailureToOption() {
        assertThat(Try.failure(new RuntimeException()).toOptional().isPresent()).isFalse();
    }

    // -- filter

    private <T> boolean filterThrows(T t) {
        throw new RuntimeException("xxx");
    }

    @Test
    public void shouldFilterMatchingPredicateOnFailure() {
        final Try.Try1<String> actual = Try.failure(new RuntimeException());
        assertThat(actual.filter(s -> true)).isEqualTo(actual);
    }

    @Test
    public void shouldFilterNonMatchingPredicateOnFailure() {
        final Try.Try1<String> actual = Try.failure(new RuntimeException());
        assertThat(actual.filter(s -> false)).isEqualTo(actual);
    }


    @Test
    public void shouldFilterWithExceptionOnFailure() {
        final Try.Try1<String> actual = Try.failure(new RuntimeException());
        assertThat(actual.filter(this::filterThrows)).isEqualTo(actual);
    }

    @Test
    public void shouldReturnIdentityWhenFilterOnFailure() {
        final Try.Try1<String> identity = Try.failure(new RuntimeException());
        assertThat(identity.filter(s -> true)).isEqualTo(identity);
    }

    @Test
    public void shouldReturnIdentityWhenFilterWithErrorProviderOnFailure() {
        final Try.Try1<String> identity = Try.failure(new RuntimeException());
        assertThat(identity.filter(s -> false, ignored -> new IllegalArgumentException())).isEqualTo(identity);
    }

    // -- filterNot

    @Test
    public void shouldFilterNotOnMatchingPredicateOnFailure() {
        final Try.Try1<String> failure = Try.failure(new RuntimeException());
        assertThat(failure.filterNot(s -> false)).isEqualTo(failure);
    }

    @Test
    public void shouldFilterNotOnNonMatchingPredicateOnFailure() {
        final Try.Try1<String> failure = Try.failure(new RuntimeException());
        assertThat(failure.filterNot(s -> true)).isEqualTo(failure);
    }

    @Test
    public void shouldFilterNotWithExceptionOnFailure() {
        final Try.Try1<String> failure = Try.failure(new RuntimeException());
        assertThat(failure.filterNot(this::filterThrows)).isEqualTo(failure);
    }

    @Test
    public void failureShouldThrowWhenFilterNotWithNullPredicate() {
        assertThatThrownBy(() -> Try.failure(new RuntimeException()).filterNot(null))
            .isInstanceOf(NullPointerException.class)
            .hasMessage("predicate is null");
    }
    // -- flatMap

    private <T> Try.Try1<T> flatMapThrows(T t) {
        throw new RuntimeException("xxx");
    }
    @Test
    public void shouldFlatMapOnFailure() {
        final Try.Try1<String> actual = Try.failure(new RuntimeException());
        assertThat(actual.flatMap(s -> Try.of(() -> s + "!"))).isEqualTo(actual);
    }

    @Test
    public void shouldFlatMapWithExceptionOnFailure() {
        final Try.Try1<String> actual = Try.failure(new RuntimeException());
        assertThat(actual.flatMap(this::flatMapThrows)).isEqualTo(actual);
    }

    // -- map

    private <T> T mapThrows(T t) {
        throw new RuntimeException("xxx");
    }

    @Test
    public void shouldMapOnFailure() {
        final Try.Try1<String> actual = Try.failure(new RuntimeException());
        assertThat(actual.map(s -> s + "!")).isEqualTo(actual);
    }

    @Test
    public void shouldMapWithExceptionOnFailure() {
        final Try.Try1<String> actual = Try.failure(new RuntimeException());
        assertThat(actual.map(this::mapThrows)).isEqualTo(actual);
    }

    @Test
    public void shouldChainSuccessWithMap() {
        final Try<Integer> actual = Try.of(() -> 100)
                .map(x -> x + 100)
                .map(x -> x + 50);

        final Try<Integer> expected = Try.success(250);
        assertThat(actual).isEqualTo(expected);
    }

    @Test
    public void shouldChainFailureWithMap() {
        final Try<Integer> actual = Try.of(() -> 100)
                .map(x -> x + 100)
                .map(x -> Integer.parseInt("aaa") + x)   //Throws exception.
                .map(x -> x / 2);
        assertThat(actual.toString()).isEqualTo("Failure(java.lang.NumberFormatException: For input string: \"aaa\")");
    }

    // -- andThen

    @Test
    public void shouldComposeFailureWithAndThenWhenFailing() {
        final Try<Void> actual = Try.run(() -> {
            throw new Error("err1");
        }).andThen(() -> {
            throw new Error("err2");
        });
        assertThat(actual.toString()).isEqualTo("Failure(java.lang.Error: err1)");
    }

    @Test
    public void shouldChainConsumableSuccessWithAndThen() {
        final Try<Integer> actual = Try.of(() -> new ArrayList<Integer>())
                .andThen(arr -> arr.add(10))
                .andThen(arr -> arr.add(30))
                .andThen(arr -> arr.add(20))
                .map(arr -> arr.get(1));

        final Try<Integer> expected = Try.success(30);
        assertThat(actual).isEqualTo(expected);
    }

    @Test
    public void shouldChainConsumableFailureWithAndThen() {
        final Try<Integer> actual = Try.of(() -> new ArrayList<Integer>())
                .andThen(arr -> arr.add(10))
                .andThen(arr -> arr.add(Integer.parseInt("aaa"))) //Throws exception.
                .andThen(arr -> arr.add(20))
                .map(arr -> arr.get(1));
        assertThat(actual.toString()).isEqualTo("Failure(java.lang.NumberFormatException: For input string: \"aaa\")");
    }

    // peek

    @Test
    public void shouldPeekFailure() {
        final List<Object> list = new ArrayList<>();
        assertThat(Try.failure(new RuntimeException()).peek(list::add)).isEqualTo(Try.failure(new RuntimeException()));
        assertThat(list.isEmpty()).isTrue();
    }

    // equals

    @Test
    public void shouldEqualFailureIfObjectIsSame() {
        final Try<?> failure = Try.failure(new RuntimeException("xxx"));
        assertThat(failure).isEqualTo(failure);
    }

    @Test
    public void shouldNotEqualFailureIfObjectIsNull() {
        assertThat(Try.failure(new RuntimeException("xxx"))).isNotNull();
    }

    @Test
    public void shouldNotEqualFailureIfObjectIsOfDifferentType() {
        assertThat(Try.failure(new RuntimeException("xxx")).equals(new Object())).isFalse();
    }

    @Test
    public void shouldEqualFailure() {
        assertThat(Try.failure(new RuntimeException("xxx"))).isEqualTo(Try.failure(new RuntimeException("xxx")));
    }


    // hashCode

    @Test
    public void shouldHashFailure() {
        final Throwable error = new RuntimeException("xxx");
        assertThat(Try.failure(error).hashCode()).isEqualTo(Arrays.hashCode(error.getStackTrace()));
    }

    // toString

    @Test
    public void shouldConvertFailureToString() {
        assertThat(Try.failure(new RuntimeException("error")).toString()).isEqualTo("Failure(java.lang.RuntimeException: error)");
    }

    // -- Success

    @Test
    public void shouldDetectSuccessOfRunnable() {
        //noinspection ResultOfMethodCallIgnored
        assertThat(Try.run(() -> String.valueOf("side-effect")).isSuccess()).isTrue();
    }

    @Test
    public void shouldDetectSuccess() {
        assertThat(Try.success(OK).isSuccess()).isTrue();
    }

    @Test
    public void shouldDetectNonFailureOnSuccess() {
        assertThat(Try.success(OK).isFailure()).isFalse();
    }

    @Test
    public void shouldGetOnSuccess() {
        assertThat(Try.success(OK).getSuccess()).isEqualTo(OK);
    }

    @Test
    public void shouldGetOrElseOnSuccess() {
        assertThat(Try.success(OK).getOrElseGet((String) null)).isEqualTo(OK);
    }

    @Test
    public void shouldOrElseGetOnSuccess() {
        assertThat(Try.success(OK).getOrElseGet(x -> null)).isEqualTo(OK);
    }

    @Test
    public void shouldOrElseRunOnSuccess() {
        final String[] result = new String[] { OK };
        Try.success(OK).orElseRun(x -> result[0] = FAILURE);
        assertThat(result[0]).isEqualTo(OK);
    }

    @Test
    public void shouldOrElseThrowOnSuccess() {
        assertThat(Try.success(OK).getOrElseThrow(x -> null)).isEqualTo(OK);
    }
    @Test
    public void shouldRecoverOnSuccess() {
        assertThat(Try.success(OK).recover(x -> null).getSuccess()).isEqualTo(OK);
    }

    @Test
    public void shouldRecoverWithOnSuccess() {
        assertThat(Try.success(OK).recoverWith(x -> null).getSuccess()).isEqualTo(OK);
    }

    @Test
    public void shouldNotConsumeThrowableWhenCallingOnFailureGivenSuccess() {
        final String[] result = new String[] { OK };
        Try.success(OK).onFailure(x -> result[0] = FAILURE);
        assertThat(result[0]).isEqualTo(OK);
    }

    @Test
    public void shouldConvertSuccessToOption() {
        assertThat(Try.success(OK).toOptional().get()).isEqualTo(OK);
    }

    @Test
    public void shouldConvertSuccessToEither() {
        assertThat(Try.success(OK).toEither().isSuccess()).isTrue();
    }


    // -- filter

    @Test
    public void shouldFilterMatchingPredicateOnSuccess() {
        assertThat(Try.success(OK).filter(s -> true).getSuccess()).isEqualTo(OK);
    }

    @Test
    public void shouldFilterMatchingPredicateWithErrorProviderOnSuccess() {
        assertThat(Try.success(OK).filter(s -> true, s -> new IllegalArgumentException(s)).getSuccess()).isEqualTo(OK);
    }

    @Test
    public void shouldFilterNonMatchingPredicateOnSuccess() {
        final Try<?> testee = Try.success(OK).filter(s -> false);
        assertThatThrownBy(testee::getSuccess).isInstanceOf(NoSuchElementException.class);
    }

    @Test
    public void shouldFilterNonMatchingPredicateAndDefaultThrowableSupplierOnSuccess() {
        assertThat(Try.success(OK).filter(s -> false).getFailure())
                .isInstanceOf(NoSuchElementException.class);
    }

    @Test
    public void shouldFilterNonMatchingPredicateAndCustomThrowableSupplierOnSuccess() {
        assertThat(Try.success(OK).filter(s -> false, () -> new IllegalArgumentException()).getFailure())
                .isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    public void shouldUseErrorProviderWhenFilterNonMatchingPredicateOnSuccess() throws Exception {
        assertThat(Try.success(OK).filter(s -> false, str -> new IllegalArgumentException(str)).getFailure())
                .isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    public void shouldFilterWithExceptionOnSuccess() {
        assertThrows(RuntimeException.class, () -> Try.success(OK).filter(s -> {
            throw new RuntimeException("xxx");
        }).getSuccess());
    }

    // -- filterNot

    @Test
    public void shouldFilterNotOnMatchingPredicateOnSuccess() {
        assertThat(Try.success(OK).filterNot(s -> false).getSuccess()).isEqualTo(OK);
    }

    @Test
    public void shouldFilterNotOnMatchingPredicateWithErrorProviderOnSuccess() {
        assertThat(Try.success(OK).filterNot(s -> false, s -> new IllegalArgumentException(s)).getSuccess()).isEqualTo(OK);
    }

    @Test
    public void shouldFilterNotOnNonMatchingPredicateOnSuccess() {
        final Try<?> success = Try.success(OK).filterNot(s -> true);
        assertThatThrownBy(success::getSuccess).isInstanceOf(NoSuchElementException.class);
    }

    @Test
    public void shouldFilterNotOnNonMatchingPredicateAndDefaultThrowableSupplierOnSuccess() {
        assertThat(Try.success(OK).filterNot(s -> true).getFailure())
                .isInstanceOf(NoSuchElementException.class);
    }

    @Test
    public void shouldFilterNotOnNonMatchingPredicateAndCustomThrowableSupplierOnSuccess() {
        assertThat(Try.success(OK).filterNot(s -> true, () -> new IllegalArgumentException()).getFailure())
                .isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    public void shouldUseErrorProviderWhenFilterNotOnNonMatchingPredicateOnSuccess() {
        assertThat(Try.success(OK).filterNot(s -> true, str -> new IllegalArgumentException(str)).getFailure())
                .isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    public void shouldFilterNotWithExceptionOnSuccess() {
        assertThrows(RuntimeException.class, () ->Try.success(OK).filterNot(s -> {
            throw new RuntimeException("xxx");
        }).getSuccess());
    }

    @Test
    public void successShouldThrowWhenFilterNotWithNullPredicate() {
        assertThatThrownBy(() -> Try.success(OK).filterNot(null))
                .isInstanceOf(NullPointerException.class)
                .hasMessage("predicate is null");
    }

    // -- flatMap

    @Test
    public void shouldFlatMapOnSuccess() {
        assertThat(Try.success(OK).flatMap(s -> Try.of(() -> s + "!")).getSuccess()).isEqualTo(OK + "!");
    }

    @Test
    public void shouldFlatMapOnIterable() {
        final Try.Try1<Integer> success = Try.success(1);
        assertThat(Try.success(OK).flatMap(ignored -> success)).isEqualTo(success);
    }

    @Test
    public void shouldFlatMapOnEmptyIterable() {
        final Try.Try1<Integer> failure = Try.failure(new Error());
        assertThat(Try.success(OK).flatMap(ignored -> failure)).isEqualTo(failure);
    }

    @Test
    public void shouldFlatMapWithExceptionOnSuccess() {
        assertThrows(RuntimeException.class, () -> Try.success(OK).flatMap(s -> {
            throw new RuntimeException("xxx");
        }).getSuccess());
    }

    @Test
    public void shouldMapOnSuccess() {
        assertThat(Try.success(OK).map(s -> s + "!").getSuccess()).isEqualTo(OK + "!");
    }

    @Test
    public void shouldMapWithExceptionOnSuccess() {
        final Try<?> testee = Try.success(OK).map(s -> {
            throw new RuntimeException("xxx");
        });
        assertThatThrownBy(testee::getSuccess).isInstanceOf(RuntimeException.class).hasMessage("xxx");
    }

    @Test
    public void shouldThrowWhenCallingGetCauseOnSuccess() {
        assertThrows(UnsupportedOperationException.class, () -> Try.success(OK).getFailure());
    }

    @Test
    public void shouldComposeSuccessWithAndThenWhenFailing() {
        final Try<Void> actual = Try.run(() -> {
        }).andThen(() -> {
            throw new Error("failure");
        });
        assertThat(actual.toString()).isEqualTo("Failure(java.lang.Error: failure)");
    }

    @Test
    public void shouldComposeSuccessWithAndThenWhenSucceeding() {
        final Try<Void> actual = Try.run(() -> {
        }).andThen(() -> {
        });
        final Try.Try1<Void> expected = Try.success();
        assertThat(actual).isEqualTo(expected);
    }

    // peek

    @Test
    public void shouldPeekSuccess() {
        final List<Object> list = new ArrayList<>();
        assertThat(success().peek(list::add)).isEqualTo(success());
        assertThat(list.isEmpty()).isFalse();
    }

    @Test
    public void shouldPeekSuccessAndThrow() {
        assertThrows(RuntimeException.class, () -> success().peek(t -> failure().getSuccess()));
    }

    // equals

    @Test
    public void shouldEqualSuccessIfObjectIsSame() {
        final Try<?> success = Try.success(1);
        assertThat(success).isEqualTo(success);
    }

    @Test
    public void shouldNotEqualSuccessIfObjectIsNull() {
        assertThat(Try.success(1)).isNotNull();
    }

    @Test
    public void shouldNotEqualSuccessIfObjectIsOfDifferentType() {
        assertThat(Try.success(1).equals(new Object())).isFalse();
    }

    @Test
    public void shouldEqualSuccess() {
        assertThat(Try.success(1)).isEqualTo(Try.success(1));
    }

    // hashCode

    @Test
    public void shouldHashSuccess() {
        assertThat(Try.success(1).hashCode()).isEqualTo(Objects.hashCode(1));
    }

    // toString

    @Test
    public void shouldConvertSuccessToString() {
        assertThat(Try.success(1).toString()).isEqualTo("Success(1)");
    }

    // -- Checked Functions

    @Test
    public void shouldCreateIdentityCheckedFunction() {
        assertThat(Function.identity()).isNotNull();
    }

    @Test
    public void shouldEnsureThatIdentityCheckedFunctionReturnsIdentity() throws Throwable {
        assertThat(Function.identity().apply(1)).isEqualTo(1);
    }

    @Test
    public void shouldNegateCheckedPredicate() {
        final ThrowablePredicate1<? extends Throwable, Integer> greaterThanZero = i -> i > 0;
        final int num = 1;
        try {
            assertThat(greaterThanZero.test(num)).isTrue();
            assertThat(greaterThanZero.negate().test(-num)).isTrue();
        } catch(Throwable x) {
            Assertions.fail("should not throw");
        }
    }

    // -- helpers

    private RuntimeException error() {
        return new RuntimeException("error");
    }

    private static <T> Try<T> failure() {
        return Try.failure(new RuntimeException());
    }

    private static <T, X extends Throwable> Try<T> failure(Class<X> exceptionType) {
        try {
            final X exception = exceptionType.getConstructor().newInstance();
            return Try.failure(exception);
        } catch (Throwable e) {
            throw new IllegalStateException("Error instantiating " + exceptionType, e);
        }
    }

    private <T> boolean filter(T t) {
        throw new RuntimeException("xxx");
    }

    private <T> Try<T> flatMap(T t) {
        throw new RuntimeException("xxx");
    }

    private <T> T map(T t) {
        throw new RuntimeException("xxx");
    }

    private Try.Try1<String> success() {
        return Try.of(() -> "ok");
    }
}
