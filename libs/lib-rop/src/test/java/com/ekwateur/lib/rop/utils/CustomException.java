package com.ekwateur.lib.rop.utils;

public class CustomException extends Exception {
    public CustomException(String message) {
        super(message);
    }
}
