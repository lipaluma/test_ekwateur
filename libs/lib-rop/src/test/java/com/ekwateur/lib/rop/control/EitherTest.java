package com.ekwateur.lib.rop.control;

import org.junit.jupiter.api.Test;

import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertThrows;

class EitherTest {

    @Test
    public void should_fold_failure() {
        final String value = Either.failure("L").fold(l -> l + "+", r -> r + "-");
        assertThat(value).isEqualTo("L+");
    }

    @Test
    public void should_fold_success() {
        final String value = Either.success("R").fold(l -> l + "-", r -> r + "+");
        assertThat(value).isEqualTo("R+");
    }

    @Test
    public void should_peek_action(){
        Either.Either1<Integer, String> actual = Either.failure(1).bimap(i -> i + 1, s -> "Hello " + s);;
        final Either<Integer, String> expected = Either.failure(2);
        assertThat(actual).isEqualTo(expected);
    }
    @Test
    public void should_bimap_failure(){
        Either.Either1<Integer, String> actual = Either.failure(1).bimap(i -> i + 1, s -> "Hello " + s);;
        final Either<Integer, String> expected = Either.failure(2);
        assertThat(actual).isEqualTo(expected);
    }

    @Test
    public void should_bimap_success(){
        Either.Either1<Integer, String> actual = Either.<Integer, String> success("Mario").bimap(i -> i + 1, s -> "Hello " + s);
        final Either<Integer, String> expected = Either.success("Hello Mario");
        assertThat(actual).isEqualTo(expected);
    }


    @Test
    public void should_if_get_success_on_failure() {
        assertThrows(UnsupportedOperationException.class, () -> Either.success(1).getFailure());
    }
    @Test
    public void should_if_get_failure_on_success() {
        assertThrows(UnsupportedOperationException.class, () -> Either.failure(1).getSuccess());
    }

    // -- recover

    @Test
    public void should_recover_with_success_either() {
        assertThat(Either.failure(1).recoverWith(lvalue -> Either.success(lvalue + 1))).isEqualTo(Either.success(2));
    }

    @Test
    public void should_recover_with_failure_either() {
        assertThat(Either.failure(1).recoverWith(lvalue -> Either.failure(lvalue + 1))).isEqualTo(Either.failure(2));
    }

    @Test
    public void should_recover_with_success() {
        final Either<String, String> value = Either.<String, String>success("R").recoverWith(lvalue -> Either.failure("L"));
        assertThat(value).isEqualTo(Either.success("R"));
    }

    @Test
    public void should_recover_failure() {
        assertThat(Either.failure(1).recover(lvalue -> "R")).isEqualTo(Either.success("R"));
    }

    @Test
    public void should_recover_success_without_invoking_recovery_on_success() {
        // Recover function should not be invoked, so hardcode it to fail
        Function<Object, String> recoveryFunction = $ -> {
            throw new RuntimeException("Lazy recovery function should not be invoked for a Right!");
        };

        assertThat(Either.success("R").recover(recoveryFunction)).isEqualTo(Either.success("R"));
    }

    // orElse

    @Test
    public void should_get_success_or_else_value_on_failure() {
        assertThat(Either.success(1).orElse(Either.success(2)).getSuccess()).isEqualTo(1);
        assertThat(Either.failure(1).orElse(Either.success(2)).getSuccess()).isEqualTo(2);
    }

    @Test
    public void should_get_success_or_else_value_by_supplier_on_failure() {
        assertThat(Either.success(1).orElse(() -> Either.success(2)).getSuccess()).isEqualTo(1);
        assertThat(Either.failure(1).orElse(() -> Either.success(2)).getSuccess()).isEqualTo(2);
    }

    // -- Check is failure or is success

    @Test
    public void should_return_true_when_calling_isFailure_on_failure() {
        assertThat(Either.failure(1).isFailure()).isTrue();
    }

    @Test
    public void should_return_false_when_calling_isSuccess_on_failure() {
        assertThat(Either.failure(1).isSuccess()).isFalse();
    }

    @Test
    public void should_return_true_when_calling_isSuccess_on_success() {
        assertThat(Either.success(1).isSuccess()).isTrue();
    }

    @Test
    public void should_return_false_when_calling_isFailure_on_success() {
        assertThat(Either.success(1).isFailure()).isFalse();
    }

    // -- filter

    @Test
    public void should_filter_on_success() {
        Either.Either1<String, Integer> either = Either.success(42);
        assertThat(either.filter(i -> true).get()).isSameAs(either);
        assertThat(either.filter(i -> false)).isSameAs(Optional.empty());
    }

    @Test
    public void should_filter_on_failure() {
        Either.Either1<String, Integer> either = Either.failure("error");
        assertThat(either.filter(i -> true).get()).isSameAs(either);
        assertThat(either.filter(i -> false).get()).isSameAs(either);
    }

    // -- filterNot

    @Test
    public void should_FilterNot_on_success() {
        Either.Either1<String, Integer> either = Either.success(42);
        assertThat(either.filterNot(i -> false).get()).isSameAs(either);
        assertThat(either.filterNot(i -> true)).isSameAs(Optional.empty());
    }

    @Test
    public void should_FilterNot_on_failure() {
        Either.Either1<String, Integer> either = Either.failure("error");
        assertThat(either.filterNot(i -> false).get()).isSameAs(either);
        assertThat(either.filterNot(i -> true).get()).isSameAs(either);
    }

    @Test
    public void should_throw_when_predicate_is_null() {
        assertThatThrownBy(() -> Either.success(42).filterNot(null))
            .isInstanceOf(NullPointerException.class)
            .hasMessage("predicate is null");
    }

    // -- filterOrElse

    @Test
    public void should_filterOrElse_on_success_with_reason_applied_by_function() {
        Either.Either1<String, Integer> either = Either.success(42);
        assertThat(either.filterOrElse(i -> true, Object::toString)).isSameAs(either);
        assertThat(either.filterOrElse(i -> false, Object::toString)).isEqualTo(Either.failure("42"));
    }

    @Test
    public void should_filterOrElse_on_success_with_reason_directly() {
        Either.Either1<String, Integer> either = Either.success(42);
        assertThat(either.filterOrElse(i -> true, "error")).isSameAs(either);
        assertThat(either.filterOrElse(i -> false, "error")).isEqualTo(Either.failure("error"));
    }

    @Test
    public void should_FilterOrElse_on_failure() {
        Either.Either1<String, Integer> either = Either.failure("error");
        assertThat(either.filterOrElse(i -> true, Object::toString)).isSameAs(either);
        assertThat(either.filterOrElse(i -> false, Object::toString)).isSameAs(either);
    }


    // -- flatMap

    @Test
    public void should_FlatMap_on_success() {
        Either.Either1<String, Integer> either = Either.success(42);
        assertThat(either.flatMap(v -> Either.success("ok")).getSuccess()).isEqualTo("ok");
    }

    @Test
    public void should_FlatMap_on_failure() {
        Either.Either1<String, Integer> either = Either.failure("error");
        assertThat(either.flatMap(v -> Either.success("ok"))).isSameAs(either);
    }

    // -- peekLeft

    @Test
    public void should_peek_failure() {
        assertThat(Either.<String, String>failure(null).peekFailure(t -> {})).isEqualTo(Either.<String, String>failure(null));
    }

    @Test
    public void should_peek_failure_on_failure() {
        final int[] effect = { 0 };
        final Either<Integer, ?> actual = Either.failure(1).peekFailure(i -> effect[0] = i);
        assertThat(actual).isEqualTo(Either.failure(1));
        assertThat(effect[0]).isEqualTo(1);
    }

    @Test
    public void should_not_peekFailure_on_success() {
        Either.success(1).peekFailure(i -> { throw new IllegalStateException(); });
    }

    // equals

    @Test
    public void should_Failure_equals_if_object_is_same() {
        final Either<Integer, ?> left = Either.failure(1);
        assertThat(left.equals(left)).isTrue();
    }

    @Test
    public void should_Failure_not_equals_if_compared_to_null() {
        assertThat(Either.failure(1).equals(null)).isFalse();
    }

    @Test
    public void should_Failure_not_equals_if_types_are_different() {
        assertThat(Either.failure(1).equals(new Object())).isFalse();
    }

    @Test
    public void should_failures_be_equals_if_same() {
        assertThat(Either.failure(1)).isEqualTo(Either.failure(1));
    }

    // hashCode

    @Test
    public void should_hash_failure() {
        assertThat(Either.failure(1).hashCode()).isEqualTo(Objects.hashCode(1));
    }

    // toString

    @Test
    public void should_convert_failure_to_string() {
        assertThat(Either.failure(1).toString()).isEqualTo("Failure(1)");
    }

    // equals

    @Test
    public void should_Success_equals_if_object_is_same() {
        final Either<?, ?> success = Either.success(1);
        assertThat(success.equals(success)).isTrue();
    }

    @Test
    public void should_Success_not_equals_if_compared_to_null() {
        assertThat(Either.success(1).equals(null)).isFalse();
    }

    @Test
    public void should_Success_not_equals_if_types_are_different() {
        assertThat(Either.success(1).equals(new Object())).isFalse();
    }

    @Test
    public void should_Success_equals() {
        assertThat(Either.success(1)).isEqualTo(Either.success(1));
    }

    // -- toTry

    @Test
    public void should_convert_Success_to_Try_in_success() {
        final Try<Integer> actual = Either.success(42).toTry(failure -> new IllegalStateException(Objects.toString(failure)));
        assertThat(actual.isSuccess()).isTrue();
        assertThat(actual.getSuccess()).isEqualTo(42);
    }

    @Test
    public void should_convert_Failure_to_Try_in_failure() {
        final Try<?> actual = Either.failure("error").toTry(failure -> new IllegalStateException(failure));
        assertThat(actual.isFailure()).isTrue();
        assertThat(actual.getFailure().getMessage()).isEqualTo("error");
    }

    // hashCode

    @Test
    public void should_hash_success() {
        assertThat(Either.success(1).hashCode()).isEqualTo(Objects.hashCode(1));
    }

    // toString

    @Test
    public void should_convert_success_to_string() {
        assertThat(Either.success(1).toString()).isEqualTo("Success(1)");
    }


}