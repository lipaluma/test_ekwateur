package com.ekwateur.lib.rop.utils;

public class CustomPredicateException extends Exception {
    public CustomPredicateException(String message) {
        super(message);
    }
}
