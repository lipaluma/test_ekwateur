package com.ekwateur.lib.rop.control;

import com.ekwateur.lib.rop.*;
import com.ekwateur.lib.rop.utils.CustomException;
import com.ekwateur.lib.rop.utils.CustomPredicateException;
import org.assertj.core.data.Offset;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class TryAddOnsTest {

    public String throwableAction(String value) throws CustomException {
        if ("nok".equalsIgnoreCase(value) || value.contains("error"))
            throw new CustomException(value);
        else
            return value;
    }
    public String throwableConcat(String value1, String value2) throws CustomException {
        if ("nok".equalsIgnoreCase(value1+value2) || (value1+value2).contains("error"))
            throw new CustomException(value1+" "+value2);
        else
            return value1+" "+value2;
    }
    public String throwableHello(String value) throws CustomException {
        if ("nok".equalsIgnoreCase(value) || value.contains("error"))
            throw new CustomException(value);
        else
            return "Hello " + value;
    }
    public <T> ThrowablePredicate1<? extends CustomPredicateException, T> throwablePredicate(Predicate<T> predicate) {
        ThrowableFunction1_1<CustomPredicateException, T, T> before = value -> {
            if(value == null)
                throw new CustomPredicateException("error");
            return value;
        };
        return before.andThen(predicate::test)::apply;
    }

    // -- on failure

    @Test
    public void should_throw_exception_when_OnFailureThrow_on_failure(){
        Executable call = () -> Try.of(() -> Class.forName("not class"))
            .onFailureThrow();
        assertThrows(ClassNotFoundException.class, call);
    }

    // -- optional

    @Test
    public void should_create_Try_from_optional_not_empty(){
        Try.Try1<Integer> trying = Try.ofOptional(Optional.of(123));
        assertThat(trying.getSuccess()).isEqualTo(123);
    }
    @Test
    public void should_create_Try_from_optional_empty(){
        Try.Try1<Integer> trying = Try.ofOptional(Optional.empty());
        assertThat(trying.getFailure()).isInstanceOf(NoSuchElementException.class);
    }

    // -- finally

    @Test
    public void should_execute_onFinally_after_failing_with_declaring_exceptions() throws CustomException {
        Try.Try1<String> trying = Try.of("test")
            .tryMap(value -> this.throwableAction("error"))
            .andFinally(() -> this.throwableAction("ok"));
        assertThat(trying.getFailure()).isInstanceOf(CustomException.class).hasMessage("error");
    }

    @Test
    public void should_execute_onFinallyTry_after_failing_without_declaring_exceptions() {
        Try.Try1<String> trying = Try.of("test")
            .tryMap(value -> this.throwableAction("error"))
            .andFinallyTry(() -> this.throwableAction("ok"));
        assertThat(trying.getFailure()).isInstanceOf(CustomException.class).hasMessage("error");
    }

    @Test
    public void should_execute_onFinally_and_throwing_new_error() {
        Try.Try1<String> trying = Try.of("test")
            .tryMap(value -> this.throwableAction("error"))
            .andFinallyTry(() -> this.throwableAction("error finally"));
        assertThat(trying.getFailure()).isInstanceOf(CustomException.class).hasMessage("error finally");
    }

    // -- recover

    @Test
    public void should_execute_recover_with_success_after_map_failing_with_declaring_exceptions() throws CustomException {
        Try.Try1<String> trying = Try.of("OK")
            .tryMap(str -> this.throwableAction("error"))
            .recover(ex -> this.throwableAction("ok"));
        assertThat(trying.getSuccess()).isEqualTo("ok");
    }

    @Test
    public void should_execute_tryRecover_with_success_after_map_failing_without_declaring_exceptions() {
        Try.Try1<String> trying = Try.of("OK")
            .tryMap(str -> this.throwableAction("error"))
            .tryRecover(ex -> this.throwableAction("ok"));
        assertThat(trying.getSuccess()).isEqualTo("ok");
    }

    @Test
    public void should_execute_recover_with_failure_after_map_failing_with_declaring_exceptions() throws CustomException {
        Try.Try1<String> trying = Try.of("OK")
            .tryMap(str -> this.throwableAction("error"))
            .recover(ex -> this.throwableAction("error recover"));
        assertThat(trying.getFailure()).isInstanceOf(CustomException.class).hasMessage("error recover");
    }

    @Test
    public void should_execute_tryRecover_with_failure_after_map_failing_with_declaring_exceptions() {
        Try.Try1<String> trying = Try.of("OK")
            .tryMap(str -> this.throwableAction("error"))
            .tryRecover(ex -> this.throwableAction("error recover"));
        assertThat(trying.getFailure()).isInstanceOf(CustomException.class).hasMessage("error recover");
    }

    @Test
    public void should_execute_recoverWith_with_failure_after_map_failing_with_declaring_exceptions() throws CustomException {
        Try.Try1<String> trying = Try.of("OK")
            .tryMap(str -> this.throwableAction("error"))
            .<CustomException>recoverWith(ex -> Try.ofThrowing(() -> this.throwableAction("error recover")));
        assertThat(trying.getFailure()).isInstanceOf(CustomException.class).hasMessage("error recover");
    }

    @Test
    public void should_execute_tryRecoverWith_with_failure_after_map_failing_without_declaring_exceptions() {
        Try.Try1<String> trying = Try.of("OK")
            .tryMap(str -> this.throwableAction("error"))
            .tryRecoverWith(ex -> Try.to(() -> this.throwableAction("error recover")));
        assertThat(trying.getFailure()).isInstanceOf(CustomException.class).hasMessage("error recover");
    }

    // -- resources

    @Test
    public void should_use_autocloseable_resource_with_throwable_functions_declaring_exceptions() throws IOException {
        Try.Try1<String> trying = Try.ofWithResourceThrowing(() -> new FileInputStream("not_exist.txt"), FileInputStream::readAllBytes)
            .map(String::new);
        assertThat(trying.getFailure()).isInstanceOf(FileNotFoundException.class);
    }
    @Test
    public void should_use_autocloseable_resource_with_throwable_functions_without_declaring_exceptions() {
        Try.Try1<String> trying = Try.ofWithResource(() -> new FileInputStream("not_exist.txt"), FileInputStream::readAllBytes)
            .map(String::new);
        assertThat(trying.getFailure()).isInstanceOf(FileNotFoundException.class);
    }

    @Test
    public void should_use_autocloseable_of_two_resources_with_throwable_functions_declaring_exceptions() throws IOException {
        Try.Try1<String> trying = Try.ofWithResourcesThrowing(
            () -> new FileInputStream("not_exist.txt"),
            () -> new TryTest.Closeable<>(1),
            (fis, integer) -> fis.readAllBytes())
            .map(String::new);
        assertThat(trying.getFailure()).isInstanceOf(FileNotFoundException.class);
    }
    @Test
    public void should_use_autocloseable_of_two_resources_with_throwable_functions_without_declaring_exceptions() {
        Try.Try1<String> trying = Try.ofWithResources(
                () -> new FileInputStream("not_exist.txt"),
                () -> new TryTest.Closeable<>(1),
                (fis, integer) -> fis.readAllBytes())
            .map(String::new);
        assertThat(trying.getFailure()).isInstanceOf(FileNotFoundException.class);
    }

    // -- mapping 1-1

    @Test
    public void should_exceptions_have_to_be_declared_with_map() throws ClassNotFoundException {
        Try.Try1<? extends Class<?>> trying = Try.of("java.lang.String")
            .map(Class::forName);
        assertThat(trying.getSuccess()).isEqualTo(String.class);
    }
    @Test
    public void should_exceptions_dont_need_to_be_declared_with_trymap() {
        Try.Try1<? extends Class<?>> trying = Try.of("java.lang.String")
            .tryMap(Class::forName);
        assertThat(trying.getSuccess()).isEqualTo(String.class);
    }
    @Test
    public void should_exceptions_have_to_be_declared_with_map_on_failure() throws ClassNotFoundException {
        Try.Try1<? extends Class<?>> trying = Try.of("NotExistingClass")
            .map(Class::forName);
        assertThat(trying.getFailure()).isInstanceOf(ClassNotFoundException.class).hasMessage("NotExistingClass");
    }
    @Test
    public void should_exceptions_dont_need_to_be_declared_with_trymap_on_failure() {
        Try.Try1<? extends Class<?>> trying = Try.of("NotExistingClass")
            .tryMap(Class::forName);
        assertThat(trying.getFailure()).isInstanceOf(ClassNotFoundException.class).hasMessage("NotExistingClass");
    }

    @Test
    public void should_declare_exceptions_with_throwing() throws ClassNotFoundException {
        Try.Try1<? extends Class<?>> trying = Try.of("NotExistingClass")
            .tryMap(Class::forName).throwing(ClassNotFoundException.class);
        assertThat(trying.getFailure()).isInstanceOf(ClassNotFoundException.class).hasMessage("NotExistingClass");
    }

    // -- mapping 1-2 and 2-1
    @Test
    public void should_map_2_values_from_1() {
        Try.Try2<String, String> trying = Try.of("Luke Skywalker")
            .map(fullName -> fullName.split(" "))
            .map2(array -> Tuple.of(array[0], array[1]));
        assertThat(trying.getSuccess()).isEqualTo(Tuple.of("Luke","Skywalker"));
    }

    @Test
    public void should_map_1_values_from_2() {
        Try.Try1<String> trying = Try.of("Luke", "Skywalker")
            .map1((first, last) -> first+" "+last);
        assertThat(trying.getSuccess()).isEqualTo("Luke Skywalker");
    }

    @Test
    public void should_map_new_value_and_keep_old() {
        Try.Try1<Double> trying = Try.of(List.of(15, 12, 9, 11, 6, 18))
            .add(notes -> notes.stream().reduce(0, Integer::sum))
            .map((notes, sum) -> Tuple.of(sum, notes.size()))
            .map1((sum, count) -> sum / (double) count);
        assertThat(trying.getSuccess()).isEqualTo(11.83, Offset.offset(0.01));
    }

    @Test
    public void should_map_new_value_only_when_predicate_is_successful() {
        Try.Try1<String> trying = Try.of("86615")
            .addWhen(Objects::nonNull, String::length)
            .map1((ref, optionSize) -> optionSize.map(size -> "reference " + ref + " has size " + size).orElse("reference is empty"));
        assertThat(trying.getSuccess()).isEqualTo("reference 86615 has size 5");
    }
    @Test
    public void should_not_map_new_value_when_predicate_is_not_successful() {
        Try.Try1<String> trying = Try.success((String)null)
            .addWhen(Objects::nonNull, String::length)
            .map1((ref, optionSize) -> optionSize.map(size -> "reference " + ref + " has size " + size).orElse("reference is empty"));
        assertThat(trying.getSuccess()).isEqualTo("reference is empty");
    }

    // -- map Failures
    @Test
    public void should_map_to_a_new_failure() {
        Try.Try1<? extends Class<?>> trying = Try.of(() -> Class.forName("NotExist"))
            .mapFailure(ex -> new RuntimeException("Wrap error", ex));
        assertThatThrownBy(trying::getSuccess).isInstanceOf(RuntimeException.class)
            .hasMessage("Wrap error").hasCauseInstanceOf(ClassNotFoundException.class);
    }

    @Test
    public void should_map_failure_because_exception_type_matches() {
        Try.Try1<? extends Class<?>> trying = Try.of(() -> Class.forName("NotExist"))
            .mapFailure(ClassNotFoundException.class, ex -> new RuntimeException("Wrap error", ex));
        assertThatThrownBy(trying::getSuccess).isInstanceOf(RuntimeException.class)
            .hasMessage("Wrap error").hasCauseInstanceOf(ClassNotFoundException.class);
    }
    @Test
    public void should_not_map_failure_because_exception_type_not_match() {
        Try.Try1<? extends Class<?>> trying = Try.of(() -> Class.forName("NotExist"))
            .mapFailure(ArrayIndexOutOfBoundsException.class, ex -> new RuntimeException("Wrap error", ex));
        assertThatThrownBy(trying::getSuccess).isInstanceOf(ClassNotFoundException.class);
    }

    @Test
    public void should_map_failure_because_exception_type_matches_with_one_on_the_list() {
        Try.Try1<? extends Class<?>> trying = Try.of(() -> Class.forName("NotExist"))
            .mapFailures(List.of(ArrayIndexOutOfBoundsException.class, ClassNotFoundException.class), ex -> new RuntimeException("Wrap error", ex));
        assertThatThrownBy(trying::getSuccess).isInstanceOf(RuntimeException.class)
            .hasMessage("Wrap error").hasCauseInstanceOf(ClassNotFoundException.class);
    }
    @Test
    public void should_not_map_failure_because_exception_type_not_match_anyone_on_the_list() {
        Try.Try1<? extends Class<?>> trying = Try.of(() -> Class.forName("NotExist"))
            .mapFailures(List.of(NoSuchMethodException.class, NoSuchElementException.class), ex -> new RuntimeException("Wrap error", ex));
        assertThatThrownBy(trying::getSuccess).isInstanceOf(ClassNotFoundException.class);
    }

    // -- TryIterable peek on each element

    @Test
    public void should_peekEach_on_success() {
        List<String> myList = List.of("v1", "v2");
        List<String> result = Try.iterateOn(() -> myList).peekEach(System.out::println).toList();
        assertThat(result).isEqualTo(myList);
    }

    // -- TryIterable andThen on each element

    @Test
    public void should_andThenEach_on_success() throws CustomException {
        final List<String> firstnames = List.of("Luke", "Leïa", "Anakin");
        final String name = "Skywalker";
        List<String> result = Try.iterateOn(() -> firstnames).andThenEach(this::throwableAction).toList();
        assertThat(result).isEqualTo(firstnames);
    }
    @Test
    public void should_andThenTryEach_on_success() {
        final List<String> firstnames = List.of("Luke", "Leïa", "Anakin");
        List<String> result = Try.iterateOn(() -> firstnames).andThenTryEach(this::throwableAction).toList();
        assertThat(result).isEqualTo(firstnames);
    }

    // -- TryIterable for on each element

    @Test
    public void should_forEach_on_success_with_throwable_function() throws CustomException {
        final List<String> actual = new ArrayList<>();
        List<String> myList = List.of("v1", "v2");
        Try.iterateOn(() -> myList).forEach(str -> actual.add(throwableAction(str)));
        assertThat(actual).isEqualTo(myList);
    }
    @Test
    public void should_forEachTry_on_success_with_throwable_function() {
        final List<String> actual = new ArrayList<>();
        List<String> myList = List.of("v1", "v2");
        Try.iterateOn(() -> myList).forEachTry(str -> actual.add(throwableAction(str)));
        assertThat(actual).isEqualTo(myList);
    }

    // -- TryIterable map on each element

    @Test
    public void should_mapEach_on_success() {
        final List<Integer> initial = List.of(10, 20);
        List<Integer> result = Try.iterateOn(() -> initial).mapEach(i -> i + 5).toList();
        assertThat(result).isEqualTo(List.of(15, 25));
    }
    @Test
    public void should_mapEach_on_success_with_throwable_function() throws CustomException {
        final List<String> initial = List.of("Luke", "Han");
        List<String> result = Try.iterateOn(() -> initial).mapEach(this::throwableHello).toList();
        assertThat(result).isEqualTo(List.of("Hello Luke", "Hello Han"));
    }
    @Test
    public void should_mapEach_by_setting_values_in_a_set_with_throwable_function() throws CustomException {
        final List<String> initial = List.of("Luke", "Han", "Luke", "Han");
        List<String> result = Try.iterateOn(() -> initial).mapEach(this::throwableHello, new HashSet<>()).toList();
        assertThat(result).isEqualTo(List.of("Hello Luke", "Hello Han"));
    }
    @Test
    public void should_tryMapEach_on_success_with_throwable_function() {
        final List<String> initial = List.of("Luke", "Han");
        List<String> result = Try.iterateOn(() -> initial).tryMapEach(this::throwableHello).toList();
        assertThat(result).isEqualTo(List.of("Hello Luke", "Hello Han"));
    }
    @Test
    public void should_tryMapEach_by_setting_values_in_a_set_with_throwable_function() {
        final List<String> initial = List.of("Luke", "Han", "Luke", "Han");
        List<String> result = Try.iterateOn(() -> initial).tryMapEach(this::throwableHello, new HashSet<>()).toList();
        assertThat(result).isEqualTo(List.of("Hello Luke", "Hello Han"));
    }
    @Test
    public void should_mapEachWhen() throws CustomException, CustomPredicateException {
        record Jedi(String name, boolean sith){}
        final List<Jedi> jedis = List.of(
            new Jedi("Luke", false),
            new Jedi("Anakin", true),
            new Jedi("Obi-wan", false),
            new Jedi("Yoda", false),
            new Jedi("Palpatine", true)
        );
        Map<String, List<String>> result = Try.iterateOn(() -> jedis)
            .mapEachWhen(throwablePredicate(Jedi::sith), jedi -> Tuple.of("JEDI", jedi.name()), jedi -> Tuple.of("SITH", throwableAction(jedi.name())))
            .map2Each(tuple -> tuple)
            .groupBy();
        assertThat(result).isEqualTo(Map.of("SITH", List.of("Anakin", "Palpatine"), "JEDI", List.of("Luke", "Obi-wan", "Yoda")));
    }
    @Test
    public void should_tryMapEachWhen_using_other_collection() {
        record Jedi(String name, boolean sith){}
        final List<Jedi> jedis = List.of(
            new Jedi("Luke", false),
            new Jedi("Anakin", true),
            new Jedi("Anakin", true),
            new Jedi("Obi-wan", false),
            new Jedi("Yoda", false),
            new Jedi("Yoda", false),
            new Jedi("Palpatine", true)
        );
        Map<String, List<String>> result = Try.iterateOn(() -> jedis)
            .tryMapEachWhen(throwablePredicate(Jedi::sith), jedi -> Tuple.of("JEDI", jedi.name()), jedi -> Tuple.of("SITH", throwableAction(jedi.name())), new HashSet<>())
            .map2Each(tuple -> tuple)
            .groupBy();
        assertThat(result).isEqualTo(Map.of("SITH", List.of("Anakin", "Palpatine"), "JEDI", List.of("Luke", "Obi-wan", "Yoda")));
    }

    // -- TryIterable map 1-2 or 2-1 on each element

    @Test
    public void should_map2_each_element_on_success_and_group_by_name() {
        final List<String> firstnames = List.of("Luke", "Obi-wan", "Han");
        final String name = "Skywalker";
        Map<String, List<String>> map = Try.iterateOn(() -> firstnames)
            .map2Each(first -> Tuple.of(name, first))
            .groupBy();
        assertThat(map).isEqualTo(Map.of(name, firstnames));
    }
    @Test
    public void should_map2_each_element_on_success_and_group_by_name_by_reverting_key_and_value() {
        final List<String> firstnames = List.of("Luke", "Obi-wan", "Han");
        final String name = "Skywalker";
        Map<String, List<String>> map = Try.iterateOn(() -> firstnames)
            .map2Each(first -> Tuple.of(first, name))
            .groupBy((key, value) -> value, (key, value) -> key);
        assertThat(map).isEqualTo(Map.of(name, firstnames));
    }
    @Test
    public void should_map2_each_element_with_throwable_function_on_success() throws CustomException {
        final List<String> firstnames = List.of("Luke", "Obi-wan", "Han");
        final String name = "Skywalker";
        Map<String, List<String>> map = Try.iterateOn(() -> firstnames).map2Each(first -> Tuple.of(first, this.throwableAction(name)))
            .groupBy((key, value) -> value, (key, value) -> key);
        assertThat(map).isEqualTo(Map.of(name, firstnames));
    }

    @Test
    public void should_addOnEach_element_with_throwable_function_on_success() throws CustomException {
        final List<String> firstnames = List.of("Luke", "Obi-wan", "Han");
        final String name = "Skywalker";
        Map<String, List<String>> map = Try.iterateOn(() -> firstnames).addOnEach(first -> this.throwableAction(name))
            .groupBy((key, value) -> value, (key, value) -> key);
        assertThat(map).isEqualTo(Map.of(name, firstnames));
    }
    @Test
    public void should_tryAddOnEach_element_with_throwable_function_on_success() {
        final List<String> firstnames = List.of("Luke", "Obi-wan", "Han");
        final String name = "Skywalker";
        Map<String, List<String>> map = Try.iterateOn(() -> firstnames).tryAddOnEach(first -> this.throwableAction(name))
            .groupBy((key, value) -> value, (key, value) -> key);
        assertThat(map).isEqualTo(Map.of(name, firstnames));
    }
    @Test
    public void should_tryAddOnEach_element_with_throwable_function_on_success_with_other_collection() throws CustomException {
        final List<String> firstnames = List.of("Luke", "Obi-wan", "Han", "Luke", "Han");
        final String name = "Skywalker";
        Map<String, List<String>> map = Try.iterateOn(() -> firstnames).addOnEach(first -> this.throwableAction(name), new HashSet<>())
            .groupBy((key, value) -> value, (key, value) -> key);
        assertThat(map).isEqualTo(Map.of(name, List.of("Obi-wan", "Luke", "Han")));
    }
    @Test
    public void should_addOnEachWhen_with_throwable_function_on_success_with_other_collection() throws CustomException {
        final List<String> firstnames = List.of("Luke", "Obi-wan", "Han");
        final Map<String, String> mapNames = Map.of("Luke", "Skywalker");
        Map<? extends String, List<String>> map = Try.iterateOn(() -> firstnames)
            .addOnEachWhen(mapNames::containsKey, first -> throwableAction(mapNames.get(first)))
            .filterEach((first, name)-> name.isPresent())
            .mapEach((first, name) -> Tuple.of(name.get(), first))
            .groupBy();
        assertThat(map).isEqualTo(Map.of("Skywalker", List.of("Luke")));
    }




    // -- TryIterable zip

    @Test
    public void should_zip_and_map_each_other_on_success() {
        final List<String> firstnames = List.of("Luke", "Obi-wan", "Han");
        final List<String> names = List.of("Skywalker", "Kenobi", "Solo");
        List<String> result = Try.iterateOn(() -> firstnames).zip(names).map1Each((first, last) -> first+" "+last).toList();
        assertThat(result).isEqualTo(List.of("Luke Skywalker", "Obi-wan Kenobi", "Han Solo"));
    }
    @Test
    public void should_zip_and_map_each_other_on_failure() throws Exception {
        final List<String> firstnames = List.of("Luke", "Obi-wan", "Han");
        final List<String> names = List.of("Skywalker", "Kenobi", "Solo");
        Try.Try1Iterable<? extends Collection<String>, String> trying = Try.iterateOn(() -> firstnames).zip(names).map1Each((first, last) -> {
            if ("Solo".equals(last))
                throw new Exception(first + " " + last + " is not a jedi");
            return first + " " + last;
        });
        assertThatThrownBy(trying::toList).isInstanceOf(Exception.class).hasMessage("Han Solo is not a jedi");
    }
    @Test
    public void should_zip_and_map_each_other_on_success_passing_by_iterate_method() {
        List<String> result = Try.of(List.of("Luke", "Obi-wan"))
            .iterate(new ArrayList<>())
            .zip(List.of("Skywalker", "Kenobi"))
            .map1Each((first, last) -> first + " " + last)
            .toList();
        assertThat(result).isEqualTo(List.of("Luke Skywalker", "Obi-wan Kenobi"));
    }
    @Test
    public void should_zip_and_map_each_other_on_success_passing_by_iterate_method_through_hashset() {
        List<String> result = Try.of(List.of("Luke", "Luke", "Obi-wan", "Luke", "Obi-wan"))
            .iterate(new HashSet<>())
            .zip(List.of("Skywalker", "Kenobi"))
            .map1Each((first, last) -> first + " " + last)
            .toList();
        assertThat(result).isEqualTo(List.of("Luke Skywalker", "Obi-wan Kenobi"));
    }
    @Test
    public void should_zip_on_failure_if_iterable_sizes_are_not_equals() {
        Try.Try1Iterable<? extends Collection<String>, String> trying = Try.of(List.of("Luke", "Obi-wan", "Jabba"))
            .iterate(new ArrayList<>())
            .zip(List.of("Skywalker", "Kenobi"))
            .map1Each((first, last) -> first + " " + last);
        assertThatThrownBy(trying::toList).isInstanceOf(IndexOutOfBoundsException.class);
    }
    @Test
    public void should_raise_exception_if_iterate_on_non_iterable_success() {
        assertThatThrownBy(() -> Try.of("Luke")
            .iterate(new ArrayList<String>())
            .zip(List.of("Skywalker", "Kenobi"))
            .map1Each((first, last) -> first + " " + last)
            .toList()).isInstanceOf(UnsupportedOperationException.class).hasMessage("Cannot iterate on class java.lang.String");
    }

    @Test
    public void should_zip_with_index_on_success() {
        final List<String> firstnames = List.of("Luke", "Obi-wan", "Han");
        final List<String> names = List.of("Skywalker", "Kenobi", "Solo");
        List<String> result = Try.iterateOn(() -> firstnames)
            .zipWithIndex()
            .map1Each((index, firstname) -> firstname+" "+names.get(index))
            .toList();
        assertThat(result).isEqualTo(List.of("Luke Skywalker", "Obi-wan Kenobi", "Han Solo"));
    }

    // -- TryIterable reduce

    @Test
    public void should_reduce_on_sum_success() {
        final List<Integer> evaluations = List.of(12, 9, 15, 16, 13);
        Optional<Integer> result = Try.iterateOn(() -> evaluations)
            .reduce(Integer::sum)
            .getSuccess();
        assertThat(result).hasValue(65);
    }

    @Test
    public void should_reduce_to_another_type() throws CustomException {
        List<String> list = List.of("Bonjour", "Hello", "Bom dia", "Guten tag");
        Integer result = Try.iterateOn(() -> list)
            .reduce(0, (acc, str) -> acc + throwableAction(str).length())
            .getSuccess();
        assertThat(result).isEqualTo(28);
    }


    @Test
    public void should_use_Try1_methods_from_try1Iterable_calcul_average_success() throws CustomException {
        final List<Integer> evaluations = List.of(12, 9, 15, 16, 13);
        Integer result = Try.iterateOn(() -> evaluations)
            .add(List::size)
            .map((list, size) -> Tuple.of(Try.iterateOn(()->list).reduce(Integer::sum).getSuccess(), size))
            .map1((total, size) -> total.map(value -> value/size).orElseThrow(()->new CustomException("error")))
            .getSuccess();
        assertThat(result).isEqualTo(13);
    }

    @Test
    public void should_use_Try1_methods_from_try1Iterable_calcul_average2_success() throws CustomException {
        final List<Integer> evaluations = List.of(12, 9, 15, 16, 13);
        Integer result = Try.iterateOn(() -> evaluations)
            .flatMap2(list -> Try.iterateOn(()->list).reduce(Integer::sum).add(__ -> list.size()))
            .filter((total, size) -> total.isPresent(), () -> new CustomException("error"))
            .map1((total, size) -> total.get()/size)
            .getSuccess();
        assertThat(result).isEqualTo(13);
    }

}
