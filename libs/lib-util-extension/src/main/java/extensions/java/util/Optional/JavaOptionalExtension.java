package extensions.java.util.Optional;

import com.ekwateur.lib.rop.control.Try;
import manifold.ext.rt.api.Extension;
import manifold.ext.rt.api.This;

import java.util.Optional;

@Extension
public class JavaOptionalExtension {
    public static <T> Try.Try1<T> toTry(@This Optional<T> thiz) {
        return Try.of(thiz.get());
    }
}
