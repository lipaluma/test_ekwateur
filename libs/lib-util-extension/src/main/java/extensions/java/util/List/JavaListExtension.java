package extensions.java.util.List;

import com.ekwateur.lib.rop.ThrowableFunction1_0;
import com.ekwateur.lib.rop.ThrowableFunction1_1;
import extensions.java.util.stream.Stream.JavaStreamExtension;
import manifold.ext.rt.api.Extension;
import manifold.ext.rt.api.This;

import java.util.List;
import java.util.stream.Stream;

@Extension
public class JavaListExtension {
    public static <E, R, X extends Exception> Stream<R> mapThrowing(@This List<E> thiz, ThrowableFunction1_1<X, ? super E, R> mapper ) throws X {
        return JavaStreamExtension.mapThrowing(thiz.stream(), mapper);
    }
    public static <E, R, X extends Exception> Stream<R> tryMap(@This List<E> thiz, ThrowableFunction1_1<X, ? super E, R> mapper ) {
        return JavaStreamExtension.tryMap(thiz.stream(), mapper);
    }

    public static <E, X extends Exception> Stream<E> andThen(@This List<E> thiz, ThrowableFunction1_0<X, ? super E> consumer ) throws X {
        return JavaStreamExtension.andThen(thiz.stream(), consumer);
    }
    public static <E, X extends Exception> Stream<E> andThenTry(@This List<E> thiz, ThrowableFunction1_0<X, ? super E> consumer ) {
        return JavaStreamExtension.andThenTry(thiz.stream(), consumer);
    }

}
