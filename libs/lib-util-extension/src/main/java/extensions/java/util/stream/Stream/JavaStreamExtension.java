package extensions.java.util.stream.Stream;

import com.ekwateur.lib.rop.ThrowableFunction1_0;
import com.ekwateur.lib.rop.ThrowableFunction1_1;
import manifold.ext.rt.api.Extension;
import manifold.ext.rt.api.This;

import java.util.stream.Stream;

import static extensions.java.util.Utils.sneakyThrow;

@Extension
public class JavaStreamExtension {

    public static <T, R, X extends Throwable> Stream<R> mapThrowing(@This Stream<T> thiz, ThrowableFunction1_1<X, ? super T, R> mapper ) throws X {
        return tryMap(thiz, mapper);
    }
    public static <T, R, X extends Throwable> Stream<R> tryMap(@This Stream<T> thiz, ThrowableFunction1_1<X, ? super T, R> mapper ) {
        return thiz.map(element -> {
            try {
                return mapper.apply(element);
            } catch (Throwable e) {
                return sneakyThrow(e);
            }
        });
    }

    public static <T, X extends Throwable> Stream<T> andThen(@This Stream<T> thiz, ThrowableFunction1_0<X, ? super T> consumer ) throws X {
        return andThenTry(thiz, consumer);
    }
    public static <T, X extends Throwable> Stream<T> andThenTry(@This Stream<T> thiz, ThrowableFunction1_0<X, ? super T> consumer ) {
        return thiz.peek(element -> {
            try {
                consumer.apply(element);
            } catch (Throwable e) {
                sneakyThrow(e);
            }
        });
    }

}
